<?php
/******************************************
US English
Admin Common Language
******************************************/

//header
$lang['common_sales'] = 'Sales';
$lang['common_orders']= 'Orders';
$lang['common_customers'] = 'Customers';
$lang['common_groups']= 'Groups';
$lang['common_reports'] = 'Reports';
$lang['common_coupons'] = 'Coupons';
$lang['common_gift_cards'] = 'Gift Cards';
$lang['common_catalog'] = 'Catalog';
$lang['common_categories']= 'Categories';
$lang['common_filters'] = 'Filters';
$lang['common_products']= 'Products';
$lang['common_digital_products']= 'Digital Products';
$lang['common_content'] = 'Content';
$lang['common_banners'] = 'Banners';
$lang['common_boxes'] = 'Boxes';
$lang['common_pages'] = 'Pages';
$lang['common_administrative']= 'Administrative';
$lang['common_gocart_configuration'] = 'TargetUs Configuration';
$lang['common_shipping_modules'] = 'Shipping Modules';
$lang['common_payment_modules'] = 'Payment Modules';
$lang['common_canned_messages'] = 'Canned Messages';
$lang['common_locations'] = 'Locations';
$lang['common_administrators']= 'Administrators';
$lang['common_note']= 'Note';
$lang['common_alert'] = 'Alert';
$lang['common_log_out'] = 'Log Out';
$lang['common_front_end'] = 'Front End';
$lang['common_dashboard'] = 'Dashboard';
$lang['common_home']= 'Home';
$lang['common_actions'] = 'Actions';
$lang['common_availablelocation'] = 'Available location';
$lang['enabled'] = 'Enabled';
$lang['disabled'] = 'Disabled';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['product_reports'] = 'Product Reports';
$lang['homepage_slider'] = 'Home Page Slider';
//buttons
$lang['save'] = 'Save';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['search'] = 'Search';
$lang['toggle_wysiwyg'] = 'Toggle WYSIWYG';
$lang['ltitle'] = 'Title';
$lang['laddress'] = 'Address';
$lang['lphone'] = 'Phone';
$lang['ltitle_link'] = 'Title link';
$lang['licons'] = 'Icons';
//slider language
$lang['s_title'] = 'Title';
$lang['s_description'] = 'Description';
$lang['s_link'] = 'Image link';
$lang['s_image'] = 'Image';
//moneris config
$lang['moneris_storeid'] = 'Store Id';
$lang['moneris_hpkey'] = 'Hp Key';
$lang['moneris_chargetot'] = 'Charge Total';
$lang['moneris_url'] = 'Payment Url';

