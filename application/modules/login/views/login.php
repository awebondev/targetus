
<div class="container" id="login">
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
            <?php echo form_open( base_url().'login', 'id="loginForm"'); ?>
                <div class="green-strips">
                    <h6>Login<?php echo validation_errors() ?></h6>
                </div>
                <div class="white-strips">
                    <div class="form-group">
                        <label for="email"><?php echo lang('email');?></label>
                        <input type="text" class="form-control" name="cardid" placeholder="The ID is printed on the back of the card"/>
                        <!-- <label>Dartslive Card ID</label>
                        <input class="form-control" type="input" placeholder="The ID is printed on the back of the card"> -->
                    </div>
                    <div class="form-group">
                        <label for="password"><?php echo lang('password');?></label>
                        <input class="form-control" type="password" name="password" palceholder=""/>
                        <!-- <label>PIN Number</label>
                        <input class="form-control" type="input" palceholder=""> -->
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <center>
                            <button type="submit" class="login-btn">
                                <span><img src="<?php echo base_url(); ?>assets/img/login-2.png"></span>
                                <h1><?php echo lang('form_login');?></h1>
                            </button>
                            <!-- <input type="submit" value="<?php echo lang('form_login');?>" name="submit" class="blue"/> -->
                        </center>
                    </div>
                </div>
                <div class="grey-strips">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <a href="<?php echo site_url('forgot-password'); ?>"><?php echo lang('forgot_password')?></a>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <a href="<?php echo base_url() ?>register">Register a New Card</a>
                    </div>
                </div>              
            </form>
        </div>
    </div>
</div>
<script>
    <?php if(isset($loginErrors)):?>

    var formErrors = <?php echo json_encode($loginErrors);?>
    
    for (var key in formErrors) {
        if (formErrors.hasOwnProperty(key)) {
            $('#loginForm').find('[name="'+key+'"]').parent().append('<div class="form-error text-red">'+formErrors[key]+'</div>')
        }
    }
    <?php endif;?>
</script>
