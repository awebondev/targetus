<div class="col" data-cols="1/2" data-push="1/4">
    <div class="container" id="login">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="green-strips">
                <h6 style="font-size:21px;"><?php echo lang('forgot_password');?></h6>
            </div>
            <div class="white-strips">
            <?php if(validation_errors()):?>
                <div class="alert red">
                    <?php echo validation_errors();?>
                </div>
            <?php endif;?>

            <?php echo form_open('forgot-password'); ?>
                <div class="form-group">
                    <label for="email"><?php echo lang('email');?></label>
                    <input class="form-control" type="text" name="email"/>
                </div>
           
                <input type="hidden" value="submitted" name="submitted"/>                
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <center>
                        <button type="submit" class="login-btn">
                            <span><img src="<?php echo base_url(); ?>assets/img/login-2.png"></span>
                            <h1><?php echo lang('reset_password');?></h1>
                        </button>                        
                    </center>
                </div>
            </form>
            </div>
            <div class="grey-strips text-center">
                <a href="<?php echo site_url('login'); ?>"><?php echo lang('return_to_login');?></a>
            </div>
            </div>
        </div>
    </div>
</div>