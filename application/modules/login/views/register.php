<div class="container" id="register">
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
            <form name="Form1" method="post" id="registration_form">
                <input type="hidden" name="submitted" value="submitted" />
                <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                <div class="green-strips">
                    <h6>Register a New Card</h6>
                </div>
                <div class="white-strips">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="company"><?php echo lang('reg_cntry');?></label>
                            <?php $options = array(
                                      ''  => 'Select',
                                      '1'  => 'Hong Kong/Macaut',
                                      '2'    => 'Taiwan',
                                      '3'   => 'Singapore',
                                      '4' => 'Japan',
                                      '6' => 'USA/Canada',
                                      '5' => 'Thailand',
                                      '8' => 'Malaysia',
                                      '10' => 'France',
                                      '11' => 'Spain',
                                    );
                                     echo form_dropdown(['name'=>'c','id'=>'country', 'options' => $options, 'class' => 'form-control']);?>
                                     <label id="cer"></label>
                            
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="hidden" id="element_7" name="element_7" class="element text medium" value=""  /> 
                    <input type="hidden" name="CustomFields[12]" id="CustomFields_12_15" maxlength="16" value="" />
                        <div class="form-group">
                            <label for="account_email"><?php echo lang('reg_email');?></label>
                            <?php echo form_input(['name'=>'email','id'=>'email', 'autocomplete'=>'email','maxlength' => '60', 'class' => 'form-control', 'value'=>set_value('email')]); ?>
                            <ul class="annotation">
                                <li>( Please enter a valid email address )</li>
                                <li><input type="hidden" name="format" value="h" /></li>
                            </ul>
                            <label id="emailer"></label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <div class="form-group">
                            <label for="account_firstname"><?php echo lang('reg_pin');?></label>
                            <?php echo form_password(['name'=>'p', 'class' => 'form-control', 'id' => 'password', 'maxlength' => '4','value'=> assign_value('reg_pin')]);?>
                            <label id="per"></label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 clear">
                        <div class="form-group">
                            <label for="account_firstname"><?php echo lang('reg_pwd');?></label>
                            <?php echo form_password(['name'=>'npassword', 'class' => 'form-control', 'id' => 'npassword', 'maxlength' => '15','value'=> assign_value('reg_pwd')]);?>
                            <label id="npassworder"></label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="account_firstname"><?php echo lang('reg_rpwd');?></label>
                            <?php echo form_password(['name'=>'rnpassword', 'class' => 'form-control', 'id' => 'rnpassword', 'maxlength' => '15','value'=> assign_value('reg_rpwd')]);?>
                            <label id="rnpassworder"></label>
                        </div>
                    </div>
                    
                    <!-- <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="account_lastname"><?php echo lang('reg_repin');?></label>
                            <?php echo form_password(['name'=>'p2', 'class' => 'form-control', 'id' => 'password', 'maxlength' => '4', 'value'=> assign_value('reg_repin')]);?>
                            <label id="p2er"></label>
                        </div>
                    </div> -->
                    <input type="hidden" name="CustomFields[14]" id="CustomFields_14_15" value="" maxlength='4'>
                    <input type="hidden" id="element_9" name="element_9"  class="element text medium" value=""   />
                    <div class="col-md-6 col-sm-6 col-xs-12 clear">
                        <div class="form-group">
                            <label for="account_firstname">DARTSLIVECard ID</label>
                            <input type="tel" name="i" maxlength="16" value="<?php echo set_value('i') ?>" id="cardID" class="form-control">
                            <ul class="annotation">
                                <li>(The ID is printed on the back of the card.)</li>
                            </ul>
                            <label id="ier"></label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="account_phone"><?php echo lang('account_phone');?></label>
                            <?php //echo form_input(['name'=>'CustomFields[4]','id' => 'CustomFields_4_15', 'class' => 'form-control', 'value'=> assign_value('phone')]);?>
                            <input type="tel" class="form-control" autocomplete="tel" name="CustomFields[4]" id="CustomFields_4_15" value="<?php echo set_value('CustomFields[4]') ?>" size='50'>
                            <input type="hidden" id="element_2" name="element_2" class="element text medium" maxlength="255" value=""/> 
                            <label id="acpher"></label>
                        </div>
                    </div>
                    <input type="hidden" id="element_3" name="element_3" class="element text medium" maxlength="255" value="" /> 
                    <div class="col-md-6 col-sm-6 col-xs-12 clear">
                        <div class="form-group">
                            <label for="account_firstname"><?php echo lang('account_firstname');?></label>

                            <?php echo form_input(['name'=>'CustomFields[2]', 'autocomplete' => 'fname', 'class' => 'form-control', 'id' => 'CustomFields_2_15','size' => '50', 'value'=> assign_value('CustomFields[2]')]);?>
                            <label id="firstnameer" class="form-error text-red"></label>

                        </div>
                        <input type="hidden" id="element_1_1" name="element_1_1"  class="element text" maxlength="255" size="8" value="" />
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label for="account_lastname"><?php echo lang('account_lastname');?></label>

                            <?php echo form_input(['name'=>'CustomFields[3]', 'autocomplete' => 'fname', 'id' =>  'CustomFields_3_15','size' => '50', 'class' => 'form-control', 'value'=> assign_value('CustomFields[3]')]);?>
                            <label id="lastnameer" class="form-error text-red"></label>

                            <input type="hidden" id="element_1_2" name="element_1_2" class="element text" maxlength="255" size="14" value="" />
                        </div>
                    </div>
                    

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="company"><?php echo lang('reg_location');?></label>
                            <select name="CustomFields[13]" class ="form-control" id="CustomFields_13_15">
                              <option value="">Select Location</option>
                              <?php foreach($locationval as $avloc){ ?>
                                  <option value="<?php echo $avloc->title ?>"><?php echo $avloc->title ?></option>
                              <?php } ?>
                            </select>                          

                            
                            <input type="hidden" id="element_10" name="element_10"  class="element text medium"  value=""   />
                            <label id="locationer" class="form-error text-red"></label>

                            <input type="hidden" name="format" value="h" />
                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12 col-xs-12 check-box">
                        <input id="checkbox" type="checkbox"/>
                        <label for="checkbox">Register after accepting Terms of Use and Privacy Policy</label>
                        </label><br>
                        <label id="termer" class="form-error text-red"></label>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">                       
                        <button type="button" href="#" value="SUBMIT" class="login-btn" 
                        onClick="return verifyEmail();" >
                            <span><img src="<?php echo base_url(); ?>assets/img/submit.png"></span>
                            <h1><?php echo lang('form_register');?></h1>
                        </button>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <a href="<?php echo base_url(); ?>login" class="ignore">Existing Registered Customers</a>
                    </div>
                </div>
                <div class="grey-strips">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                            <p>Check the e-mail address confirmation message which will be sent to you after the first login. The confirmation e-mail is sent from our domain (dartslive.jp).</p>
                    </div>
                </div>              
            </form>
            
        </div>
    </div>
</div>
<form name="frmSS15" id="frmSS15" action="http://targetus.ca/emailmarketer/form.php?form=15" method="post">
	<input type="hidden" value="" id="subscriber_email" name="email"/>
	<input type="hidden" value="0" id="CustomFields_12_15" name="CustomFields[12]"/>
	<input type="hidden" size="50" value="" id="CustomFields_2_15" name="CustomFields[2]"/>
	<input type="hidden" size="50" value="" id="CustomFields_3_15" name="CustomFields[3]"/>
	<input type="hidden" id="CustomFields_13_15" name="CustomFields[13]"/>
	<input type="hidden" size="50" value="" id="CustomFields_4_15" name="CustomFields[4]">
	<input type="hidden" maxlength="4" value="" id="CustomFields_14_15" name="CustomFields[14]">
	<input type="hidden" value="h" name="format">
</form>
<form method="POST" name="dartlivereg" id="dartlivereg" action="http://card.dartslive.com/t/doLogin.jsp">
	<input type="hidden" id="cardID" value="" name="i"/>
	<input type="hidden" value="" id="password" name="p"/>
	<input type="hidden" id="password2" name="p2"/>
	<input type="hidden" id="countryBox" name="c"/>
	<input type="hidden" id="termsCheck" name="t" value="1"/>
</form>
<script>
<?php if(validation_errors()):
    $errors = \CI::form_validation()->get_error_array(); ?>

    var formErrors = <?php echo json_encode($errors);?>
    
    for (var key in formErrors) {
        if (formErrors.hasOwnProperty(key)) {
            if(key == 'i'){
                $('#registration_form [name="'+key+'"]').parent().append('<div class="form-error text-red">The Card id already exist</div>')
            }else if(key == 'email'){
                $('#registration_form [name="'+key+'"]').parent().append('<div class="form-error text-red">The Email id already exist</div>')
            }else{
            $('#registration_form [name="'+key+'"]').parent().append('<div class="form-error text-red">'+formErrors[key]+'</div>')
            }
        }
    }
<?php endif;?>
</script>
<script language="JavaScript1.2">
function verifyEmail(){ 
    //alert('sj');
  var status = false;     
  var fname=document.forms["Form1"]["CustomFields[2]"].value;
  var lname=document.forms["Form1"]["CustomFields[3]"].value;
  var phno=document.forms["Form1"]["CustomFields[4]"].value;
  var c=document.forms["Form1"]["c"].value;
  var x=document.forms["Form1"]["p"].value;
  var y=document.forms["Form1"]["i"].value;
  var sjspwd=document.forms["Form1"]["npassword"].value;
  var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
        document.getElementById("cer").innerHTML="";
        document.getElementById("emailer").innerHTML=""; 
        //document.getElementById("p2er").innerHTML="";
        document.getElementById("per").innerHTML="";
        document.getElementById("rnpassworder").innerHTML="";
        document.getElementById("npassworder").innerHTML="";
        document.getElementById("ier").innerHTML="";
        document.getElementById("firstnameer").innerHTML="";
        document.getElementById("lastnameer").innerHTML="";
        document.getElementById("acpher").innerHTML="";
        document.getElementById("termer").innerHTML="";
    if (c=='') {
       document.getElementById("cer").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please select country</p></div>";
    }    
    if (document.Form1.npassword.value != document.Form1.rnpassword.value) {
        document.getElementById("rnpassworder").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Password do not match.  Please retype them to make sure they are the same</p></div>";
        //alert("PIN number do not match.  Please retype them to make sure they are the same.");
    } 
    if (sjspwd=='') {
       document.getElementById("npassworder").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Password required</p></div>";
    }
      
     if (!/^\d{4}$/.test(x)) {
        //alert("Please fill in 4 single-byte numerals");
        document.getElementById("per").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please fill in 4 single-byte numerals</p></div>";
    }
     if (!/^\d{16}$/.test(y)) {
        //alert("Please fill in 16 digits DARTSLIVECard ID");
        document.getElementById("ier").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please fill in 16 digits DARTSLIVECard ID</p></div>";
    }
      if (document.Form1.email.value.search(emailRegEx) == -1) {
        document.getElementById("emailer").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please enter a valid email address</p></div>";        
    }
    if (fname=='') {
       document.getElementById("firstnameer").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please select firstame</p></div>";
    }
    if (lname=='') {
       document.getElementById("lastnameer").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please select lastname</p></div>";
    }
    if (phno=='') {
       document.getElementById("acpher").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please select phone number</p></div>";
    }

     if($("#checkbox").prop('checked') == false){
        //alert('error');
        document.getElementById("termer").innerHTML="<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please accept Terms of Use and Privacy Policy</p></div>";
    }
    if(c!='' && document.Form1.npassword.value == document.Form1.rnpassword.value && /^\d{4}$/.test(x) && /^\d{16}$/.test(y) && document.Form1.email.value.search(emailRegEx) != -1 &&  $("#checkbox").prop('checked') == true && lname!=''&&lname!=''&&phno!=''){
        return OnButton1();
    }
}
</script>  

<script language="Javascript">
function OnButton1()
{	
	dartliveregForm = document.dartlivereg;
	dartliveregForm.cardID.value = cardID.value;    
	dartliveregForm.password.value = password.value;
	dartliveregForm.password2.value = password.value;
	dartliveregForm.countryBox.value = country.value;
	dartliveregForm.target = "_blank";    // Open in a new window
	dartliveregForm.submit();
    OnButton2()
    return true;
}

function OnButton2()
{
    //alert('s2');
    emailMarketingForm = document.frmSS15;
    emailMarketingForm.CustomFields_12_15.value = cardID.value;    
    emailMarketingForm.CustomFields_14_15.value = password.value;
    emailMarketingForm.CustomFields_2_15.value = CustomFields_2_15.value;
    emailMarketingForm.CustomFields_3_15.value = CustomFields_3_15.value;
    emailMarketingForm.CustomFields_13_15.value = CustomFields_13_15.value;
    emailMarketingForm.CustomFields_4_15.value = CustomFields_4_15.value;
    emailMarketingForm.subscriber_email.value = email.value;
    emailMarketingForm.target = "_blank";    // Open in a new window
    emailMarketingForm.submit();
    //setTimeout("document.Form1.submit();", 2000);             // Submit the page
	OnButton3();
    return true;
}

function OnButton3()
{
    s3element_7 = cardID.value;
    s3element_1_1 = CustomFields_2_15.value;
    s3element_1_2 = CustomFields_3_15.value;
    s3element_2 = CustomFields_4_15.value;
    s3element_3 = email.value;
    s3element_9 = password.value;
    s3element_10 = CustomFields_13_15.value;
    document.Form1.action = "<?php echo base_url() ?>register";
    document.Form1.submit();             // Submit the page
    return true;
}

</script>
