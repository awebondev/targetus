<?php namespace GoCart\Controller;
/**
 * Login Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    Login
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Login extends Front {

    var $customer;

    public function __construct()
    {
        parent::__construct();
        $this->customer = \CI::Login()->customer();
    }

    public function login($redirect= '')
    {
        //find out if they're already logged in
        if (\CI::Login()->isLoggedIn(false, false))
        {
            redirect($redirect);
        }
       
        \CI::load()->library('form_validation');
        \CI::form_validation()->set_rules('cardid', 'Cardid', 'required');
        \CI::form_validation()->set_rules('password', 'Password', ['required', ['check_login_callable', function($str){
            $email = \CI::input()->post('cardid');
            $password = \CI::input()->post('password');
            $remember = \CI::input()->post('remember');
            $login = \CI::Login()->loginCustomernew($email, sha1($password), $remember);
            if(!$login)
            {
                \CI::form_validation()->set_message('check_login_callable', lang('login_failed'));
                return false;
            }
        }]]);

        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('login', ['redirect'=>$redirect, 'loginErrors'=>\CI::form_validation()->get_error_array()]);
        }
        else
        {
            if(\GC::totalItems() > 0)
                {
                    $redirect = 'checkout';
                }else{
                    $redirect = 'category/all';
                }
                redirect($redirect);
        }
    }

    public function registerlogin($redirect= '')
    {
        //find out if they're already logged in
        if (\CI::Login()->isLoggedIn(false, false))
        {
            redirect($redirect);
        }
       
        \CI::load()->library('form_validation');
        \CI::form_validation()->set_rules('email', 'lang:address_email', ['trim','required','valid_email']);
        \CI::form_validation()->set_rules('password', 'Password', ['required', ['check_login_callable', function($str){
            $email = \CI::input()->post('email');
            $password = \CI::input()->post('password');
            $remember = \CI::input()->post('remember');
            $login = \CI::Login()->loginCustomer($email, sha1($password), $remember);
            if(!$login)
            {
                \CI::form_validation()->set_message('check_login_callable', lang('login_failed'));
                return false;
            }
        }]]);

        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('register', ['redirect'=>$redirect, 'loginErrors'=>\CI::form_validation()->get_error_array()]);
        }
        else
        {
            redirect($redirect);
        }
    }

    public function logout()
    {
        \CI::Login()->logoutCustomer();
        redirect('login');
    }

    public function forgotPassword()
    {
        $data['page_title'] = lang('forgot_password');

        \CI::form_validation()->set_rules('email', 'lang:address_email', ['trim', 'required', 'valid_email',
            ['email_callable', function($str)
                {
                    $reset = \CI::Customers()->reset_password($str);

                    if(!$reset)
                    {
                        \CI::form_validation()->set_message('email_callable', lang('error_no_account_record'));
                        return FALSE;
                    }
                    else
                    {
                        //user does exist. and the password is reset.
                        return TRUE;
                    }
                }
            ]
        ]);

        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('forgot_password', $data);
        }
        else
        {
            \CI::session()->set_flashdata('message', lang('message_new_password'));
            redirect('login');
        }
    }

    public function register($redirect= '')
    {/*
        echo 'regi';
        exit();*/
        $redirect  = \CI::Login()->isLoggedIn(false, false);
        //if they are logged in, we send them back to the my_account by default
        /*if ($redirect)
        {
            redirect('my-account');
        }*/
        
        \CI::load()->library('form_validation');
        
        //default values are empty if the customer is new
        $data = [
            'company' => '',
            'firstname' => '',
            'lastname' => '',
            'email' => '',
            'phone' => '',
            'address1' => '',
            'address2' => '',
            'city' => '',
            'state' => '',
            'zip' => '',

            'redirect' => \CI::session()->flashdata('redirect')
        ];
        
        \CI::form_validation()->set_rules('c', 'country', 'required');
        \CI::form_validation()->set_rules('p', 'pin', 'required|numeric');
        //\CI::form_validation()->set_rules('p2', 'repin', 'required|numeric');
        \CI::form_validation()->set_rules('npassword', 'Password', 'required');
        \CI::form_validation()->set_rules('rnpassword', 'retype password', 'required');
        \CI::form_validation()->set_rules('i', 'card', 'required|is_unique[customers.customercard_id]|numeric');
        \CI::form_validation()->set_rules('email', 'email', 'required|is_unique[customers.email]');
        \CI::form_validation()->set_rules('CustomFields[2]', 'fname', 'required');
        \CI::form_validation()->set_rules('CustomFields[3]', 'lastname', 'required');
        \CI::form_validation()->set_rules('CustomFields[4]', 'phone', 'required');
        \CI::form_validation()->set_rules('CustomFields[13]', 'area', 'required');        
        if (\CI::form_validation()->run() == FALSE)
        {
            //print_r($_POST);
           /* echo 'c1'.$save['country'] = \CI::input()->post('c');
            echo 'c2'.$save['pin']  = \CI::input()->post('p');
            echo 'c3'.$save['email'] = \CI::input()->post('email');
            echo 'c4'.$save['phone'] = \CI::input()->post('i');
            //echo 'c5'.$save['email'] = \CI::input()->post('email');
            echo 'c6'.$save['fname'] = \CI::input()->post('CustomFields_2');
            echo 'c7'.$save['lanem'] = \CI::input()->post('CustomFields_3');
            echo 'c8'.$save['phone'] = \CI::input()->post('CustomFields_4');
            echo 'c9'.$save['location'] = \CI::input()->post('CustomFields_13');*/

            //if they have submitted the form already and it has returned with errors, reset the redirect
            if (\CI::input()->post('submitted'))
            {
                $data['redirect'] = \CI::input()->post('redirect');
            }
            
            // load other page content 
            //\CI::load()->model('banner_model');
            \CI::load()->helper('directory');
            $data['locationval'] = \CI::Categories()->select_lcation();

            $data['registrationErrors'] = \CI::form_validation()->get_error_array();

            $this->view('register', $data);
        }
        else
        {
           
            $save['id'] = false;

            $save['country'] = \CI::input()->post('c');
            //$save['pin']  = \CI::input()->post('p');
            $save['email'] = \CI::input()->post('email');
            $save['customercard_id'] = \CI::input()->post('i');
            //echo 'c5'.$save['email'] = \CI::input()->post('email');
            $save['firstname'] = \CI::input()->post('CustomFields[2]');
            $save['lastname'] = \CI::input()->post('CustomFields[3]');
            $save['phone'] = \CI::input()->post('CustomFields[4]');
            $save['location'] = \CI::input()->post('CustomFields[13]');

            /*print_r($_POST);
           exit();*/

            /*$save['country'] = \CI::input()->post('c');
           // $save['pin']  = \CI::input()->post('p');
            $save['email'] = \CI::input()->post('email');
            $save['phone'] = \CI::input()->post('i');
            // /$save['company'] = \CI::input()->post('email');
            $save['active'] = (bool)config_item('CustomFields_2');
            $save['active'] = (bool)config_item('CustomFields_3');
            $save['active'] = (bool)config_item('CustomFields_4');
            $save['active'] = (bool)config_item('CustomFields_13');*/
            //$save['email_subscribe'] = intval((bool)\CI::input()->post('email_subscribe'));
            
            $save['password']  = sha1(\CI::input()->post('npassword'));
            $save['pin']  = sha1(\CI::input()->post('p'));
            $save['active']  = 1;
            
            $redirect  = \CI::input()->post('redirect');
            
            //if we don't have a value for redirect
            if ($redirect == '')
            {
                if(\GC::totalItems() > 0)
                {
                    $redirect = 'checkout';
                }else{
                    $redirect = 'category/all';
                }                
            }
            /*print_r($save);
            exit();*/
            // save the customer info and get their new id
            \CI::Customers()->save($save);
            
            //send the registration email
            \GoCart\Emails::registration($save);

            //load twig for this language string
            $loader = new \Twig_Loader_String();
            $twig = new \Twig_Environment($loader);
            
            //if they're automatically activated log them in and send them where they need to go
            if($save['active'])
            {
                \CI::session()->set_flashdata('message', $twig->render( lang('registration_thanks'), $save) );
            
                //lets automatically log them in
                \CI::Login()->loginCustomer($save['email'], $save['password']);

                //to redirect them, if there is no redirect, the it should redirect to the homepage.
                redirect($redirect);
            }
            else
            {
                //redirect to the login page if they need to wait for activation
                \CI::session()->set_flashdata('message', $twig->render( lang('registration_awaiting_activation'), $save) );
                redirect('login');
            }
        }
    }

    public function check_email($str)
    {
        $email = \CI::Customers()->check_email($str);
        
        if ($email)
        {
            \CI::form_validation()->set_message('check_email_callable', lang('error_email'));
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
}
