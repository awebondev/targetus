<?php
/**
 * Products Class
 *
 * @package     GoCart
 * @subpackage  Models
 * @category    Products
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

Class Products extends CI_Model
{
    public function __construct()
    {
        $this->customer = \CI::Login()->customer();
    }

    public function getProduct($id)
    {
        //do this again right here since it can be used for combining the cart. We want to make sure it's fresh.
        $this->customer = \GC::getCustomer();

        //find the product
        $product = CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price')->where('id', $id)->where('enabled_'.$this->customer->group_id, '1')->get('products')->row();
        $product = $this->processImageDecoding($product);
        return $product;
    }

    public function product_autocomplete($name, $limit)
    {
        return  CI::db()->like('name', $name)->get('products', $limit)->result();
    }

    public function touchInventory($id, $quantity)
    {
        $product = $this->getProduct($id);
        if(!$product)
        {
            return false;
        }

        CI::db()->where('id', $id)->update('products', ['quantity' => ($product->quantity - $quantity)]);
    }

    public function products($data=[], $return_count=false)
    {
        if(empty($data))
        {
            //if nothing is provided return the whole shabang
            CI::db()->order_by('name', 'ASC');
            $result = CI::db()->get('products');

            return $result->result();
        }
        else
        {
            //grab the limit
            if(!empty($data['rows']))
            {
                CI::db()->limit($data['rows']);
            }

            //grab the page
            if(!empty($data['page']))
            {
                CI::db()->offset($data['page']);
            }

            //do we order by something other than category_id?
            if(!empty($data['order_by']))
            {
                //if we have an order_by then we must have a direction otherwise KABOOM
                CI::db()->order_by($data['order_by'], $data['sort_order']);
            }

            //do we have a search submitted?
            if(!empty($data['term']))
            {
                $search = json_decode($data['term']);
                //if we are searching dig through some basic fields
                if(!empty($search->term))
                {
                    CI::db()->like('name', $search->term);
                    CI::db()->or_like('description', $search->term);
                    CI::db()->or_like('excerpt', $search->term);
                    CI::db()->or_like('sku', $search->term);
                }

                if(!empty($search->category_id))
                {
                    //lets do some joins to get the proper category products
                    CI::db()->join('category_products', 'category_products.product_id=products.id', 'right');
                    CI::db()->where('category_products.category_id', $search->category_id);
                    CI::db()->order_by('sequence', 'ASC');
                }
            }

            if($return_count)
            {
                return CI::db()->count_all_results('products');
            }
            else
            {
                return CI::db()->get('products')->result();
            }

        }
    }

    public function getProductss($category_id = false, $limit = false, $offset = false, $by=false, $sort=false)
    {
        //if we are provided a category_id, then get products according to category        
        if ($category_id)
        {
            $catli = [];
            foreach($category_id as $tt){
                /*print_r($tt);
                exit();*/
                //echo $tt;
                //array_push($catli,$tt);
                 $cats = \CI::Categories()->get_categories($tt);
                 /*for($i=0;i<count($cats);i++){ */
                        foreach($cats as $catss){
                 array_push($catli,$catss->id);
                    }
                //}
                 
                 //exit();
                //$data['total_products'] = \CI::Products()->count_products($category->id);
                    /*$productss = \CI::Products()->getProducts($tt->id, $per_page, $page, $sort = 'id', $direction = 'ASC');
                    foreach($productss as $productsss) {
                        array_push($products,$productsss);
                    }*/
            } 
            /*$dsds = $catli;
            echo count($dsds);
            print_r($dsds);
            foreach($dsds as $sjsj)
            {
                foreach($sjsj as $sjsjs)
                {
                echo $sjsjs->id.'<br><br>';
                }
            }*/
            //print_r($dsds);
                 //echo '<br><br><br>';
            //exit();










            $wheres = '';
             $cntsj = count($catli);
             //print_r($catli);
            //exit();
             $idm = 1;
            foreach($catli as $category_ids){
              //echo '<br>'  .$category_ids;
                if($idm < $cntsj){
                    $wheres .= ' category_id = "'.$category_ids.'" or';
                }else{
                    $wheres .= ' category_id = "'.$category_ids.'"';
                }
               $idm = $idm+1;
            }
            $where = '('.$wheres.')';
       
            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);

            CI::db()->order_by($by, $sort);

            $result = CI::db()->get()->result();

            $cuntspproduct = count($result);

            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);

            CI::db()->order_by($by, $sort);

            $result = CI::db()->limit($limit)->offset($offset)->get()->result();

            //echo '<br>'.count($result);
            //exit();
            $products = [];
           /* echo CI::db()->last_query;
            exit();*/

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            $productspecific = array(
                    'products' => $products,
                    'productcnt' => $cuntspproduct
                );
            return $productspecific;
        }
        else
        {   
            //sort by alphabetically by default
            $result = CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price')->limit($limit)->offset($offset)->order_by($by, $sort)->get('products')->result();
            $products = [];

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            return $products; 
        }
    }

    public function getProducts($category_id = false, $limit = false, $offset = false, $by=false, $sort=false)
    {
        //if we are provided a category_id, then get products according to category        
        if ($category_id)
        {
            $wheres = '';
             $cntsj = count($category_id);
             $idm = 1;
            foreach($category_id as $category_ids){
               // echo '<br>'  .$category_ids;
                if($idm < $cntsj){
                    $wheres .= ' category_id = "'.$category_ids.'" or';
                }else{
                    $wheres .= ' category_id = "'.$category_ids.'"';
                }
                $idm = $idm+1;
            }
            //exit();
            $where = '('.$wheres.')';
       
            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);

            CI::db()->order_by($by, $sort);

            $result = CI::db()->get()->result();

            $cuntspproduct = count($result);

            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);

            CI::db()->order_by($by, $sort);

            $result = CI::db()->limit($limit)->offset($offset)->get()->result();

            //echo '<br>'.count($result);
            //exit();
            $products = [];
           /* echo CI::db()->last_query;
            exit();*/

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            $productspecific = array(
                    'products' => $products,
                    'productcnt' => $cuntspproduct
                );
            return $productspecific;
        }
        else
        {   
            //sort by alphabetically by default
            $result = CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price')->limit($limit)->offset($offset)->order_by($by, $sort)->get('products')->result();
            $products = [];

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            return $products; 
        }
    }

    public function getProductsbyprspecial($category_id = false, $limit = false, $offset = false, $by=false, $sort=false)
    {
        //if we are provided a category_id, then get products according to category        
        if ($category_id)
        {
            $wheres = '';
             $cntsj = count($category_id);
             $idm = 1;
            foreach($category_id as $category_ids){
               // echo '<br>'  .$category_ids;
                if($idm < $cntsj){
                    $wheres .= ' category_id = "'.$category_ids.'" or';
                }else{
                    $wheres .= ' category_id = "'.$category_ids.'"';
                }
                $idm = $idm+1;
            }
            //exit();
            $where = '('.$wheres.')';
       
            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);
            CI::db()->where('Promo', 'TRUE');
            CI::db()->where('Special', 'TRUE');
            CI::db()->order_by($by, $sort);

            $result = CI::db()->get()->result();

            $cuntspproduct = count($result);

            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);
            CI::db()->where('Promo', 'TRUE');
            CI::db()->where('Special', 'TRUE');
            CI::db()->order_by($by, $sort);

            $result = CI::db()->limit($limit)->offset($offset)->get()->result();

            //echo '<br>'.count($result);
            //exit();
            $products = [];
           /* echo CI::db()->last_query;
            exit();*/

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            $productspecific = array(
                    'products' => $products,
                    'productcnt' => $cuntspproduct
                );
            return $productspecific;
        }
        else
        {   
            //sort by alphabetically by default
            $result = CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price')->where('Promo', 'TRUE')->where('Special', 'TRUE')->limit($limit)->offset($offset)->order_by($by, $sort)->get('products')->result();
            $products = [];

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            return $products; 
        }
    }

    public function count_all_products()
    {
        return CI::db()->count_all_results('products');
    }

    public function count_products($id)
    {
        return CI::db()->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id, 'enabled_'.$this->customer->group_id=>1))->count_all_results();
    }

    public function slug($slug, $related=true)
    {
      $result = CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price')->get_where('products', array('slug'=>$slug, 'enabled_'.$this->customer->group_id=>1))->row();

      if(!$result)
        {
            return false;
        }

        $related = json_decode($result->related_products);

        if(!empty($related))
        {
            //build the where
            $where = [];
            foreach($related as $r)
            {
                $where[] = '`id` = '.$r;
            }
            CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price');
            CI::db()->where('('.implode(' OR ', $where).')', null);
            CI::db()->where('enabled_'.$this->customer->group_id, 1);

            $result->related_products   = CI::db()->get('products')->result();
        }
        else
        {
            $result->related_products   = [];
        }
        $result->categories = $this->getProductCategories($result->id);

        return $result;
    }

    public function find($id, $related=true)
    {
        $result = CI::db()->get_where('products', array('id'=>$id))->row();
        if(!$result)
        {
            return false;
        }

        if($related)
        {
            $relatedItems = json_decode($result->related_products);
            if(!empty($relatedItems))
            {
                //build the where
                $where = [];
                foreach($relatedItems as $r)
                {
                    $where[] = '`id` = '.$r;
                }

                CI::db()->where('('.implode(' OR ', $where).')', null);
                CI::db()->where('enabled_'.$this->customer->group_id, 1);

                $result->related_products   = CI::db()->get('products')->result();
            }
            else
            {
                $result->related_products   = [];
            }
        }

        $result->categories = $this->getProductCategories($result->id);

        return $result;
    }

    public function getProductCategories($id)
    {
        return CI::db()->where('product_id', $id)->join('categories', 'category_id = categories.id')->get('category_products')->result();
    }

    public function save($product, $options=false, $categories=false)
    {
        if ($product['id'])
        {
            CI::db()->where('id', $product['id']);
            CI::db()->update('products', $product);

            $id = $product['id'];
        }
        else
        {
            CI::db()->insert('products', $product);
            $id = CI::db()->insert_id();
        }

        //loop through the product options and add them to the db
        if($options !== false)
        {

            // wipe the slate
            CI::ProductOptions()->clearOptions($id);

            // save edited values
            $count = 1;
            foreach ($options as $option)
            {
                $values = $option['values'];
                unset($option['values']);
                $option['product_id'] = $id;
                $option['sequence'] = $count;

                CI::ProductOptions()->saveOption($option, $values);
                $count++;
            }
        }

        if($categories !== false)
        {
            if($product['id'])
            {
                //get all the categories that the product is in
                $cats   = $this->getProductCategories($id);

                //generate cat_id array
                $ids    = [];
                foreach($cats as $c)
                {
                    $ids[]  = $c->id;
                }

                //eliminate categories that products are no longer in
                foreach($ids as $c)
                {
                    if(!in_array($c, $categories))
                    {
                        CI::db()->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
                    }
                }

                //add products to new categories
                foreach($categories as $c)
                {
                    if(!in_array($c, $ids))
                    {
                        CI::db()->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
                    }
                }
            }
            else
            {
                //new product add them all
                foreach($categories as $c)
                {
                    CI::db()->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
                }
            }
        }

        //return the product id
        return $id;
    }

    public function delete_product($id)
    {
        // delete product
        CI::db()->where('id', $id);
        CI::db()->delete('products');

        //delete references in the product to category table
        CI::db()->where('product_id', $id);
        CI::db()->delete('category_products');

        // delete coupon reference
        CI::db()->where('product_id', $id);
        CI::db()->delete('coupons_products');
    }

    public function search_products($term, $limit=false, $offset=false, $by=false, $sort=false)
    {
        $results = [];

        CI::db()->select('*, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false);
        //this one counts the total number for our pagination
        CI::db()->where('enabled_'.$this->customer->group_id, 1);
        CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($term).'%" OR description LIKE "%'.CI::db()->escape_like_str($term).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($term).'%" OR sku LIKE "%'.CI::db()->escape_like_str($term).'%")');
        $results['count'] = CI::db()->count_all_results('products');


        CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false);
        //this one gets just the ones we need.
        CI::db()->where('enabled_'.$this->customer->group_id, 1);
        CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($term).'%" OR description LIKE "%'.CI::db()->escape_like_str($term).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($term).'%" OR sku LIKE "%'.CI::db()->escape_like_str($term).'%")');

        if($by && $sort)
        {
            CI::db()->order_by($by, $sort);
        }
        $products = CI::db()->get('products', $limit, $offset)->result();
        $results['products'] = [];
        foreach($products as $product)
        {
            $results['products'][] = $this->processImageDecoding($product);
        }

        return $results;
    }

    public function search_products_alpha($term, $limit=false, $offset=false, $by=false, $sort=false)
    {
        $results = [];

        CI::db()->select('*, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false);
        //this one counts the total number for our pagination
        CI::db()->where('enabled_'.$this->customer->group_id, 1);
        CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($term).'%")');
        $results['count'] = CI::db()->count_all_results('products');


        /*CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false);*/
        //this one gets just the ones we need.
        CI::db()->where('enabled_'.$this->customer->group_id, 1);
        CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($term).'%")');

        if($by && $sort)
        {
            CI::db()->order_by($by, $sort);
        }
        $products = CI::db()->get('products', $limit, $offset)->result();
        $results['products'] = [];
        foreach($products as $product)
        {
            $results['products'][] = $this->processImageDecoding($product);
        }

        return $results;
    }

    public function search_products_byprice($stprice=null, $endprice=null, $term, $limit=false, $offset=false, $by=false, $sort=false)
    {
        $results = [];

        CI::db()->select('*, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false);
        //this one counts the total number for our pagination
        CI::db()->where('enabled_'.$this->customer->group_id, 1);
        CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
        CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);
        //CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($term).'%" OR description LIKE "%'.CI::db()->escape_like_str($term).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($term).'%" OR sku LIKE "%'.CI::db()->escape_like_str($term).'%")');
        $results['count'] = CI::db()->count_all_results('products');


        CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false);
        //this one gets just the ones we need.
        CI::db()->where('enabled_'.$this->customer->group_id, 1);
        //CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($term).'%" OR description LIKE "%'.CI::db()->escape_like_str($term).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($term).'%" OR sku LIKE "%'.CI::db()->escape_like_str($term).'%")');
        CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
        CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);

        if($by && $sort)
        {
            CI::db()->order_by($by, $sort);
        }
        $products = CI::db()->get('products', $limit, $offset)->result();
        $results['products'] = [];
        foreach($products as $product)
        {
            $results['products'][] = $this->processImageDecoding($product);
        }

        return $results;
    }

    public function search_all_products($stprice=null, $endprice=null, $category_id, $limit=false, $offset=false, $by=false, $sort=false,$sr_serach_input=null,$sr_alpha_list=null ,$allctegary=null, $category_idd=false,$pr_size=false, $pr_color=false)
    {



        //if we are provided a category_id, then get products according to category        
        if ($category_id && $category_id != 'all')
        {

            if($category_idd[0] != '' ){
                /*echo 'gf';
                echo $category_idd;
                echo 'gg';
                print_r($category_idd);
                exit();*/
                /*echo $category_idd;
                print_r($category_idd);
                exit();*/
                $dproducts = explode(',', $category_idd[0]);
                /*print_r($dproducts);
                exit(); */             
            }else{
                /*echo '1gf';*/
                $category = \CI::Categories()->getBySlug($category_id);
                $catcunt = \CI::Categories()->get_categories($category->id);
                $dproducts = CI::Categories()->ConstructHierarchy($dd = null,$catcunt,$ss=null,$products_per_page,$page,$sort, $direction);

            }
            
                /*print_r($dproducts);
                exit();*/
            
            $wheres = '';
             $cntsj = count($dproducts);
             if($cntsj > 1){

             $idm = 1;
            foreach($dproducts as $category_ids){
               // echo '<br>'  .$category_ids;
                if($idm < $cntsj){
                    $wheres .= ' category_id = "'.$category_ids.'" or';
                }else{
                    $wheres .= ' category_id = "'.$category_ids.'"';
                }
                $idm = $idm+1;
            }
            //exit();
            $where = '('.$wheres.')';
            }else{
                /*$category = \CI::Categories()->getBySlug($category_id);
                $catcunt = \CI::Categories()->get_categories($category->id);
                print_r($catcunt);
                exit();*/
                //echo $catid = $category->id;

                //exit();
                $where .= ' category_id = "'.$dproducts[0].'"';
            }








            $wherecolumn = '';
            if($allctegary[0]){
                //print_r($allctegary);
                $allctegary= explode(',',$allctegary[0]);
                
                $catcunt = count($allctegary);
             //exit();
             if($catcunt > 1){
                //echo 'sj1';

             $catidm = 1;
            foreach($allctegary as $category_idss){
                if($catidm < $catcunt){
                    $wherecolumn .= $category_idss.'= "TRUE" and ';
                }else{
                    $wherecolumn .= $category_idss.'= "TRUE" ';
                }
                $catidm = $catidm+1;
            }
           
            $wherecolumn = '('.$wherecolumn.')';
            }else{
                //echo 'sj2';
                $wherecolumn .= $allctegary[0].'= "TRUE" ';
            }

        }
        /*echo 'outside';
        exit();*/



















       
            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);
            if($wherecolumn){
                CI::db()->where($wherecolumn);
            }
        if($sr_alpha_list){
            CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($sr_alpha_list).'%")');
        }else{

            CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR description LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR sku LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%")');
        }
            CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
            CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);
            CI::db()->where('Online_Item', 'TRUE');

            if($pr_size){
                CI::db()->where('size', $pr_size);                
            }
            if($pr_color){
                CI::db()->where('color', $pr_color);                
            }
            

                 //CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
                //CI::db()->where('enabled_'.$this->customer->group_id, 1);
        


            CI::db()->order_by($by, $sort);

            $result = CI::db()->get()->result();

            /*echo CI::db()->last_query();
            exit();*/

            $cuntspproduct = count($result);

            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);
            if($wherecolumn){
                CI::db()->where($wherecolumn);
            }
            if($sr_alpha_list){
            CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($sr_alpha_list).'%")');
            }else{

                CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR description LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR sku LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%")');
            }
                CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
                CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);
                CI::db()->where('Online_Item', 'TRUE');
                if($pr_size){
                    CI::db()->where('size', $pr_size);                
                }
                if($pr_color){
                    CI::db()->where('color', $pr_color);                
                }

            CI::db()->order_by($by, $sort);

            $result = CI::db()->limit($limit)->offset($offset-1)->get()->result();

            //echo '<br>'.count($result);
            //exit();
            $products = [];
           /* echo CI::db()->last_query;
            exit();*/

            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
            $productspecific = array(
                    'products' => $products,
                    'productcnt' => $cuntspproduct
                );
            return $productspecific;
        }
        else
        {   
            $wherecolumn = '';
            if($allctegary[0]){
                //print_r($allctegary);
                $allctegary= explode(',',$allctegary[0]);
                
                $catcunt = count($allctegary);
                 //exit();
                 if($catcunt > 1){
                    //echo 'sj1';

                 $catidm = 1;
                foreach($allctegary as $category_idss){
                    if($catidm < $catcunt){
                        $wherecolumn .= $category_idss.'= "TRUE" and ';
                    }else{
                        $wherecolumn .= $category_idss.'= "TRUE" ';
                    }
                    $catidm = $catidm+1;
                }
               
                $wherecolumn = '('.$wherecolumn.')';
                }else{
                    //echo 'sj2';
                    $wherecolumn .= $allctegary[0].'= "TRUE" ';
                }

            }



            /*CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);*/

            
            //sort by alphabetically by default
             CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price' );

            if($wherecolumn){
                CI::db()->where($wherecolumn);
            }
            if($sr_alpha_list){
                CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($sr_alpha_list).'%")');
            }else{

                CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR description LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR sku LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%")');
            }
                CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
                CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);
                CI::db()->where('Online_Item', 'TRUE');
                if($pr_size){
                   // echo "sj1";
                    CI::db()->where('size', $pr_size);                
                }
                if($pr_color){
                    //echo "sj2";
                    CI::db()->where('color', $pr_color);                
                }



            $result = CI::db()->order_by($by, $sort)->get('products')->result();
            /*echo $sjsjs = CI::db()->last_query();
            exit();
*/
            $cuntspproductall = count($result);


            CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price' );

            if($wherecolumn){
                CI::db()->where($wherecolumn);
            }
            if($sr_alpha_list){
                CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($sr_alpha_list).'%")');
            }else{

                CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR description LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR sku LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%")');
            }
                CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
                CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);
                CI::db()->where('Online_Item', 'TRUE');
                if($pr_size){
                    CI::db()->where('size', $pr_size);                
                }
                if($pr_color){
                    CI::db()->where('color', $pr_color);                
                }




            $result = CI::db()->limit($limit)->offset($offset-1)->order_by($by, $sort)->get('products')->result();

            /*echo $sjsjs = CI::db()->last_query($result);
            exit();*/
            /*print_r($result);
            exit();*/



            /*echo CI::db()->last_query();
            exit();*/
            $products = [];



            foreach($result as $product)
            {
                $products[] = $this->processImageDecoding($product);
            }
           /* print_r($products);
            exit();*/
            $productspecific = array(
                    'products' => $products,
                    'productcnt' => $cuntspproductall
                );
            return $productspecific; 


        }















    }

    public function processImageDecoding($product)
    {
        if($product)
        {
            $product->images = json_decode($product->images, true);
            if($product->images)
            {
                $product->images = array_values($product->images);
            }
            else
            {
                $product->images = [];
            }
            return $product;
        }
        else
        {
            return $product;
        }
        
    }

    public function validate_slug($slug, $id=false, $counter=false)
    {
        CI::db()->select('slug');
        CI::db()->from('products');
        CI::db()->where('slug', $slug.$counter);
        if ($id)
        {
            CI::db()->where('id !=', $id);
        }
        $count = CI::db()->count_all_results();

        if ($count > 0)
        {
            if(!$counter)
            {
                $counter = 1;
            }
            else
            {
                $counter++;
            }
            return $this->validate_slug($slug, $id, $counter);
        }
        else
        {
             return $slug.$counter;
        }
    }


    //adminsearch
     public function sumof_all_productsadmin($stprice=null, $endprice=null, $category_id, $limit=false, $offset=false, $by=false, $sort=false,$sr_serach_input=null,$sr_alpha_list=null ,$allctegary=null, $category_idd=false,$pr_size=false, $pr_color=false,$prmonth=null,$pryear=null)
    {
        if ($category_id && $category_id != 'all')
        {

            if($category_idd[0] != '' ){                
                $dproducts = explode(',', $category_idd[0]);                            
            }else{
                $category = \CI::Categories()->getBySlug($category_id);
                $catcunt = \CI::Categories()->get_categories($category->id);
                $dproducts = CI::Categories()->ConstructHierarchy($dd = null,$catcunt,$ss=null,$products_per_page,$page,$sort, $direction);
            }
            $wheres = '';
            $cntsj = count($dproducts);
            if($cntsj > 1){
                $idm = 1;
                foreach($dproducts as $category_ids){
                    if($idm < $cntsj){
                        $wheres .= ' category_id = "'.$category_ids.'" or';
                    }else{
                        $wheres .= ' category_id = "'.$category_ids.'"';
                    }
                    $idm = $idm+1;
                }
            $where = '('.$wheres.')';
            }else{
                $where .= ' category_id = "'.$dproducts.'"';
            }
            $wherecolumn = '';
            if($allctegary[0]){
                $allctegary= explode(',',$allctegary[0]);                
                $catcunt = count($allctegary);
                if($catcunt > 1){
                    $catidm = 1;
                    foreach($allctegary as $category_idss){
                        if($catidm < $catcunt){
                            $wherecolumn .= $category_idss.'= "TRUE" and ';
                        }else{
                            $wherecolumn .= $category_idss.'= "TRUE" ';
                        }
                        $catidm = $catidm+1;
                    }           
                    $wherecolumn = '('.$wherecolumn.')';
                }else{
                    $wherecolumn .= $allctegary[0].'= "TRUE" ';
                }
            }
       
            CI::db()->select('category_products.*, products.*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price, LEAST(IFNULL(NULLIF(saleprice_'.$this->customer->group_id.', 0), price_'.$this->customer->group_id.'), price_'.$this->customer->group_id.') as sort_price', false)->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('enabled_'.$this->customer->group_id=>1))->where($where);
            if($wherecolumn){
                CI::db()->where($wherecolumn);
            }
            if($sr_alpha_list){
                CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($sr_alpha_list).'%")');
            }else{
                CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR description LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR sku LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%")');
            }
            CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
            CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);

            if($pr_size){
                CI::db()->where('size', $pr_size);
            }
            if($pr_color){
                CI::db()->where('color', $pr_color);
            }            
            CI::db()->order_by($by, $sort);
            $result = CI::db()->get()->result();
           // $cuntspproduct = count($result);
             $totalpricead=array();

            foreach($result as $product){
                $orders = \CI::Orders()->getorderby_productid($product->id,$prmonth,$pryear);
                //echo $orders[0]->product_id.'hh';
                if($orders[0]->product_id){
                    ////print_r($orders);
                    echo '';
                array_push($totalpricead,$orders);
                }
            } 
            //print_r($totalpricead);
        //exit();
              $sjs = '<table class="table table-striped" style="margin-top:20px;">
            <thead>
                <tr style="color:green">
                    <th>PRODUCT NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Manufaturer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Total Product&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Weight</th>
                </tr>
            </thead><tbody>';
            $csvf = '"PRODUCT NAME","AMOUNT","Manufaturer","Total Product","Weight"<br>';
            $arprdct = array();
            $csvf = array();
             $csvfs = array(
                    'PRODUCT NAME','AMOUNT','Manufaturer','Total Product','Weight'
                );
             array_push($csvf,$csvfs);
            foreach($totalpricead as $sortlistt){
                foreach($sortlistt as $sortlist){
               // array_push($arprdct , $sortlist->product_id)

                //print_r($sortlist);
                /*echo $sortlist->product_id;
                echo $sortlist->totalprc;
                echo '<br><br>';*/
                //$prdtdetail = $this->getProduct($sortlist->product_id);
                //$csvf .=  '"'.$sortlist->name.'","'.$sortlist->totalprc.'","'.$sortlist->Manufaturer.'","'.$sortlist->prweight.'"<br>';
                
                $list = array
                (
                    $sortlist->name,$sortlist->totalprc,$sortlist->Manufaturer,$sortlist->cntprdct,$sortlist->prweight 
                );
                array_push($csvf,$list);


                $sjs .= 
                '<tr>
                    <td>'.$sortlist->name.'</td>
                    <td>'.$sortlist->totalprc.'</td>
                    <td>'.$sortlist->Manufaturer.'</td>
                    <td>'.$sortlist->cntprdct.'</td>
                    <td>'.$sortlist->prweight.'</td>
                </tr>';
            } }
            //exit();prweight
            $sjs .= '</tbody>';
            $sjs .= '</table>';
            if(!empty($totalpricead)){
               $sjs .= '<a class="btn btn-success" href="'.base_url().'csvfiles/salesdetails.csv">get csv</button>';
            }else{
                $sjs .= '<center style="color:red">NO ORDER FOUND</center>';
            }
                /*$list = array
                (
                "Peter,Griffin,Oslo,Norway",
                "Glenn,Quagmire,Oslo,Norway",
                );*/

                $file = fopen("csvfiles/salesdetails.csv","w");

                foreach ($csvf as $line)
                  {
                  fputcsv($file,$line);
                  }

                fclose($file); 

            echo $sjs;               
        }
        else
        {   
            $wherecolumn = '';
            if($allctegary[0]){
                $allctegary= explode(',',$allctegary[0]);                
                $catcunt = count($allctegary);
                 if($catcunt > 1){
                 $catidm = 1;
                foreach($allctegary as $category_idss){
                    if($catidm < $catcunt){
                        $wherecolumn .= $category_idss.'= "TRUE" and ';
                    }else{
                        $wherecolumn .= $category_idss.'= "TRUE" ';
                    }
                    $catidm = $catidm+1;
                }               
                $wherecolumn = '('.$wherecolumn.')';
                }else{
                    $wherecolumn .= $allctegary[0].'= "TRUE" ';
                }
            }            
            //sort by alphabetically by default
             CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price' );
            if($wherecolumn){
                CI::db()->where($wherecolumn);
            }
            if($sr_alpha_list){
                CI::db()->where('(name LIKE "'.CI::db()->escape_like_str($sr_alpha_list).'%")');
            }else{

                CI::db()->where('(name LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR description LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR excerpt LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%" OR sku LIKE "%'.CI::db()->escape_like_str($sr_serach_input).'%")');
            }
                CI::db()->where('price_'.$this->customer->group_id.' >=', $stprice);
                CI::db()->where('price_'.$this->customer->group_id.' <=', $endprice);

                if($pr_size){
                    CI::db()->where('size', $pr_size);                
                }
                if($pr_color){
                    CI::db()->where('color', $pr_color);                
                }
            $result = CI::db()->order_by($by, $sort)->get('products')->result();

            $totalpricead=array();

            foreach($result as $product){
                $orders = \CI::Orders()->getorderby_productid($product->id,$prmonth,$pryear);
                //echo $orders[0]->product_id.'hh';
                if($orders[0]->product_id){
                    ////print_r($orders);
                    echo '';
                array_push($totalpricead,$orders);
                }
            } 
            //print_r($totalpricead);
        //exit();
            $sjs = '<table class="table table-striped" style="margin-top:20px;">
            <thead>
                <tr style="color:green">
                    <th>PRODUCT NAME&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>AMOUNT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Manufaturer&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Total Product&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    <th>Weight</th>
                </tr>
            </thead><tbody>';
            $csvf = '"PRODUCT NAME","AMOUNT","Manufaturer","Total Product","Weight"<br>';
            $arprdct = array();
            $csvf = array();
             $csvfs = array(
                    'PRODUCT NAME','AMOUNT','Manufaturer','Total Product','Weight'
                );
             array_push($csvf,$csvfs);
            foreach($totalpricead as $sortlistt){
                foreach($sortlistt as $sortlist){
               // array_push($arprdct , $sortlist->product_id)

                //print_r($sortlist);
                /*echo $sortlist->product_id;
                echo $sortlist->totalprc;
                echo '<br><br>';*/
                //$prdtdetail = $this->getProduct($sortlist->product_id);
                //$csvf .=  '"'.$sortlist->name.'","'.$sortlist->totalprc.'","'.$sortlist->Manufaturer.'","'.$sortlist->prweight.'"<br>';
                
                $list = array
                (
                    $sortlist->name,$sortlist->totalprc,$sortlist->Manufaturer,$sortlist->cntprdct,$sortlist->prweight 
                );
                array_push($csvf,$list);


                $sjs .= 
                '<tr>
                    <td>'.$sortlist->name.'</td>
                    <td>'.$sortlist->totalprc.'</td>
                    <td>'.$sortlist->Manufaturer.'</td>
                    <td>'.$sortlist->cntprdct.'</td>
                    <td>'.$sortlist->prweight.'</td>
                </tr>';
            } }
            //exit();prweight
            $sjs .= '</tbody>';
            $sjs .= '</table>';
            if(!empty($totalpricead)){
               $sjs .= '<a class="btn btn-success" href="'.base_url().'csvfiles/salesdetails.csv">get csv</button>';
            }else{
                $sjs .= '<center style="color:red">NO ORDER FOUND</center>';
            }

            
                /*$list = array
                (
                "Peter,Griffin,Oslo,Norway",
                "Glenn,Quagmire,Oslo,Norway",
                );*/

                $file = fopen("csvfiles/salesdetails.csv","w");

                foreach ($csvf as $line)
                  {
                  fputcsv($file,$line);
                  }

                fclose($file); 

            echo $sjs;           
            


                
        }
    }


}
