<div class="col-nest"><div class="container">
    <div class="row">
        <div class="col-md-12 ">
            <div class="dynamic-search">
                <div class="col-md-2 no-padding"><h5>Find Your Categories</h5></div>
                <div class="col-md-10 no-padding">
                    <a href="#">#</a>
                    <a href="#">a</a>
                    <a href="#">b</a>
                    <a href="#">c</a>
                    <a href="#">d</a>
                    <a href="#">e</a>
                    <a href="#">f</a>
                    <a href="#">g</a>
                    <a href="#">h</a>
                    <a href="#">i</a>
                    <a href="#">j</a>
                    <a href="#">k</a>
                    <a href="#">l</a>
                    <a href="#">m</a>
                    <a href="#">n</a>
                    <a href="#">o</a>
                    <a href="#">p</a>
                    <a href="#">q</a>
                    <a href="#">r</a>
                    <a href="#">s</a>
                    <a href="#">t</a>
                    <a href="#">u</a>
                    <a href="#">v</a>
                    <a href="#">w</a>
                    <a href="#">x</a>
                    <a href="#">y</a>
                    <a href="#">z</a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="breadcrumb">                        
                <span><img src="<?php echo base_url(); ?>assets/img/home.png"></span> 
                <h6><?php echo $product->name; ?></h6>              
            </div>
        </div>
        <div class="col-md-12 no-padding pro-bot-des">
            <div class="col-md-4 ">
                <div class="product service-image-left">
                <?php
                    $photo = theme_img('no_picture.png', lang('no_image_available'));

                    if(!empty($product->images[0]))
                    {
                        foreach($product->images as $photo)
                        {
                            if(isset($photo['primary']))
                            {
                                $primary = $photo;
                            }
                        }
                        if(!isset($primary))
                        {
                            $tmp = $product->images; //duplicate the array so we don't lose it.
                            $primary = array_shift($tmp);
                        }

                        $photo = '<img src="'.base_url('product_photo/'.$primary['filename']).'" alt="'.$product->seo_title.'" data-caption="'.htmlentities(nl2br($primary['caption'])).'"/>';
                    }
                    echo $photo
                    ?></div>
                    <?php if(count($product->images) > 1):?>
                            <?php foreach($product->images as $image):?>
                                    <img id="item-display" src="<?php echo base_url('product_photo/'.$image['filename']);?>" />
                            <?php endforeach;?>
                    <?php endif;?>
                </div>
                                                            
            <div class="col-md-8">
                <div id="productAlerts"></div>
                <div class="product-title"><?php echo $product->name;?></div>
                <!-- div class="product-avali">Availability : <span class="product-stock">
                        <?php //if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')):?>
                            <div class="categoryItemNote yellow"><?php// echo lang('out_of_stock');?></div>
                        <?php //elseif($product->saleprice > 0):?>
                            <div class="categoryItemNote red"><?php //echo lang('on_sale');?></div>
                        <?php //endif;?>
                </span></div -->
                <hr>
                <!-- div class="product-left"><?php //echo "Only ".$product->quantity." Left"; ?></div --> 
                <div class="product-left">Price : </div>                             
                <div class="product-price"><?php echo $product->price_1;?></div>
                <div class="product-desc"><?php echo $product->description;?></div>             
                <hr>
                <!-- div class="btn-group wishlist">
                    <div class="sp-quantity">                       
                        <div class="sp-input">
                            <input type="text" class="quntity-input" value="1" placeholder="Qty :" />
                            <input type="hidden" id="maxqty" value="<?php //echo $product->quantity; ?>" placeholder="Qty :" />
                        </div>                    
                        <div class="sp-plus fff"> <a class="ddd" href="#">+</a>
                        </div>
                         <div class="sp-minus fff"> <a class="ddd" href="#">-</a>
                        </div>                      
                    </div -->                
                <div class="btn-group cart">
                        <div class="productDetails">
            
            <?php echo form_open('cart/add-to-cart', 'id="add-to-cart"'); ?>

            <input type="hidden" name="cartkey" value="<?php echo CI::session()->flashdata('cartkey');?>" />
            <input type="hidden" name="id" value="<?php echo $product->id?>"/>

            <?php if(count($options) > 0): ?>
                <?php foreach($options as $option):
                    $required = '';
                    if($option->required)
                    {
                        $required = ' class="required"';
                    }
                    ?>
                        <div class="col-nest">
                            <div class="col" data-cols="1/3">
                                <label<?php echo $required;?>><?php echo ($product->is_giftcard) ? lang('gift_card_'.$option->name) : $option->name;?></label>
                            </div>
                            <div class="col" data-cols="2/3">
                                        <?php
                                        if($option->type == 'checklist')
                                        {
                                            $value  = [];
                                            if($posted_options && isset($posted_options[$option->id]))
                                            {
                                                $value  = $posted_options[$option->id];
                                            }
                                        }
                                        else
                                        {
                                            if(isset($option->values[0]))
                                            {
                                                $value  = $option->values[0]->value;
                                                if($posted_options && isset($posted_options[$option->id]))
                                                {
                                                    $value  = $posted_options[$option->id];
                                                }
                                            }
                                            else
                                            {
                                                $value = false;
                                            }
                                        }

                                        if($option->type == 'textfield'):?>
                                            <input type="text" name="option[<?php echo $option->id;?>]" value="<?php echo $value;?>"/>
                                        <?php elseif($option->type == 'textarea'):?>
                                            <textarea name="option[<?php echo $option->id;?>]"><?php echo $value;?></textarea>
                                        <?php elseif($option->type == 'droplist'):?>
                                            <select name="option[<?php echo $option->id;?>]">
                                                <option value=""><?php echo lang('choose_option');?></option>

                                            <?php foreach ($option->values as $values):
                                                $selected   = '';
                                                if($value == $values->id)
                                                {
                                                    $selected   = ' selected="selected"';
                                                }?>

                                                <option<?php echo $selected;?> value="<?php echo $values->id;?>">
                                                    <?php if($product->is_giftcard):?>
                                                        <?php echo($values->price != 0)?' (+'.format_currency($values->price).') ':''; echo lang($values->name);?>
                                                    <?php else:?>
                                                        <?php echo($values->price != 0)?' (+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                    <?php endif;?>
                                                    
                                                </option>

                                            <?php endforeach;?>
                                            </select>
                                        <?php elseif($option->type == 'radiolist'):?>
                                            <label class="radiolist">
                                            <?php foreach ($option->values as $values):

                                                $checked = '';
                                                if($value == $values->id)
                                                {
                                                    $checked = ' checked="checked"';
                                                }?>
                                                <div>
                                                    <input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>"/>
                                                    <?php echo($values->price != 0)?'(+'.format_currency($values->price).') ':''; echo $values->name;?>
                                                </div>
                                            <?php endforeach;?>
                                            </label>
                                        <?php elseif($option->type == 'checklist'):?>
                                            <label class="checklist"<?php echo $required;?>>
                                            <?php foreach ($option->values as $values):

                                                $checked = '';
                                                if(in_array($values->id, $value))
                                                {
                                                    $checked = ' checked="checked"';
                                                }?>
                                                <div><input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>"/>
                                                <?php echo($values->price != 0 && $option->name != 'Buy a Sample')?'('.format_currency($values->price).') ':''; echo $values->name;?></div>
                                            <?php endforeach; ?>
                                            </label>
                                        <?php endif;?>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                            <?php endif;?>

                            <div class="text-right">
                            <?php if(!config_item('inventory_enabled') || config_item('allow_os_purchase') || !(bool)$product->track_stock || $product->quantity > 0) : ?>

                                <?php if(!$product->fixed_quantity) : ?>   
                                    <?php if($product->Online_Salable == 'TRUE'){ ?>                                 
                                    <sapn class="qty">Quantity&nbsp;</span>
                                    <input type="text" name="quantity" value="1" style="width:50px; display:inline"/>&nbsp;
                                    <button class="product-desbtn" type="button" value="submit" onclick="addToCart($(this));">
                                    <i class="icon-cart"></i> <?php echo lang('form_add_to_cart');?></button>
                                    <?php } ?>
                                    <?php else: ?>                                        
                                    <button class="product-desbtn" type="button" value="submit" onclick="addToCart($(this));">
                                    <i class="icon-cart"></i> <?php echo lang('form_add_to_cart');?></button>
                                    <?php endif;?>
                            <?php endif;?>
                                </div>
                            </form>
                    </div>
                </div>
                <hr>
                <div class="wish">
                    <a href="#" class=""><i class="fa fa-heart-o"></i><span>Add to Wishlist</span></a>
                </div>
                <div class="compare">
                    <a href="#" class=""><img src="<?php echo base_url(); ?>assets/img/compare-pro.png"><span>Add to Compare</span></a>
                </div>
                <hr>
                <div class="social-icons">
                    <a href="#"><i class="fa fa-facebook-square"></i></a>
                    <a href="#"><i class="fa fa-twitter-square"></i></a>
                    <a href="#" class="pull-right"><i class="fa fa-envelope-o"></i><span>Email to a Friend</span></a>
                </div>
                <hr>                
            </div>
            </div>
        </div>              
    </div>
</div> 

<script>
    function addToCart(btn)
    {
        $('.productDetails').spin();
        btn.attr('disabled', true);
        var cart = $('#add-to-cart');
        $.post(cart.attr('action'), cart.serialize(), function(data){
            if(data.message != undefined)
            {   
                $('.productDetails').spin(false);
                $('#productAlerts').html('<div class="alert green">'+data.message+' <a href="<?php echo site_url('checkout');?>"> <?php echo lang('view_cart');?></a> <i class="close"></i></div>');
                //updateItemCount(data.itemCount);
                cart[0].reset();
                $.post('<?php echo site_url('cart/get-cart-info');?>', '', function(data){
                	 $('#header-cart-info').find('span.rate').html(data.totalprice+' $');  
                	 $('#header-cart-info').find('span.item').html(data.totalItems+' items');            	
                    }, 'json');
            }
            else if(data.error != undefined)
            {
                $('.productDetails').spin(false);
                $('#productAlerts').html('<div class="alert red">'+data.error+' <i class="close"></i></div>');
            }
            btn.attr('disabled', false);
        }, 'json');
    }

    var banners = false;
    $(document).ready(function(){
        banners = $('#banners').html();
    })

    $('.productImages img').click(function(){
        if(banners)
        {
            $.gumboTray(banners);
            $('.banners').gumboBanner($('.productImages img').index(this));
        }
    });

    $('.tabs').gumboTabs();
</script>

<?php if(count($product->images) > 1):?>
<script id="banners" type="text/template">
    <div class="banners">
        <?php
        foreach($product->images as $image):?>
                <div class="banner" style="text-align:center;">
                    <img src="<?php echo base_url('product_photo/'.$image['filename']);?>" style="max-height:600px; margin:auto;"/>
                    <?php if(!empty($image['caption'])):?>
                        <div class="caption">
                            <?php echo $image['caption'];?>
                        </div>
                    <?php endif; ?>
                </div>
        <?php endforeach;?>
        <a class="controls" data-direction="back"><i class="icon-chevron-left"></i></a>
        <a class="controls" data-direction="forward"><i class="icon-chevron-right"></i></a>
        <div class="banner-timer"></div>
    </div>
</script>
<?php endif;?>


<?php if(!empty($product->related_products)):?>
    <div class="page-header" style="margin-top:30px;">
        <h3><?php echo lang('related_products_title');?></h3>
    </div>
    <?php
    $relatedProducts = [];
    foreach($product->related_products as $related)
    {
        $related->images    = json_decode($related->images, true);
        $relatedProducts[] = $related;
    }
    \GoCart\Libraries\View::getInstance()->show('categories/products', ['products'=>$relatedProducts]); ?>

<?php endif;?>