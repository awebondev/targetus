<div class="col-nest">
	<div class="container">
	    <div class="row">
	    <?php foreach ($products as $product) { ?>
	        <div class="col-md-4 col-sm-4 col-xs-12">
        		<div onclick="'+divlink+'" class="categoryItem">
        			<div class="pro-list">
        				<div class="pro-list-img">
        					<a href="#">
        					<?php if($product->images == ''){ ?>
        						<img src="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png" class="img-responsive">
        						<?php }else{ ?>
        						<img src="<?php echo base_url(); ?>/product_photo/uploads/images/medium/<?php echo $product->images; ?>" class="img-responsive">
        						<?php } ?>
        					</a>
        				</div>
        				<div class="price-tag">
        					<a href="#"><?php echo $product->price_1 ?></a>
        				</div>
        				<div class="pro-list-name">
        					<a href="#"><?php echo $product->name ?>'</a>
        				</div>
        				<div class="pro-list-desc"><?php echo $product->name ?></div>
        				<div class="favorite">
        					<ul>
        						<li class="pro-cart"><a href="#"></a></li>
        						<li class="pro-heart"><a href="#"></a></li>
        						<li class="pro-file"><a href="#"></a></li>
        					</ul>
        				</div>
        			</div>
        		</div>
	        </div>
	        	<?php } ?>
	    </div>
	</div>
</div>