<form method="post" id='locationForm' enctype="multipart/form-data" >
   <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
        	<label><?php echo lang('s_title');?></label>
            <?php echo form_input(['name'=>'title', 'class'=>'form-control', 'value'=> $sliderslist->title]);?>
            <input type="hidden" name="idval" value="<?php echo $sliderslist->id ?>" >
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="required"><?php echo lang('s_description');?></label>
            <?php echo form_textarea(['name'=>'description', 'rows'=>'5', 'class'=>'form-control',  'value'=> $sliderslist->description]);?>
        </div>
    </div> 
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="required"><?php echo lang('s_image');?></label>
            <input type="file" name="image" id="image" >
        </div>
    </div> 
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="required"><?php echo lang('s_link');?></label>
            <?php echo form_input(['name'=>'image_link', 'class'=>'form-control', 'value'=> $sliderslist->image_link]);?>
        </div>
    </div>    

    <div class="col-md-12 col-sm-12 col-xs-12">
        <button type="submit" name="submit" class="btn btn-success" value="1">
            <span><img src="<?php echo base_url(); ?>assets/img/submit.png" data-pin-nopin="true"></span>
            Save
        </button>
        <a href="<?php echo base_url() ?>admin/list_location" class="btn btn-danger">
        <span><img src="<?php echo base_url(); ?>assets/img/cancel.png"></span>
        cancel</a>
    </div>          
</form>
   
