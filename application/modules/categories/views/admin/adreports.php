<div class="container">
    <div class="row">
        <!-- <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="dynamic-search">
                <div class="col-md-2 col-sm-12 col-xs-12 no-padding"><h5>Find Your Products</h5></div>

                <div class="col-md-10 col-sm-12 col-xs-12 no-padding">
                <?php foreach (range('a', 'z') as $i)
                echo '<a href="#" onclick="return alphpeticvalue(\''.$i.'\');" >'.$i.'</a>';  ?>&nbsp;<a href="<?php echo base_url() ?>category/all">All</a>
                </div>
            </div>
        </div> -->
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="breadcrumb">                        
                <span><img src="<?php echo base_url(); ?>assets/img/home.png"></span>         
                <h6><?php echo CI::breadcrumbs()->generate(); ?></h6>
            </div>
        </div>        
        <div class="row" id="product-list">
            <div class="col-md-12 col-sm-12 col-xs-12 no-right-pad ver-top">
                <div id="main-container" class="">                    
                    <div class="col-md-2 slp-padding">
                        <h6>Categories</h6> 
                        <span>&nbsp;</span>                                               
                            <?php
                            unset($categories[-1]);
                            unset($categories[0]);

                            if(!empty($categories))
                            {
                                echo '<select class="form-control" id="mycat" name="category_id">';
                                echo '<option value="">'.lang('filter_by_category').'</option>';
                                foreach($categories as $key=>$name)
                                {
                                    echo '<option value="'.$key.'">'.$name.'</option>';
                                }
                                echo '</select>';

                            }?>                        
                    </div>
                    <!-- <div id="tree-container">                        
                    </div> -->
                    <!-- <h6>Size</h6>  -->
                    <div class="col-md-2 slp-padding">
                        <h6>Size</h6> 
                        <span>&nbsp;</span>                                                                                            
                        <select id="size" class="form-control">
                            <option value="">Select Size</option>                            
                            <option value="13mm">13mm</option>
                            <option value="18mm">18mm</option>
                            <option value="24mm">24mm</option>
                            <option value="28.5mm">28.5mm</option>
                            <option value="31mm">31mm</option>
                            <option value="Standard">Standard</option>
                            <option value="F-Shape">F-Shape</option>
                            <option value="Rocket">Rocket</option>
                            <option value="Super_Slim">Super_Slim</option>
                            <option value="Teardrop">Teardrop</option>                            
                        </select>
                    </div>                            
                    <!-- <h6>Color</h6>    -->                               
                    <div class="col-md-2 slp-padding"> 
                        <h6>Color</h6> 
                        <span>&nbsp;</span>                                              
                        <select id="color" class="form-control">
                            <option value="">Select Color</option>
                            <option value="white">white</option>
                            <option value="Clear_Blue">Clear_Blue</option>
                            <option value="Clear_Orange">Clear_Orange</option>
                            <option value="Clear_Red">Clear_Red</option>
                            <option value="D-Black">D-Black</option>
                            <option value="Black">Black</option>
                            <option value="Clear-F">Clear-F</option>
                            <option value="Purple">Purple</option>
                            <option value="Green">Green</option>
                            <option value="Pink">Pink</option>
                        </select>
                    </div>
                    <div class="col-md-2 slp-padding"> 
                        <h6>Other</h6> 
                        <span>&nbsp;</span>                       
                        <select id="othercat" class="form-control" onchange="return filtbyothercat(this.value)">
                            <option value="">Other</option>
                            <option value="Freeship">Freeship</option>
                            <option value="Promo">Promo</option>
                            <option value="Special">Special</option>
                            <option value="Online Salable">Online Salable</option>
                            <option value="Active">Active</option>
                            <option value="SALEABLE">Saleable</option>
                            <option value="PHASEOUT">Phaseout</option>                            
                        </select>
                    </div>
                    <div class="col-md-4 slp-padding">
                        <h6>Price</h6> 
                        <form id="price_form">                            
                            <div class="col-md-5 no-padding"> 
                                <div class="form-group">                       
                                    <label>From</label>
                                    <input type="text" name="st_price" id="st_price" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-5 no-padding">
                                <div class="form-group">
                                    <label>To</label>
                                    <input type="text" name="end_price" id="end_price" class="form-control">
                                </div>
                            </div>                            
                            <div class="col-md-2 slp-padding">                        
                                <a onclick="return pricebysearch('searinput-id');" style="cursor:pointer" class="close-btn" id="priceview">
                                    <img src="<?php echo base_url(); ?>/assets/img/close.png" data-pin-nopin="true">
                                </a>
                            </div>           
                        </form>   
                    </div>
                   <!--  <div class="display">
                        <h6>Other</h6>
                    </div>
                    <div class="brands">                    
                        <ul id="" class="checktree">                            
                            <li>
                                <input type="checkbox" class="checktypeall" id="freeship" name="checkboxall[]" value="Freeship">  
                                <label for="freeship">Freeship</label>                         
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="promo" name="checkboxall[]" value="Promo">   
                                <label for="promo">Promo</label>                          
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="special" name="checkboxall[]" value="Special"> 
                                <label for="special">Special</label> 
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="online_salable" name="checkboxall[]" value="Online Salable"> 
                                <label for="online_salable">Online Salable</label> 
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="online_item" name="checkboxall[]" value="Online Item"> 
                                <label for="online_item">Online Item</label> 
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="active" name="checkboxall[]" value="Active"> 
                                <label for="active">Active</label> 
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="saleable" name="checkboxall[]" value="SALEABLE"> 
                                <label for="saleable">Saleable</label> 
                            </li>
                            <li>
                                <input type="checkbox" class="checktypeall" id="phaseout" name="checkboxall[]" value="PHASEOUT"> 
                                <label for="phaseout">Phaseout</label> 
                            </li>
                        </ul>
                    </div>  --> 
                    <div class="col-md-8 no-padding" id='product-listing-div'>
                        <?php include(__DIR__.'/reports_details.php');?>
                         <input type="hidden" name="slug" id="slug" value="<?php echo $slug; ?>">
                    </div>  
                </div>
            </div>                    
        </div>
        <!-- <div class="catview">
        </div> -->
        <input type="hidden" id="total_productshidden" value="<?php echo $total_products ?>">
        <input type="hidden" id="slughidden" value="<?php echo $slug ?>">
        <!-- <input type="hidden" id="" value="">
        <input type="hidden" id="" value="">
        <input type="hidden" id="" value="">
        <input type="hidden" id="" value=""> -->
        <form id="next_form">            
            <input type="hidden" name="st_price_sub" id="st_price_sub" value="<?php echo $stprice; ?>" >
            <input type="hidden" name="end_price_sub" id="end_price_sub" value="<?php echo $endprice; ?>" >
            <input type="hidden" name="sr_slug" id="sr_slug" value="<?php echo $slug; ?>" >
            <input type="hidden" name="sr_sort" id="sr_sort" value="<?php echo $sort; ?>" >
            <input type="hidden" name="sr_dir" id="sr_dir" value="<?php echo $dir; ?>" >
            <input type="hidden" name="sr_page" id="sr_page" value="<?php echo $pagect=$page; ?>" >
            <input type="hidden" name="sr_serach_input" id="sr_serach_input" value="<?php echo $sr_serach_input; ?>" >
            <input type="hidden" name="sr_droporder_list" id="sr_droporder_list" value="<?php echo $droporder_list; ?>" >
            <input type="hidden" name="sr_alpha_list" id="sr_alpha_list" value="<?php echo $sr_alpha_list; ?>" >
            <input type="hidden" name="allctegary[]" id="allctegary" value="" >
            <input type="hidden" name="allprductctegary[]" id="allprductctegary" value="" >
            <input type="hidden" name="prsize" id="prsize" value="" >
            <input type="hidden" name="prcolor" id="prcolor" value="" >
            <input type="hidden" name="prmonth" id="prmonth" value="" >
            <input type="hidden" name="pryear" id="pryear" value="" >
            <!-- <input type="hidden" name="sr_priceby2" id="sr_priceby2" value="" > -->
        </form>        
    </div>
</div>  


<script>
function allvaluetoget(sj){
        testHoldon('sk-dot');
        if(sj == 'nextid' ){

            //alert('his');
        var nextval = $('#nextsid').attr('nextcnt');
        var curpage = parseInt(nextval)+1;
                $("#sr_page").val(curpage);
            }

        //alert(nextval);
        $.post("<?php echo base_url().'admin/searchallproduct' ?>", $('#next_form').serialize(), function(data){
            //alert('mine');
            console.log(data);
            HoldOn.close();
                $('#product_total_sale').html(data);
            
                
            });
        //}
        HoldOn.close();
    }
    function searinputchanges(sj){
       var seval = $("#searchinall").val();
       $("#sr_serach_input").val(seval); 
       allvaluetoget('serachinputva');
    }

    function alphpeticvalue(sj){
        //alert(sj);
       //var seval = $("#searchinall").val();
       $("#sr_alpha_list").val(sj); 
       allvaluetoget('serachinputva');
    }

    $("#search_form").submit(function(event) {
        var seval = $("#searchinall").val();
       $("#sr_serach_input").val(seval); 
       allvaluetoget('serachinputva');
        //searinputchanges('searinput-id');
    })

    function pricebysearch(){
       var stprcice = $("#st_price").val();
       var endprice = $("#end_price").val();
       $("#st_price_sub").val(stprcice); 
       $("#end_price_sub").val(endprice); 
       if($("#end_price_sub").val() == '' || $("#end_price_sub").val() <= $("#st_price_sub").val()){
        $("#end_price_sub").val(1000000);
       }
       //alert('worked');
       allvaluetoget('serachinputva');
    }

    /*$('.checktypeall').on('click', function() { 
        
        if($('#'+this.id).hasClass('checked')){
           $('#'+this.id).removeClass('checked'); 
       }else{
            $('#'+this.id).addClass('checked');
       }
        var values = $("input[type=checkbox][name='checkboxall[]']:checked").map(function() {
        return this.value;
    }).get();        
        $("#allctegary").val(values);
        $("#sr_page").val('1');     
        allvaluetoget('allcatlist');
    });*/

    function filtbyothercat(sdsd){       
        $("#allctegary").val(sdsd);
        $("#sr_page").val('1');
        allvaluetoget('othercat');
    }

    function filtbymoth(sdsd){
        $('#prmonth').val(sdsd);
         allvaluetoget('prmonth');
    }
    function filtbyear(sdsd){
        $('#pryear').val(sdsd);
         allvaluetoget('pryear');
    }


    /*$(document).on('click','.checkbox',function(){
        //alert('SJ');
        var values = $("input[type=checkbox][name='selectcatej[]']:checked").map(function() {
            return this.value;
        }).get();
        
        $("#allprductctegary").val(values);
        $("#sr_page").val('1');
        //console.log(values);   
        if(values != ''){ 
            //alert('sj1');
            $("#sr_slug").val(' ');
        }else{
            //alert('sj2');
            $("#sr_slug").val('all');
        }
        allvaluetoget('allcatlist');
    });
*/
     $('#mycat').on('change', function() {
        
        $('#allprductctegary').val(this.value);
        $("#sr_page").val('1');
        //console.log(values);   
        if(this.value != ''){ 
            //alert('sj1');
            $("#sr_slug").val(' ');
        }else{
            //alert('sj2');
            $("#sr_slug").val('all');
        }
         allvaluetoget('dropcate');

    });

    
    $('#size').on('change', function() {
        
        $('#prsize').val(this.value);
         allvaluetoget('dropsize');

    });
    $('#color').on('change', function() {
        
        $('#prcolor').val(this.value);
         allvaluetoget('dropcolor');

    });

$(document).ready(function () {

   if($("#sr_slug").val() == 'all'){
        allvaluetoget('allprdctsale');
   }


    /*$('#priceview').on('click', function(){
          alert('save-order-address');
            //$('.cartSummary').spin();
            //e.preventDefault();
            $.post("<?php echo base_url().'search/searchprdtbyprice/' ?>", $('#price_form').serialize(), function(data){
                $('#product-listing-div').html(data);

                
            });
          
    
      });*/





    















    /*$('#sort-product').on('change', function() {
              window.location.href = this.value;
    });*/

    /*$(document).on('click','.checkbox',function(){
    getCategoriesProduct();
    });*/
    categories();
    function categories() {
        $.ajax({
            type:'POST',
            data: {'slug':$("#slug").val()},
            url:"<?php echo site_url('categoriescontroller/getcategories'); ?>",
            success : function(data){
                console.log(data);
                var cc = JSON.parse(data);
               // getCategoriesProduct();
                $('#tree-container').highCheckTree({
                    data: cc,
                    //onCheck: getCategoriesProduct,
                });
            }
        });
    }

    var container = $(this), $tree = this;

    function getCategoriesProduct(treesdata){
                $(".catview").html('');
                var val = [];
                $('input[type="checkbox"].checked').each(function(i){
                  val[i] = $(this).val();
                });
                //console.log(val);
                $.ajax({
                type:'POST',
                data: {'cate_id':val,'slug':$("#slughidden").val()},
                dataType: "html",
                url:"<?php echo site_url('categoriescontroller/getCategoriesProduct'); ?>",
                success : function(data){
                	$('#product-listing-div').html(data);
                    /*var cc = JSON.parse(data);
                    var lookup = [];
                    //var ilistj = cc.item;
                    for (var key in cc ) {
                         var ccs = JSON.parse(cc[key].images);
                         //var cccs = JSON.parse(ccs);
                        for (var keys in ccs ) {

                            var imgpath = ccs[keys].filename;
                        }
                     if(cc[key].images){
                       var imgpathj ="<?php echo base_url(); ?>/product_photo/uploads/images/medium/"+imgpath; 
                     }else{
                       var imgpathj ="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png";
                     }
                    var catid = cc[key].id;
                    //alert($.inArray(catid,lookup));
                    if ($.inArray(catid,lookup)==-1){
                    lookup.push(cc[key].id);
                    var divpathlnk = "<?php echo site_url('/product/'); ?>/"+cc[key].slug;

                    var divlink = "window.location = '"+divpathlnk+"'"; 
                    $(".catview").append('<div class="col-md-4 col-sm-4 col-xs-12"><div onclick="'+divlink+'" class="categoryItem"><div class="pro-list"><div class="pro-list-img"><a href="#"><img src="'+imgpathj+'" class="img-responsive"></a></div><div class="price-tag"><a href="#">$'+cc[key].saleprice_1+'</a></div><div class="pro-list-name"><a href="#">'+cc[key].name+'</a></div><div class="pro-list-desc">Gold Ring.</div><div class="favorite"><ul><li class="pro-cart"><a href="#"></a></li><li class="pro-heart"><a href="#"></a></li><li class="pro-file"><a href="#"></a></li></ul></div></div></div></div>');

                    }                    
                }  */                 
                }
            });
        }
});
</script>




