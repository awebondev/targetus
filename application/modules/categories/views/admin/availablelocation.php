<form method="post" id='locationForm' >
   <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
        	<label><?php echo lang('ltitle');?></label>
            <?php echo form_input(['name'=>'title', 'class'=>'form-control', 'value'=> $locationval->title]);?>
            <input type="hidden" name="idval" value="<?php echo $id ?>" >
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="required"><?php echo lang('ltitle_link');?></label>
            <?php echo form_input(['name'=>'title_link', 'class'=>'form-control', 'value'=> $locationval->title_link]);?>
        </div>
    </div> 

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="required"><?php echo lang('lphone');?></label>
            <?php echo form_input(['name'=>'phone', 'class'=>'form-control', 'maxlength' => '15', 'value'=> $locationval->phone]);?>
        </div>
    </div> 

    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="required"><?php echo lang('laddress');?></label>
            <?php echo form_textarea(['name'=>'address', 'rows'=>'5', 'class'=>'form-control',  'value'=> $locationval->address]);?>
        </div>
    </div> 
    <div class="col-md-6 col-sm-6 col-xs-12">
    <?php $aa = $locationval->icons;
            $rr = explode(',', $aa);
            $check1= ''; $check2= ''; $check3= ''; $check4= '';
            foreach($rr as $iconsdata){                
                if($iconsdata == 1){
                    $check1= 'checked';
                }else if($iconsdata == 2){
                    $check2= 'checked';
                }else if($iconsdata == 3){
                    $check3= 'checked';
                }else if($iconsdata == 4){
                    $check4= 'checked';
                }else if($iconsdata == 5){
                    $check5= 'checked';
                }else if($iconsdata == 6){
                    $check6= 'checked';
                }
            }
    ?> 
        <div class="form-group">                    
            <input <?php echo $check1; ?> id="checkbox-1" type="checkbox" value="1" name="icons[]">
            <label for="checkbox-1"><img src="http://search.ca.dartslive.com/common2/img/icn_premium.png" width="54" height="19"></label>
        </div>
        <div class="form-group">
            <input <?php echo $check2; ?> id="checkbox-2" type="checkbox" value="2" name="icons[]">
            <label for="checkbox-2"><img src="http://search.ca.dartslive.com/common2/img/icn_plus10.gif" width="54" height="19"></label>
        </div>
        <div class="form-group">            
            <input <?php echo $check3; ?> id="checkbox-3" type="checkbox" value="3" name="icons[]">
            <label for="checkbox-3"><img src="http://search.ca.dartslive.com/common2/img/icn_live2s.gif" width="20" height="19"></label>
        </div>
        <div class="form-group">           
            <input <?php echo $check4; ?> id="checkbox-4" type="checkbox" value="4" name="icons[]">
            <label for="checkbox-4"><img src="http://search.ca.dartslive.com/common2/img/icn_touch_s.gif" width="20" height="19"></label>
        </div>
        <div class="form-group">            
            <input <?php echo $check5; ?> id="checkbox-5" type="checkbox" value="5" name="icons[]">
            <label for="checkbox-5"><img src="http://search.ca.dartslive.com/common2/img/icn_league_s.gif" width="20" height="19"></label>
        </div>
        <div class="form-group">            
            <input <?php echo $check6; ?> id="checkbox-6" type="checkbox" value="6" name="icons[]">
            <label for="checkbox-6"><img src="http://search.ca.dartslive.com/common2/img/icn_plus5.gif" width="54" height="19"></label>
        </div>
    </div> 

    <div class="col-md-12 col-sm-12 col-xs-12">
        <button type="submit" name="submit" class="btn btn-success" value="1">
            <span><img src="<?php echo base_url(); ?>assets/img/submit.png" data-pin-nopin="true"></span>
            Save
        </button>
        <a href="<?php echo base_url() ?>admin/list_location" class="btn btn-danger">
        <span><img src="<?php echo base_url(); ?>assets/img/cancel.png"></span>
        cancel</a>
    </div>          
</form>
   
