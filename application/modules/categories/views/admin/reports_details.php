         
        <!-- <select class="cate-select">
            <option>Test-1</option>
            <option>Test-1</option>
            <option>Test-1</option>
        </select> -->
       <?php /*
            $sortbynameor = '';
            $sortbynamedes = '';
            $sortbyprcasc = '';
            $sortbyprcdes = '';
        if($sort == 'name' && $dir == 'ASC'){ 
            $sortbynameor = 'selected';
        }else if($sort == 'name' && $dir == 'DESC'){
            $sortbynamedes = 'selected';
        }else if($sort == 'price_1' && $dir == 'ASC'){
            $sortbyprcasc = 'selected';
        }else if($sort == 'price_1' && $dir == 'DESC'){
            $sortbyprcdes = 'selected';
        }  ?>
        <select class="form-control" id="sort-product" onchange="return serchbydropdownne(this.value)" >
            <option <?php echo $sortbynameor; ?> value="1" >Sort by name A to Z</option>
            <option <?php echo $sortbynamedes; ?> value="2" >Sort by name Z to A</option>
            <option <?php echo $sortbyprcasc; ?> value="3" >Sort by price Low to High</option>
            <option <?php echo $sortbyprcdes; ?> value="4" >Sort by price High to Low</option>
        </select> */ ?>
        <div class="col-md-6 slp-padding">   
            <h6>Month</h6>
            <select name="summa" class="form-control" onchange="return filtbymoth(this.value)">
            	<option value="">Select Month</option>
            	<?php 
            		$months = array(
            			"01"=>"January",
            			"02"=>"February",
            			"03"=>"March",
            			"04"=>"April",
            			"05"=>"May",
            			"06"=>"june",
            			"07"=>"july",
            			"08"=>"august",
            			"09"=>"September",
            			"10"=>"October",
            			"11"=>"November",
            			"12"=>"December",
            		);
    				foreach ($months as $key => $value) {
    			    echo "<option value=\"" . $key . "\">" . $value . "</option>";
    			}
    			?>
            </select>
        </div>
        <div class="col-md-6 slp-padding"> 
            <h6>Year</h6>
            <select name="summa" class="form-control" onchange="return filtbyear(this.value)">
            	<option value="">Select Year</option>
            	<?php         		
    				for($i=2015;$i<2065; $i++) {
    			    echo "<option value=\"" . $i . "\">" . $i . "</option>";
    			}
    			?>
            </select>
        </div>
    </div>
</div>
<?php if(count($products) == 0):?>
    <h2>
        <?php echo lang('no_products');?>
    </h2>
<?php endif;?>
<div class="row">
    <div class="col-md-12">Total sale :</div>
    <div class="col-md-12" id="product_total_sale"></div>
</div>
