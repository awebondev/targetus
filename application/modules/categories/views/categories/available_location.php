<div class="container">
    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <div class="breadcrumb">                        
                <span><img src="<?php echo base_url(); ?>assets/img/home.png"></span>         
                <h6>Available Location/location list</h6>
            </div>
        </div>

        <div class="address" id="event_location">
            <?php foreach($locationval as $avloc){ ?>
                <div class="col-md-12 col-sm-12 col-xs-12 location">
                    <div class="row">
                        <div class="col-md-9">
                            <a href="<?php echo $avloc->title_link ?>"><?php echo $avloc->title ?></a>
                            <p> <i class="fa fa-map-marker"></i> <?php echo $avloc->address ?></p>
                            <p> <i class="fa fa-phone-square"></i> <?php echo $avloc->phone ?></p>
                        </div>
                        <div class="col-md-3">
                            <div class="">
                                <ul>
                                    <?php
                                    $exploddata = explode(',', $avloc->icons);
                                     foreach($exploddata as $iconsdata){ 
                                        if($iconsdata == 1){
                                            echo '<li><img src="http://search.ca.dartslive.com/common2/img/icn_premium.png" width="54" height="19"></li>';
                                        }else if($iconsdata == 2){
                                            echo '<li><img src="http://search.ca.dartslive.com/common2/img/icn_plus10.gif" width="54" height="19"></li>';
                                        }else if($iconsdata == 3){
                                            echo '<li><img src="http://search.ca.dartslive.com/common2/img/icn_live2s.gif" width="20" height="19"></li>';
                                        }else if($iconsdata == 4){
                                            echo '<li><img src="http://search.ca.dartslive.com/common2/img/icn_touch_s.gif" width="20" height="19"></li>';
                                        }else if($iconsdata == 5){
                                            echo '<li><img src="http://search.ca.dartslive.com/common2/img/icn_league_s.gif" width="20" height="19"></li>';
                                        }else if($iconsdata == 6){
                                            echo '<li><img src="http://search.ca.dartslive.com/common2/img/icn_plus5.gif" width="54" height="19"></li>';
                                        } 
                                        ?>
                                    <?php } ?>                              
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>




