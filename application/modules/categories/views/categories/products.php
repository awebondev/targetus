<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12" id="filter">
                <div class="col-md-12">
                <?php if($sr_alpha_list){
                    echo "<p style='color:green;border:3px dotted green;width:250px;padding:10px;'>Showing results  starting with '<span style='font-size:20px;'>".$sr_alpha_list."</span>'</p>";
                    } ?>                    
                </div>
                <div class="col-md-4 col-sm-3 col-xs-12 ">
                    <?php if($per_page*$page >= $total_products){
                    $productend_perpage = $total_products;   }else{
                    $productend_perpage = $per_page*$page;
                 } ?>
                    <p class="pagination">Showing <?php if($total_products!=0  && $total_products != -1){ echo $per_page*$page+1-$per_page.'-'.$productend_perpage.' of '.$total_products; }else{ echo '0 of 0'; } ?></p>
                </div>
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="">
                        <span class="filter">Sort by :</span>
                        <!-- <select class="cate-select">
                            <option>Test-1</option>
                            <option>Test-1</option>
                            <option>Test-1</option>
                        </select> -->
                       <?php
                            $sortbynameor = '';
                            $sortbynamedes = '';
                            $sortbyprcasc = '';
                            $sortbyprcdes = '';
                        if($sort == 'name' && $dir == 'ASC'){ 
                            $sortbynameor = 'selected';
                        }else if($sort == 'name' && $dir == 'DESC'){
                            $sortbynamedes = 'selected';
                        }else if($sort == 'price_1' && $dir == 'ASC'){
                            $sortbyprcasc = 'selected';
                        }else if($sort == 'price_1' && $dir == 'DESC'){
                            $sortbyprcdes = 'selected';
                        }  ?>
                        <select class="form-control" id="sort-product" onchange="return serchbydropdownne(this.value)" >
                            <option <?php echo $sortbynameor; ?> value="1" >Sort by name A to Z</option>
                            <option <?php echo $sortbynamedes; ?> value="2" >Sort by name Z to A</option>
                            <option <?php echo $sortbyprcasc; ?> value="3" >Sort by price Low to High</option>
                            <option <?php echo $sortbyprcdes; ?> value="4" >Sort by price High to Low</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12">
                    <p class="filter">Page <?php if($total_products != 0  && $total_products != -1){ echo $page.' of '.$totpage = ceil($total_products/$per_page); }else{ echo 'Page 0 of 0'; } ?></p>
                    <?php (empty($total_products))? $totpage = 1:''; ?>
                    <?php if($page != $totpage){ 
                        if($total_products!=0  && $total_products != -1){ ?>
                             <a class="next-prev" id="nextsid" nextcnt="<?php echo $cpage = $page; ?>" href="#" onclick="return allvaluetoget('nextid');">Next</a>

                    <?php } } ?>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive" data-pin-nopin="true">
            </div>
        </div>
<?php if(count($products) == 0):?>

    <h2 style="margin:50px 0px; text-align:center; line-height:30px;">
        <?php echo lang('no_products');?>
    </h2>

<?php else:?>

    <div class="categoryItems element">
    <?php /*print_r($products); exit();*/ foreach($products as $product):  ?>
        <div class="col-md-4 col-sm-6 col-xs-12">
            <?php
            $photo  = theme_img('no_picture.png');

            if(!empty($product->images[0]) && $photo['filename'] != '')
            {
                $primary    = $product->images[0];
                foreach($product->images as $photo)
                {
                    if(isset($photo['primary']))
                    {

                        $primary    = $photo;
                    }
                }
                if($photo['filename']){
                    $photo  = base_url('product_photo/'.$primary['filename']);
                }else{
                    $photo  = theme_img('no_picture.png');
                }
            }
            ?>
            <div class="categoryItem" >
                <?php if((bool)$product->track_stock && $product->quantity < 1 && config_item('inventory_enabled')):?>
                    <div class="categoryItemNote yellow"><?php echo lang('out_of_stock');?></div>
                <?php elseif($product->saleprice > 0):?>
                    <div class="categoryItemNote red"><?php //echo lang('on_sale');?></div>
                <?php endif;?>
                <div class="pro-list">
                    <div class="product_main_img">
                        <div class="pro-list-img">
                                <a href="<?php echo site_url('/product/'.$product->slug); ?>"><img src="<?php echo $photo;?>" class="img-responsive"></a>
                        </div>
                    </div>
                    <?php if(!$product->is_giftcard): //do not display this if the product is a giftcard?>
                        <div class="price-tag">
                            <a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');">
                            <?php echo ( $product->saleprice>0?format_currency($product->saleprice):format_currency($product->price) );?></a>
                        </div>
                    <?php endif;?>
                    <div class="pro-list-name">
                        <a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');">
                        <?php echo $product->name;?></a>
                    </div>
                    <div class="pro-list-desc">
                        <?php echo $product->description; ?>
                    </div>
                    <div class="favorite">
                        <ul>
                            <li class="pro-cart"><a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');"></a></li>
                            <!-- <li class="pro-heart"><a href="#"></a></li>
                            <li class="pro-file"><a href="#"></a></li> -->
                        </ul>                               
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
    </div>

<?php endif;?>
<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive" data-pin-nopin="true">
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12" id="filter">
                <div class="col-md-5 col-sm-3 col-xs-12 ">
                    <p class="pagination">Showing <?php if($total_products!=0  && $total_products != -1){ echo $per_page*$page+1-$per_page.'-'.$productend_perpage.' of '.$total_products; }else{ echo '0 of 0'; } ?></p>
                </div>
                
                <div class="col-md-4 col-sm-6 col-xs-12">
                <!-- <div class="pull-right">
                    <span class="filter">Sort by :</span>
                    <select class="cate-select">
                        <option>Test-1</option>
                        <option>Test-1</option>
                        <option>Test-1</option>
                    </select>
                </div> -->
                </div>
                <div class="col-md-3 col-sm-3 col-xs-12 pull-right">
                    <p class="filter">Page <?php if($total_products!=0  && $total_products != -1){ echo $page.' of '.$totpage = ceil($total_products/$per_page); }else{ echo 'Page 0 of 0'; } ?></p>
                    <?php (empty($total_products))? $totpage = 1:''; ?>
                    <?php if($page != $totpage){ 
                        if($total_products!=0  && $total_products != -1){ ?>
                    <a class="next-prev" href="<?php echo base_url().'category/'.$slug.'/'.$sort.'/'.$dir; ?>/<?php echo $page+1; ?>">Next</a>
                    <?php } } ?>
                </div>
            </div>
        </div>