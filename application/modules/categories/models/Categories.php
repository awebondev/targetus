<?php
/**
 * Categories Class
 *
 * @package GoCart
 * @subpackage Models
 * @category Categories
 * @author Clear Sky Designs
 * @link http://gocartdv.com
 */

Class Categories
{

    var $tiered;
    //public $productlistt = array();
    public function __construct()
    {
        $this->tiered = [];
        $this->customer = \CI::Login()->customer();
        $this->get_categories_tiered();
    }

    public function tier($parent_id)
    {
        if(isset($this->tiered[$parent_id]))
        {
            return $this->tiered[$parent_id];
        }
        else
        {
            return false;
        }
    }

    public function getBySlug($slug)
    {
        foreach($this->tiered['all'] as $c)
        {
            if($c->slug == $slug)
            {  
                return $c;
                break;
            }
        }
        return false;
    }

    public function get($slug, $sort, $direction, $page, $products_per_page)
    {
        if($slug != NULL && $slug != 'all'){
            //get the category by slug
            $category = $this->getBySlug($slug);

            //if the category does not exist return false
            if(!$category || !$category->{'enabled_'.$this->customer->group_id})
            {
                return false;
            }

            //create view variable
            $data['page_title'] = $category->name;
            $data['meta'] = $category->meta;
            $data['seo_title'] = (!empty($category->seo_title))?$category->seo_title:$category->name;
            $data['category'] = $category;

            $data['total_products'] = CI::Products()->count_products($category->id);
            
            $catcunt = \CI::Categories()->get_categories($category->id);
            $productlistt = [];
            $dproducts = array();
            //$dproductss = array();
        $sees_create = \CI::session()->set_userdata('product_list',$productlistt);
            $dproducts = CI::Categories()->ConstructHierarchy($dd = null,$catcunt,$ss=null,$products_per_page,$page,$sort, $direction);

            $productlistt = CI::Products()->getProducts($dproducts, $products_per_page, $page, $sort, $direction);
            
            $data['products'] = $productlistt['products'];
            $data['total_products'] = $productlistt['productcnt'];
            

        }else{
                        
            //$data['products'] = CI::Products()->getProducts($category_id = false, $products_per_page, $page, $sort, $direction);
            $stprice=0;
            $endprice=1000000; 
            $data['products'] = CI::Products()->search_all_products($stprice, $endprice, $category_id='all', $limit=false, $offset=false, $by=false, $sort=false,$sr_serach_input=null,$sr_alpha_list=null ,$allctegary=null, $category_idd=false,$pr_size=false, $pr_color=false);
            $data['total_products'] = count($data['products']);
            
             
        }

        return $data;
    }



    
     public function ConstructHierarchy($parent=null,$childrens=[],$slug=false,$products_per_page=null,$page=null,$sort=null, $direction=null){
        $ChilderenArray = [];       

        foreach($childrens as $childrancat) {
            $childrenitemClass = new \stdClass();
            $uichildrenCategoryClass = new \stdClass();
            $uichildrenCategoryClass->id=$childrancat->id;
            $uichildrenCategoryClass->label=$childrancat->name;
            $childrenitemClass->item = $uichildrenCategoryClass;
            if($childrancat->slug == $slug || $slug === true)
                $uichildrenCategoryClass->checked=true;
            else
                $uichildrenCategoryClass->checked=false;
            $ChilderenArray[]=  $childrenitemClass;
            $uichildrenCategoryClass->id;
            
                $dd = \CI::session()->userdata('product_list');
                array_push($dd,$uichildrenCategoryClass->id);                            

                $sees_create = \CI::session()->set_userdata('product_list',$dd);
             $subCategories = \CI::Categories()->get_categories($uichildrenCategoryClass->id);
             if(!empty($subCategories)){
                $this->ConstructHierarchy($childrenitemClass,$subCategories,$uichildrenCategoryClass->checked,$products_per_page,$page,$sort, $direction);
             }
        }
        if($parent !=  null){
            $parent->children = $ChilderenArray;
        }
        return $dd = \CI::session()->userdata('product_list');
    }


    public function get_categories($parent = false)
    {
        if ($parent !== false)
        {
            CI::db()->where('parent_id', $parent);
        }
        CI::db()->select('id');
        CI::db()->order_by('categories.sequence', 'ASC');
        
        //this will alphabetize them if there is no sequence
        CI::db()->order_by('name', 'ASC');
        $result = CI::db()->get('categories');
        
        $categories = [];
        foreach($result->result() as $cat)
        {
            $categories[] = $this->find($cat->id);
        }
        
        return $categories;
    }
   
    public function get_main_categories($admin = false, $limit = false,$catid = false)
    {
        if(!$admin && !empty($this->tiered))
        {
            return $this->tiered;
        }

        if(!$admin)
        {
            CI::db()->where('enabled_'.$this->customer->group_id, 1);
        }
        
        CI::db()->order_by('sequence');
        CI::db()->order_by('name', 'ASC');
        
        $categories = CI::db()->limit($limit)->get('categories')->result();
        $results = [];
        $results['all'] = [];
        foreach($categories as $category) {

            // Set a class to active, so we can highlight our current category
            if(CI::uri()->segment(2) == $category->slug && CI::uri()->segment(1) == 'category') {
                $category->active = true;
            } else {
                $category->active = false;
            }
            $results['all'][$category->id] = $category;
            $results[$category->parent_id][$category->id] = $category;
        }
        
        if(!$admin)
        {
            //$this->tiered = $results;
        }
        return $results;
    }
    public function get_categories_tiered($admin = false)
    {
        if(!$admin && !empty($this->tiered))
        {
            return $this->tiered;
        }

        if(!$admin)
        {
            CI::db()->where('enabled_'.$this->customer->group_id, 1);
        }
        
        CI::db()->order_by('sequence');
        CI::db()->order_by('name', 'ASC');
        $categories = CI::db()->get('categories')->result();
        $results = [];
        $results['all'] = [];
        foreach($categories as $category) {

            // Set a class to active, so we can highlight our current category
            if(CI::uri()->segment(2) == $category->slug && CI::uri()->segment(1) == 'category') {
                $category->active = true;
            } else {
                $category->active = false;
            }
            $results['all'][$category->id] = $category;
            $results[$category->parent_id][$category->id] = $category;
        }
        
        if(!$admin)
        {
            $this->tiered = $results;
        }
        return $results;
    }   
    
    public function getCategoryOptionsMenu($hideId = false)
    {
        $cats = $this->get_categories_tiered(true);
        $options = [-1 => lang('hidden'), 0 => lang('top_level_category')];
        $listCategories = function($parent_id, $sub='') use (&$options, $cats, &$listCategories, $hideId) {
            
            if(isset($cats[$parent_id]))
            {
                foreach ($cats[$parent_id] as $cat)
                {
                    //if this matches the hide id, skip it and all it's children
                    if(!$hideId || $cat->id != $hideId)
                    {
                        $options[$cat->id] = $sub.$cat->name;

                        if (isset($cats[$cat->id]) && sizeof($cats[$cat->id]) > 0)
                        {
                            $sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
                            $sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
                            $listCategories($cat->id, $sub2);
                        }
                    }
                }
            }
            
        };
        
        $listCategories(-1);
        $listCategories(0);
        
        return $options;
    }
    
    public function slug($slug)
    {
        return CI::db()->get_where('categories', array('slug'=>$slug))->row();
    }

    public function find($id)
    {
        return CI::db()->get_where('categories', array('id'=>$id))->row();
    }
    
    public function get_category_products_admin($id)
    {
        CI::db()->order_by('sequence', 'ASC');
        $result = CI::db()->get_where('category_products', array('category_id'=>$id));
        $result = $result->result();
        
        $contents = [];
        foreach ($result as $product)
        {
            $result2 = CI::db()->get_where('products', array('id'=>$product->product_id));
            $result2 = $result2->row();
            
            $contents[] = $result2; 
        }
        
        return $contents;
    }
    
    public function get_category_products($id, $limit, $offset)
    {
        CI::db()->order_by('product_id', 'ASC');
        $result = CI::db()->get_where('category_products', array('category_id'=>$id), $limit, $offset);
        $result = $result->result();
        
        $contents = [];
        $count = 1;
        foreach ($result as $product)
        {
            $result2 = CI::db()->select('*, saleprice_'.$this->customer->group_id.' as saleprice, price_'.$this->customer->group_id.' as price')->get_where('products', array('id'=>$product->product_id));
            $result2 = $result2->row();
            
            $contents[$count] = $result2;
            $count++;
        }
        
        return $contents;
    }

    public function save($category)
    {
        if ($category['id'])
        {
            CI::db()->where('id', $category['id']);
            CI::db()->update('categories', $category);
            
            return $category['id'];
        }
        else
        {
            CI::db()->insert('categories', $category);
            return CI::db()->insert_id();
        }
    }
    
    public function delete($id)
    {
        CI::db()->where('id', $id);
        CI::db()->delete('categories');

        //update child records to hidden
        CI::db()->where('parent_id', $id)->set('parent_id', -1)->update('categories');
        
        //delete references to this category in the product to category table
        CI::db()->where('category_id', $id);
        CI::db()->delete('category_products');
    }

    /*
    * check if slug already exists
    */

    public function validate_slug($slug, $id=false, $counter=false)
    {
        CI::db()->select('slug');
        CI::db()->from('categories');
        CI::db()->where('slug', $slug.$counter);
        if ($id)
        {
            CI::db()->where('id !=', $id);
        }
        $count = CI::db()->count_all_results();

        if ($count > 0)
        {
            if(!$counter)
            {
                $counter = 1;
            }
            else
            {
                $counter++;
            }
            return $this->validate_slug($slug, $id, $counter);
        }
        else
        {
            return $slug.$counter;
        }
    }

    public function availaction_insert($adata){
        CI::db()->insert('available_location', $adata);
        return CI::db()->insert_id();
    }
    public function availaction_update($id, $adata){
        CI::db()->where('id', $id);
        return $update = CI::db()->update('available_location', $adata);
    }
    public function select_lcation($id=null){
        CI::db()->select('*');
        CI::db()->from('available_location');        
        if($id!=null){
           CI::db()->where('id',$id); 
           return $resultr = CI::db()->get()->row();
        }else{
           return $resultr = CI::db()->get()->result(); 
       }        
    }

    public function select_lcationbyuser(){
        CI::db()->select('*');
        CI::db()->from('available_location');
        CI::db()->where('status','1');         
        return $resultr = CI::db()->get()->result();         
    }

    public function delete_lcation($id,$status){
        $data = array(
            'status' => ($status==1)? '0':'1'
            );
        CI::db()->where('id', $id);
        //return $delloc=CI::db()->delete('available_location');
         return $update = CI::db()->update('available_location', $data);

    }

    public function saveslide($id,$data){
        if($id){
            CI::db()->where('id', $id);
            return $update = CI::db()->update('sliders', $data);
        }else{
            CI::db()->insert('sliders', $data);
            return CI::db()->insert_id();
        }
    }

    public function sliderlist($id=null){
        CI::db()->select('*');
        CI::db()->from('sliders');        
        if($id!=null){
           CI::db()->where('id',$id); 
           return $resultr = CI::db()->get()->row();
        }else{
           return $resultr = CI::db()->get()->result(); 
       }        
    }
    public function sliderdelete($id){
        CI::db()->where('id', $id);
        return $delloc=CI::db()->delete('sliders');
    }
    

}