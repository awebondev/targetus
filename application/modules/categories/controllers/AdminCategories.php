<?php namespace GoCart\Controller;
/**
 * AdminCategories Class
 *
 * @package GoCart
 * @subpackage Controllers
 * @category AdminCategories
 * @author Clear Sky Designs
 * @link http://gocartdv.com
 */

class AdminCategories extends Admin { 
    
    function __construct()
    { 
        parent::__construct();
        
        \CI::auth()->check_access('Admin', true);
        \CI::lang()->load('categories');
        \CI::load()->model('Categories');
    }
    
    function index()
    {
        $data['groups'] = \CI::Customers()->get_groups();
        $data['page_title'] = lang('categories');
        $data['categories'] = \CI::Categories()->get_categories_tiered(true);
        
        $this->view('categories', $data);
    }

    function form($id = false)
    {

        $data['groups'] = \CI::Customers()->get_groups();
        
        $config['upload_path'] = 'product_photo/uploads/images/full';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_width'] = '1024';
        $config['max_height'] = '768';
        $config['encrypt_name'] = true;
        \CI::load()->library('upload', $config);
        
        
        $this->category_id = $id;
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');
        \CI::form_validation()->set_error_delimiters('<div class="error">', '</div>');
        
        $data['categories'] = \CI::Categories()->getCategoryOptionsMenu($id);
        $data['page_title'] = lang('category_form');
        
        //default values are empty if the customer is new
        $data['id'] = '';
        $data['name'] = '';
        $data['slug'] = '';
        $data['description'] = '';
        $data['excerpt'] = '';
        $data['sequence'] = '';
        $data['image'] = '';
        $data['seo_title'] = '';
        $data['meta'] = '';
        $data['parent_id'] = 0;
        $data['error'] = '';
        
        foreach($data['groups'] as $group)
        {
            $data['enabled_'.$group->id] = '';
        }

        //create the photos array for later use
        $data['photos'] = [];
        
        if ($id)
        { 
            $category = \CI::Categories()->find($id);

            //if the category does not exist, redirect them to the category list with an error
            if (!$category)
            {
                \CI::session()->set_flashdata('error', lang('error_not_found'));
                redirect('admin/categories');
            }
            
            //helps us with the slug generation
            $this->category_name = \CI::input()->post('slug', $category->slug);
            
            //set values to db values
            $data['id'] = $category->id;
            $data['name'] = $category->name;
            $data['slug'] = $category->slug;
            $data['description'] = $category->description;
            $data['excerpt'] = $category->excerpt;
            $data['sequence'] = $category->sequence;
            $data['parent_id'] = $category->parent_id;
            $data['image'] = $category->image;
            $data['seo_title'] = $category->seo_title;
            $data['meta'] = $category->meta;
            foreach($data['groups'] as $group)
            {
                $data['enabled_'.$group->id] = $category->{'enabled_'.$group->id};
            }
            
        }
        
        \CI::form_validation()->set_rules('name', 'lang:name', 'trim|required|max_length[64]');
        \CI::form_validation()->set_rules('slug', 'lang:slug', 'trim');
        \CI::form_validation()->set_rules('description', 'lang:description', 'trim');
        \CI::form_validation()->set_rules('excerpt', 'lang:excerpt', 'trim');
        \CI::form_validation()->set_rules('sequence', 'lang:sequence', 'trim|integer');
        \CI::form_validation()->set_rules('parent_id', 'parent_id', 'trim');
        \CI::form_validation()->set_rules('image', 'lang:image', 'trim');
        \CI::form_validation()->set_rules('seo_title', 'lang:seo_title', 'trim');
        \CI::form_validation()->set_rules('meta', 'lang:meta', 'trim');
        
        foreach($data['groups'] as $group)
        {
            \CI::form_validation()->set_rules('enabled_'.$group->id, lang('enabled').'('.$group->name.')', 'trim|numeric');
        }
        
        // validate the form
        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('category_form', $data);
        }
        else
        {
            
            
            $uploaded = \CI::upload()->do_upload('image');
            
            if ($id)
            {
                //delete the original file if another is uploaded
                if($uploaded)
                {
                    
                    if($data['image'] != '')
                    {
                        $file = [];
                        $file[] = 'product_photo/uploads/images/full/'.$data['image'];
                        $file[] = 'product_photo/uploads/images/medium/'.$data['image'];
                        $file[] = 'product_photo/uploads/images/small/'.$data['image'];
                        $file[] = 'product_photo/uploads/images/thumbnails/'.$data['image'];
                        
                        foreach($file as $f)
                        {
                            //delete the existing file if needed
                            if(file_exists($f))
                            {
                                unlink($f);
                            }
                        }
                    }
                }
                
            }
            
            if(!$uploaded)
            {
                $data['error'] = \CI::upload()->display_errors();
                if($_FILES['image']['error'] != 4)
                {
                    $data['error'] .= \CI::upload()->display_errors();
                    $this->view('category_form', $data);
                    return; //end script here if there is an error
                }
            }
            else
            {
                $image = \CI::upload()->data();
                $save['image'] = $image['file_name'];
                
                \CI::load()->library('image_lib');
                
                //this is the larger image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'product_photo/uploads/images/full/'.$save['image'];
                $config['new_image'] = 'product_photo/uploads/images/medium/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 600;
                $config['height'] = 500;
                \CI::image_lib()->initialize($config);
                \CI::image_lib()->resize();
                \CI::image_lib()->clear();

                //small image
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'product_photo/uploads/images/medium/'.$save['image'];
                $config['new_image'] = 'product_photo/uploads/images/small/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 300;
                $config['height'] = 300;
                \CI::image_lib()->initialize($config); 
                \CI::image_lib()->resize();
                \CI::image_lib()->clear();

                //cropped thumbnail
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'product_photo/uploads/images/small/'.$save['image'];
                $config['new_image'] = 'product_photo/uploads/images/thumbnails/'.$save['image'];
                $config['maintain_ratio'] = TRUE;
                $config['width'] = 150;
                $config['height'] = 150;
                \CI::image_lib()->initialize($config); 
                \CI::image_lib()->resize(); 
                \CI::image_lib()->clear();
            }
            
            \CI::load()->helper('text');
            
            //first check the slug field
            $slug = \CI::input()->post('slug');
            
            //if it's empty assign the name field
            if(empty($slug) || $slug=='')
            {
                $slug = \CI::input()->post('name');
            }
            
            $slug = url_title(convert_accented_characters($slug), 'dash', TRUE);

            if($id)
            {
                $slug = \CI::Categories()->validate_slug($slug, $category->id);
            }
            else
            {
                $slug = \CI::Categories()->validate_slug($slug);
            }
            
            $save['id'] = $id;
            $save['name'] = \CI::input()->post('name');
            $save['description'] = \CI::input()->post('description');
            $save['excerpt'] = \CI::input()->post('excerpt');
            $save['parent_id'] = intval(\CI::input()->post('parent_id'));
            $save['sequence'] = intval(\CI::input()->post('sequence'));
            $save['seo_title'] = \CI::input()->post('seo_title');
            $save['meta'] = \CI::input()->post('meta');
            $save['slug'] = $slug;
            foreach($data['groups'] as $group)
            {
                $save['enabled_'.$group->id] = \CI::input()->post('enabled_'.$group->id);
            }
            
            $category_id = \CI::Categories()->save($save);
            
            \CI::session()->set_flashdata('message', lang('message_category_saved'));
            
            //go back to the category list
            redirect('admin/categories');
        }
    }

    function delete($id)
    {
        
        $category = \CI::Categories()->find($id);
        //if the category does not exist, redirect them to the customer list with an error
        if ($category)
        {
            if($category->image != '')
            {
                $file = [];
                $file[] = 'product_photo/uploads/images/full/'.$category->image;
                $file[] = 'product_photo/uploads/images/medium/'.$category->image;
                $file[] = 'product_photo/uploads/images/small/'.$category->image;
                $file[] = 'product_photo/uploads/images/thumbnails/'.$category->image;
                
                foreach($file as $f)
                {
                    //delete the existing file if needed
                    if(file_exists($f))
                    {
                        unlink($f);
                    }
                }
            }

            \CI::Categories()->delete($id);
            
            \CI::session()->set_flashdata('message', lang('message_delete_category'));
            redirect('admin/categories');
        }
        else
        {
            \CI::session()->set_flashdata('error', lang('error_not_found'));
        }
    }
    
    public function avail_location($id=0){
        
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');

        \CI::form_validation()->set_rules('title', 'Title', 'required');
        //\CI::form_validation()->set_rules('title_link', 'Title link', 'required');
        \CI::form_validation()->set_rules('address', 'address', 'required');
        \CI::form_validation()->set_rules('phone', 'phone', 'required');
        \CI::form_validation()->set_rules('icons[]', 'icons', 'required');
        
        if (\CI::form_validation()->run() == FALSE)
        {
            //$settings = \CI::Settings()->get_settings('PickUpRate');
            if($id!=0){
            $iddd['locationval'] = \CI::Categories()->select_lcation($id);
            }
            $iddd['id'] = $id;

            $this->view('availablelocation', $iddd);
        }
        else
        {
            $aa = \CI::input()->post('icons');
            $rr = implode($aa,',');
            $cid = \CI::input()->post('idval');
             $a = [];
            if($cid != 0){
                $a['title'] = \CI::input()->post('title');
                $a['address'] = \CI::input()->post('address');
                $a['phone'] = \CI::input()->post('phone');
                $a['title_link'] = \CI::input()->post('title_link');
                $a['icons'] = $rr; 
                
                $cart_address_id = \CI::Categories()->availaction_update($cid,$a);
                //set_flashdata('ff')
                \CI::session()->set_flashdata('message', 'Successfully Updated');
                redirect('admin/list_location');
            }else{               
                $a['title'] = \CI::input()->post('title');
                $a['address'] = \CI::input()->post('address');
                $a['phone'] = \CI::input()->post('phone');
                $a['title_link'] = \CI::input()->post('title_link');
                $a['icons'] = $rr;
                
                $cart_address_id = \CI::Categories()->availaction_insert($a);
                //set_flashdata('ff')
                \CI::session()->set_flashdata('message', 'Successfully Saved');
                redirect('admin/list_location');
            }
           /* \CI::Settings()->save_settings('PickUpRate', \CI::input()->post());
            redirect('admin/shipping');*/
        }

        /*echo 'dd';
        $data['customers'] = '';

        $this->view('availablelocation', $data);*/
    }

    public function list_location(){
        $sjs['locationval'] = \CI::Categories()->select_lcation();
        $this->view('available_location', $sjs);
    }

    public function deletelocation($id,$st){
        $sjs['locationval'] = \CI::Categories()->delete_lcation($id,$st);
        \CI::session()->set_flashdata('message', 'Successfully Deleted');
        redirect('admin/list_location');
    }

    public function product_report(){
        $sjs['locationval'] = \CI::Categories()->delete_lcation($id);
        $sjs['slug'] = 'all';
        $sjs['page'] = 1;
        $sjs['droporder_list'] = 1;
        $sjs['sort'] = 'id';
        $sjs['dir'] = 'ASC';
        $per_page=config_item('products_per_page');
        $sjs['per_page'] = $per_page;
        $sjs['stprice'] = 1;
        $sjs['endprice'] = 1000000;   
        $sjs['categories'] = \CI::Categories()->getCategoryOptionsMenu();     
        \CI::session()->set_flashdata('message', 'Successfully Deleted');
        $this->view('adreports', $sjs);
        
    }
    public function productrportlist($page=1){
        $per_page = config_item('products_per_page');
        $stprice = $_POST['st_price_sub'];
        $endprice = $_POST['end_price_sub'];
        $slug = $_POST['sr_slug'];
        $sort = $_POST['sr_sort'];
        $dir = $_POST['sr_dir'];
        $page = $_POST['sr_page'];
        $droporder_list = $_POST['sr_droporder_list'];
        $sr_serach_input = $_POST['sr_serach_input'];
        $sr_alpha_list = $_POST['sr_alpha_list'];        
        $allctegary = $_POST['allctegary'];
        $allprductctegary = $_POST['allprductctegary'];
        $pr_size = $_POST['prsize'];            
        $pr_color = $_POST['prcolor'];
        $prmonth = $_POST['prmonth'];
        $pryear = $_POST['pryear'];    
        
        if($droporder_list == 1){
            $sort = 'name'; $dir = 'ASC';
        }else if($droporder_list == 2){
            $sort = 'name'; $dir = 'DESC';
        }else if($droporder_list == 3){
            $sort = 'price_1'; $dir = 'ASC';
        }else if($droporder_list == 4){
            $sort = 'price_1'; $dir = 'DESC';
        }

        $result = \CI::Products()->sumof_all_productsadmin($stprice, $endprice, $slug, $per_page, $page, $sort,$dir, $sr_serach_input, $sr_alpha_list, $allctegary, $allprductctegary, $pr_size, $pr_color,$prmonth,$pryear);
        /*print_r($result);
        exit();*/
        //$config['total_rows'] = $result['products'];
        \CI::load()->library('pagination');
       // $config['base_url'] = $pagination_base_url;
        $config['uri_segment'] = 5;
        $config['per_page'] = $per_page;
        $config['num_links'] = 3;
        $config['total_products'] = $result['productcnt'];


        $data['sr_droporder_list'] = $droporder_list;
        $data['sr_serach_input'] = $sr_serach_input;
        $data['sr_alpha_list'] = $sr_alpha_list;


        $data['pagecall'] = 'search';
        $data['serchbyprice'] = 'serchbyprice';
        $data['stprice'] = $stprice;
        $data['endprice'] = $endprice;
        $data['slug'] = $slug;
        $data['sort'] = $sort;
        $data['dir'] = $dir;
        $data['page'] = $page;
        $data['per_page'] = $per_page;
        $data['total_products'] = $result['productcnt'];
        \CI::pagination()->initialize($config);

        $data['products'] = $result;
        $data['category'] = (object)['name'=>str_replace('{term}', $term, lang('search_title'))];

        //$this->view('categories/category', $data);
        /*print_r($data);
        exit();*/
        echo $totalvalue = $result;
        

    
    }

    public function add_slider($id=0){   

        \CI::load()->helper('form');
        \CI::load()->library('form_validation');

        \CI::form_validation()->set_rules('title', 'Title', 'required');
        \CI::form_validation()->set_rules('description', 'description', 'required');
        if(empty($_FILES['image']['name']) && $id==0)
        {
        \CI::form_validation()->set_rules('image', 'Image', 'required');
        }
        \CI::form_validation()->set_rules('image_link', 'image link', 'required');
        
        if (\CI::form_validation()->run() == FALSE)
        {
            $iddd['sliderslist'] = \CI::Categories()->sliderlist($id);
            $iddd['id'] = $id;
            $this->view('add_slider', $iddd);
        }
        else
        {
            $a = [];
             $sjsid = \CI::input()->post('idval');
            if($sjsid){
                $a['id'] = $id;
                $randval = rand(000,99999);   
                $a['title'] = \CI::input()->post('title');
                $a['description'] = \CI::input()->post('description');
                $a['image_link'] = \CI::input()->post('image_link');

                $image = $_FILES['image']['name'];
                $file_tmp = $_FILES['image']['tmp_name'];
                $uploadfile = FCPATH.'\product_photo/sliderimage/'.$randval.basename($_FILES['image']['name']);
                move_uploaded_file($file_tmp, $uploadfile);
                if($image){
                $a['image'] = 'sliderimage/'.$randval.$image;
                }
                $cart_address_id = \CI::Categories()->saveslide($sjsid,$a);
                \CI::session()->set_flashdata('message', 'Successfully Updated');
                redirect('admin/view_slider');
                
                /*$cart_address_id = \CI::Categories()->availaction_update($cid,$a);
                \CI::session()->set_flashdata('message', 'Successfully Updated');
                redirect('admin/add_slider');*/
            }else{      
                $randval = rand(000,99999);                
                $a['title'] = \CI::input()->post('title');
                $a['description'] = \CI::input()->post('description');
                $a['image_link'] = \CI::input()->post('image_link');

                $image = $_FILES['image']['name'];
                $file_tmp = $_FILES['image']['tmp_name'];
                $uploadfile = FCPATH.'\product_photo/sliderimage/'.$randval.basename($_FILES['image']['name']);
                move_uploaded_file($file_tmp, $uploadfile);
                $a['image'] = 'sliderimage/'.$randval.$image;
                $cart_address_id = \CI::Categories()->saveslide($cid,$a);
                \CI::session()->set_flashdata('message', 'Successfully Inserted');
                redirect('admin/view_slider');
                //echo base_url().'product_photo';
                //exit();
                //$a['image_link'] = \CI::input()->post('image_link');
                
                
                /*$cart_address_id = \CI::Categories()->availaction_update($cid,$a);
                \CI::session()->set_flashdata('message', 'Successfully Updated');
                redirect('admin/add_slider');*/
            }
           /* \CI::Settings()->save_settings('PickUpRate', \CI::input()->post());
            redirect('admin/shipping');*/
        }
    
    }
    public function slider_view(){
        $sjs['sliderslist'] = \CI::Categories()->sliderlist();
        $this->view('view_slider', $sjs);

    }
    public function slider_delete($id){
        $sjs['sliderslist'] = \CI::Categories()->sliderdelete($id);
        redirect('admin/view_slider');

    }

}