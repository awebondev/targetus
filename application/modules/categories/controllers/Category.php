<?php namespace GoCart\Controller;
/**
 * Category Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    Category
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Category extends Front {

    public function index($slug=NULL, $sort='id', $dir="ASC", $page=1) {
       
        \CI::lang()->load('categories');
        \CI::lang()->load('products');
        //define the URL for pagination
        $pagination_base_url = site_url('category/'.$slug.'/'.$sort.'/'.$dir);

        //how many products do we want to display per page?
        //this is configurable from the admin settings page.
        $per_page = config_item('products_per_page');

        //grab the categories
        $categories['products'] = '';
        /*echo $page.'<br>';
        echo $per_page.'<br>';
        echo $sort.'<br>';
        echo $dir.'<br>';
        echo $slug;
        print_r($categories);
        exit();*/

        //no category? show 404
        if(!$categories)
        {
            throw_404();
            return;
        }
        //$categories['categories'] = $categories;
        $categories['stprice'] = 0;
        $categories['endprice'] = 1000000;
        $categories['search_input'] = '';
        $categories['sr_droporder_list'] = '';
        $categories['sr_serach_input'] = '';
        $categories['sr_alpha_list'] = '';
        
        $categories['sort'] = $sort;
        $categories['dir'] = $dir;
        if($slug == NULL){
            $categories['slug'] = 'all'; 
        }else{
            $categories['slug'] = $slug;
        }
        //$categories['slug'] = $slug;
        $categories['page'] = $page;
        $categories['per_page'] = $per_page;
        $categories['droporder_list'] = 1;
        
        
        //load up the pagination library
        \CI::load()->library('pagination');
        $config['base_url'] = $pagination_base_url;
        $config['uri_segment'] = 5;
        $config['per_page'] = $per_page;
        $config['num_links'] = 3;
        $config['total_rows'] = $categories['total_products'];

        \CI::pagination()->initialize($config);

        //load the view
        $this->view('categories/category', $categories);
    }

    public function shortcode($slug = false, $perPage = false)
    {   
        if(!$perPage)
        {
            $perPage = config_item('products_per_page');
        }

        $products = \CI::Categories()->get($slug, 'id', 'ASC', 0, $perPage);

        return $this->partial('categories/products', $products);
    }
    public function getcategories() {
        $slug = $_POST['slug']; 
        $cats = \CI::Categories()->get_categories(0);
        $mockData = array();
        $mockData = $this->ConstructHierarchy(null,$cats,$slug);
        echo json_encode($mockData);
    }

    /*public function getCategoriesProduct() {
        //$slug = $_POST['slug']; 
        $tt = $_POST['cate_id'];
        //print_r($tt);
        foreach($tt as $tt)
        {
            
         $cats = \CI::Categories()->get_categories($tt);
        $mockData = array();

        $mockData = $this->ConstructHierarchy(null,$cats);

         $prds = \CI::Products()->getProducts($cats->product_id);
        $mockDatas = array();

        $mockDatas = $this->ConstructHierarchy(null,$prds);
        //print_r($mockData);
        //exit();
        //echo $tt;
        }
        //exit();

        echo json_encode($mockDatas);
    }
*/
    public function getCategoriesProduct() {     
        $per_page = config_item('products_per_page');   
        $products = array();
        $page = 1;
        $data=[];
        if(isset($_POST['cate_id']) && !empty($_POST['cate_id'])){
        	$cate_id = $_POST['cate_id'];
	        //foreach($cate_id as $tt){
                //$data['total_products'] = \CI::Products()->count_products($category->id);
                    $productss = \CI::Products()->getProducts($cate_id, $per_page, $page, $sort = 'id', $direction = 'ASC');
	                /*foreach($productss as $productsss) {
	                	array_push($products,$productsss);
	        		}*/
	        //}
                    $data['slug']=$_POST['slug'];
        }  else{
            $productss['productcnt'] = \CI::Products()->count_all_products();            
            $productss['products'] = \CI::Products()->getProducts($category_id = false, $per_page, $page, $sort = false, $direction= false);
            $data['slug']= 'all';
        }  
        $data['sort']='id';
        $data['dir']='ASC';
        $data['per_page']=$per_page;
        $data['page']=$page;
        
        $data['total_products']=$productss['productcnt'];   
        $data['products']=$productss['products'];
        // print_r($products);
        // exit();
        return $this->partial('categories/products', $data);
    }

    public function ConstructHierarchy($parent=null,$childrens=[],$slug=false){
        $ChilderenArray = [];

        foreach($childrens as $childrancat) {
            $childrenitemClass = new \stdClass();
            $uichildrenCategoryClass = new \stdClass();
            $uichildrenCategoryClass->id=$childrancat->id;
            $uichildrenCategoryClass->label=$childrancat->name;
            $childrenitemClass->item = $uichildrenCategoryClass;
            if($childrancat->slug == $slug || $slug === true)
            	$uichildrenCategoryClass->checked=true;
            else
            	$uichildrenCategoryClass->checked=false;
            $ChilderenArray[]=  $childrenitemClass;

             $subCategories = \CI::Categories()->get_categories($uichildrenCategoryClass->id);
             if(!empty($subCategories)){
                $this->ConstructHierarchy($childrenitemClass,$subCategories,$uichildrenCategoryClass->checked);
             }
        }
        if($parent !=  null){
            $parent->children = $ChilderenArray;
        }
        return $ChilderenArray;
    }
    public function availabelocation(){
        $sjs['locationval'] = \CI::Categories()->select_lcationbyuser();
        $this->view('categories/available_location', $sjs);
       //exit();
    }
    
}
