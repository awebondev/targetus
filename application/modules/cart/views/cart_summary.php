<?php $addrerror = \CI::session()->set_userdata('addr_error',''); ?>
<div class="container">
<?php if(!isset($hideButtons) || !$hideButtons){?>
    <div class="row">
        <div class="col-md-12">
        <div class="breadcrumb">                    
            <span><img src="<?php echo base_url(); ?>assets/img/home.png"></span>               
            <h6>Shopping Cart</h6>
        </div>
    </div> 
   <?php } ?>                 
        <div class="col-md-12 col-sm-12 hidden-xs table-mar">
            <?php
                $sjtot;
                $cartItems = GC::getCartItems();
                //print_r($cartItems);
               // exit();
                $options = CI::Orders()->getItemOptions(GC::getCart()->id);

                $charges = [];

                $charges['giftCards'] = [];
                $charges['coupons'] = [];
                $charges['tax'] = [];
                $charges['shipping'] = [];
                $charges['products'] = [];

                foreach ($cartItems as $item)
                {
                	if($item->type == 'gift card')
                    {
                        $charges['giftCards'][] = $item;
                        continue;
                    }
                    elseif($item->type == 'coupon')
                    {
                        $charges['coupons'][] = $item;
                        continue;
                    }
                    elseif($item->type == 'tax')
                    {
                        $charges['tax'][] = $item;
                        continue;
                    }
                    elseif($item->type == 'shipping')
                    {
                        $charges['shipping'][] = $item;
                        continue;
                    }
                    elseif($item->type == 'product')
                    {
                        $charges['products'][] = $item;
                    }
                }

                if(count($charges['products']) == 0)
                {
                    echo '<script>location.reload();</script>';
                }
                 $loopvar = 1;
                foreach($charges['products'] as $product):
                   
                    $photo = theme_img('no_picture.png', lang('no_image_available'));
                    $product->images = array_values(json_decode($product->images, true));
                    if(!empty($product->images))
                    {
                        foreach($product->images as $photo)
                        {
                            $primary = $photo;
                            if(isset($photo['primary']))
                            {
                                $primary = $photo;
                            }
                        }
                        if(!isset($primary))
                        {
                            $tmp = $product->images; //duplicate the array so we don't lose it.
                            $primary = array_shift($tmp);
                        }

                        $photo = '<img src="'.base_url('product_photo/'.$primary['filename']).'" class="img-responsive"/>';
                    }

            ?>
            <div class="product-table">
                <div class="col-md-2 col-sm-2 col-xs-4 no-padding">
                    <?php if($loopvar == 1) {   ?>
                    <div class="title table-propotation ">
                        Images
                    </div>
                    <?php } ?>
                    <div class="pro-img table-propotation set-ipad set-desktop">
                        <?php echo $photo;?>
                    </div>
                </div>
                <div class="col-md-4 hidden-sm no-padding ">
                    <?php if($loopvar == 1) {   ?>
                    <div class="title table-propotation">
                        Product Name
                    </div>
                    <?php } ?>
                    <div class="pro-detial table-propotation">
                        <p><?php echo $product->name; ?></p>
                        <!-- span><h3>Color</h3>: Black</span--><br>
                        <!-- span><h3>Size</h3> : Small</span--><br>
                        <!-- span><h3>Reward Points</h3> : 300 </span--><br>
                    </div>
                </div>

                <div class="col-md-2 col-sm-4 no-padding">
                    <?php if($loopvar == 1) {   ?>
                    <div class="title table-propotation">
                        Quantity
                    </div>
                    <?php } ?>
                    <div class="quantity table-propotation">
                        <input type="search" id="item_quantity_<?php echo $product->id;?>" class="oty" value="<?php echo $product->quantity; ?>"><br>                               
                        <a onclick="updateItem(<?php echo $product->id;?>,'newQty','<?php echo $product->quantity; ?>');" style="cursor:pointer" class="close-btn">
                            <img src="<?php echo base_url(); ?>assets/img/close.png">
                        </a>
                        <a onclick="updateItem(<?php echo $product->id;?>, 0);" style="cursor:pointer" class="close-btn">
                            <img src="<?php echo base_url(); ?>assets/img/cancel.png">
                        </a>                                
                    </div>
                </div>
                <div class="col-md-2 col-sm-3 no-padding">
                    <?php if($loopvar == 1) {   ?>
                    <div class="title table-propotation">
                        Unit Price
                    </div>
                    <?php } ?>
                    <div class="unit-price table-propotation">
                        <p><?php echo "$&nbsp;".$product->price; ?></p>
                    </div>
                    
                </div>
                
                <div class="col-md-2 col-sm-3 no-padding">
                    <?php if($loopvar == 1) {   ?>
                    <div class="title total table-propotation">
                        Total
                    </div>
                    <?php } ?>
                    <div class="total table-propotation">
                       <?php 
                        	 $grand_total = $product->total_price;
	                        if(isset($item->coupon_discount))
	                        {
	                        	$grand_total = ($product->total_price * $product->quantity) - ($product->coupon_discount * $product->coupon_discount_quantity);
	                        }
	                        else
	                        {
	                        	$grand_total = ($product->total_price * $product->quantity);
	                        }
                        
                        ?>
                            <p><?php $sjtot = $sjtot+$grand_total; echo "$&nbsp;".$grand_total; ?></p>
                    </div>

                </div>
            </div>
            <?php 
            $loopvar ++;
            endforeach;?>
        </div>               
<!-- for-mobile -->
        <div class="hidden-lg hidden-md hidden-sm col-xs-12 table-mar">
                <?php

                    $cartItems = GC::getCartItems();
                    $options = CI::Orders()->getItemOptions(GC::getCart()->id);

                    $charges = [];

                    $charges['giftCards'] = [];
                    $charges['coupons'] = [];
                    $charges['tax'] = [];
                    $charges['shipping'] = [];
                    $charges['products'] = [];

                    foreach ($cartItems as $item)
                    {
                        if($item->type == 'gift card')
                        {
                            $charges['giftCards'][] = $item;
                            continue;
                        }
                        elseif($item->type == 'coupon')
                        {
                            $charges['coupons'][] = $item;
                            continue;
                        }
                        elseif($item->type == 'tax')
                        {
                            $charges['tax'][] = $item;
                            continue;
                        }
                        elseif($item->type == 'shipping')
                        {
                            $charges['shipping'][] = $item;
                            continue;
                        }
                        elseif($item->type == 'product')
                        {
                            $charges['products'][] = $item;
                        }
                    }

                    if(count($charges['products']) == 0)
                    {
                        echo '<script>location.reload();</script>';
                    }
                     $loopvar = 1;
                    foreach($charges['products'] as $product):
                       
                        $photo = theme_img('no_picture.png', lang('no_image_available'));
                        //$product->images = array_values(json_decode($product->images, true));
                        if(!empty($product->images))
                        {
                            foreach($product->images as $photo)
                            {
                                $primary = $photo;
                                if(isset($photo['primary']))
                                {
                                    $primary = $photo;
                                }
                            }
                            if(!isset($primary))
                            {
                                $tmp = $product->images; //duplicate the array so we don't lose it.
                                $primary = array_shift($tmp);
                            }

                            $photo = '<img src="'.base_url('product_photo/'.$primary['filename']).'" class="img-responsive"/>';
                        }

                ?>
                <div class="product-table hidden-lg hidden-md hidden-sm">
                    <div class="col-xs-6 no-padding">
                        <?php if($loopvar == 1) {   ?>
                        <div class="title table-propotation ">
                            Images
                        </div>
                        <?php } ?>
                        <div class="pro-img table-propotation set-mobile ">
                            <?php echo $photo;?>
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding ">
                        <?php if($loopvar == 1) {   ?>
                        <div class="title table-propotation">
                            Product Name
                        </div>
                        <?php } ?>
                        <div class="pro-detial table-propotation">
                            <p><?php echo $product->name; ?></p>
                            <!-- <span><h3>Color</h3>: Black</span><br>
                            <span><h3>Size</h3> : Small</span><br>
                            <span><h3>Reward Points</h3> : 300 </span> --><br>
                        </div>
                    </div>

                    <div class="col-xs-6 no-padding">
                        <?php if($loopvar == 1) {   ?>
                        <div class="title table-propotation">
                            Quantity
                        </div>
                        <?php } ?>
                        <div class="quantity table-propotation">
                            <input type="search" class="oty" value="<?php echo $product->quantity; ?>"><br>                               
                            <a href="" class="close-btn">
                                <img src="<?php echo base_url(); ?>assets/img/close.png">
                            </a>
                            <a href="" class="close-btn">
                                <img src="<?php echo base_url(); ?>assets/img/cancel.png">
                            </a>                                
                        </div>
                    </div>
                    <div class="col-xs-6 no-padding">
                        <?php if($loopvar == 1) {   ?>
                        <div class="title table-propotation">
                            Unit Price
                        </div>
                        <?php } ?>
                        <div class="unit-price table-propotation">
                            <p><?php echo "$&nbsp;".$product->price; ?></p>
                        </div>
                        
                    </div>
                    <div class="col-xs-12 no-padding">
                        <?php if($loopvar == 1) {   ?>
                        <div class="title total table-propotation">
                            Totalss
                        </div>
                        <?php } ?>
                        <div class="total table-propotation mobiles">
                        <?php 
                        	$grand_total = $item->total_price;
	                        if(isset($item->coupon_discount))
	                        {
	                        	$grand_total = ($item->total_price * $item->quantity) - ($item->coupon_discount * $item->coupon_discount_quantity);
	                        }
	                        else
	                        {
	                        	$grand_total = ($item->total_price * $item->quantity);
	                        }
                        
                        ?>
                            <p><?php echo "$&nbsp;".$grand_total; ?></p>
                        </div>

                    </div>
                </div>
                <?php 
                $loopvar ++;
                endforeach;?>
            </div>
<!-- for-mobile -->
             
        <div class="col-md-6 pull-right">
            <div class="row product-table">
                <!--  div class="col-md-6 col-sm-6 col-xs-6 left-cat">Eco Tax (-2.00):</div>
                <div class="col-md-6 col-sm-6 col-xs-6 right-cat">$ 4.00</div-->
                
                <?php
                    $rrj = GC::getShippingMethod();  if($rrj->total_price){ 
                        //print_r($rrj); ?>
                <div class="col-md-6 col-sm-6 col-xs-6 left-cat">Shipping Cost </div>
                <div class="col-md-6 col-sm-6 col-xs-6 right-cat">
                    <?php //echo $rr = GC::getGrandTotal()-$sjtot; ?>
                    <?php echo $rrj->total_price; ?>
                </div>
                <?php } ?>
                <div class="col-md-6 col-sm-6 col-xs-6 left-cat">Total</div>
                <div class="col-md-6 col-sm-6 col-xs-6 right-cat">
                    <?php echo "$&nbsp;".GC::getGrandTotal(); ?>
                </div>                              
            </div>
            <?php if(!isset($hideButtons) || !$hideButtons){?>

                <?php
                    $ses_data = CI::session()->userdata('customer');
                    if($ses_data->firstname) { ?>
                        <a href="<?php echo base_url().'checkout/checkout-wizard' ?>" class="login-btn green-bg pull-right table-mar">
                            <span><img src="<?php echo base_url(); ?>assets/img/submit.png"></span><h1>Check Out</h1>
                        </a>
                <?php } else { ?>
                        <a href="<?php echo base_url().'login' ?>" class="login-btn green-bg pull-right table-mar">
                            <span><img src="<?php echo base_url(); ?>assets/img/submit.png"></span><h1>Check Out</h1>
                        </a>            
            <?php } } ?>             
        </div>                  
    </div>
</div>
<script>
function updateItem(id, newQuantity, oldQuantity)
{
    $('#summaryErrors').html('').hide();
	if(newQuantity=='newQty'){
		newQuantity = $('#item_quantity_'+id).val();
	}
    if(newQuantity != oldQuantity)
    {
        var active = $(document.activeElement).attr('id');

        if(newQuantity == 0)
        {
            if(!confirm('<?php echo lang('remove_item');?>')){
                return false;
            }
            else
            {
                if(oldQuantity != undefined) //if the "remove" button is used we don't need to bother with this.
                {
                    $('#qtyInput'+id).val(oldQuantity);
                }
            }
        }
        $('#cartSummary').spin();
        $.post('<?php echo site_url('cart/update-cart');?>', {'product_id':id, 'quantity':newQuantity}, function(data){

            if(data.error != undefined)
            {
                $('#summaryErrors').text(data.error).show();
                //there was an error. reset it.
                lastInput.val(lastValue).focus();
            }
            else
            {
                var elem = $(document.activeElement).attr('id');
                getCartSummary(function(){
                    $('#'+elem).focus();
                });
                $.post('<?php echo site_url('cart/get-cart-info');?>', '', function(data){
               	 	$('#header-cart-info').find('span.rate').html(data.totalprice+' $');  
               	 	$('#header-cart-info').find('span.item').html(data.totalItems+' items');            	
                   }, 'json');
            }
            
        }, 'json');
    }
}

</script>