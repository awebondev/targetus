
<div class="container" id="register">
    <div class="row">        
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">            
            <div class="green-strips">
                <h6><?php echo lang('account_information');?></h6>
            </div>
            <div class="white-strips">
                <?php echo form_open('my-account'); ?>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="account_firstname"><?php echo lang('account_firstname');?></label>
                            <?php echo form_input(['name'=>'firstname', 'class' => 'form-control', 'value'=> assign_value('firstname', $customer['firstname'])]);?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="account_lastname"><?php echo lang('account_lastname');?></label>
                            <?php echo form_input(['name'=>'lastname', 'class' => 'form-control', 'value'=> assign_value('lastname', $customer['lastname'])]);?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="account_email"><?php echo lang('account_email');?></label>
                            <?php echo form_input(['name'=>'email', 'class' => 'form-control', 'value'=> assign_value('email', $customer['email'])]);?>
                        </div>
                        <div class="check-box">
                            <input type="checkbox" id="checkbox" name="email_subscribe" value="1" <?php if((bool)$customer['email_subscribe']) { ?> checked="checked" <?php } ?>/>
                            <label for="checkbox"><?php echo lang('account_newsletter_subscribe');?></label>                        
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="account_phone"><?php echo lang('account_phone');?></label>
                            <?php echo form_input(['name'=>'phone',  'class' => 'form-control', 'value'=> assign_value('phone', $customer['phone'])]);?>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label style="margin-bottom:15px;"><?php echo lang('account_password_instructions');?></label>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="account_password"><?php echo lang('account_password');?></label>
                            <?php echo form_password(['name'=>'password', 'class' => 'form-control', ]);?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <label for="account_confirm"><?php echo lang('account_confirm');?></label>
                            <?php echo form_password(['name'=>'confirm',  'class' => 'form-control',]);?>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <button type="button" href="#" value="<?php echo lang('form_submit');?>" class="login-btn" onclick="return verifyEmail();">
                            <span><img src="<?php echo base_url() ?>assets/img/submit.png"></span>
                            <h1>Submit</h1>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- <div class="col-md-12 col-sm-12 col-xs-12">
            <div id="addresses" class="col" data-cols="2/3"></div>
        </div> -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h2><?php echo lang('order_history');?></h2>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php if($orders):
                        echo $orders_pagination;
                    ?>
                    <table class="table bordered zebra">
                        <thead>
                            <tr>
                                <th><?php echo lang('order_date');?></th>
                                <th><?php echo lang('order_number');?></th>
                                <th><?php echo lang('order_status');?></th>
                            </tr>
                        </thead>

                        <tbody>
                        <?php
                        foreach($orders as $order): ?>
                            <tr>
                                <td>
                                    <?php $d = format_date($order->ordered_on); 
                            
                                    $d = explode(' ', $d);
                                    echo $d[0].' '.$d[1].', '.$d[3];
                            
                                    ?>
                                </td>
                                <td><a href="<?php echo site_url('order-complete/'.$order->order_number); ?>"><?php echo $order->order_number; ?></a></td>
                                <td><?php echo $order->status;?></td>
                            </tr>
                    
                        <?php endforeach;?>
                        </tbody>
                    </table>
                    <?php else: ?>
                        <div class="alert yellow"><i class="close"></i><?php echo lang('no_order_history');?></div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <h3>address list</h3>
    
</div>

<script>
$(document).ready(function(){
    loadAddresses();
});

function closeAddressForm()
{
    $.gumboTray.close();
    loadAddresses();
}

function loadAddresses()
{
    $('#addresses').spin();
    $('#addresses').load('<?php echo base_url('addresses');?>');
}
</script>