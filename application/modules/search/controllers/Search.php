<?php namespace GoCart\Controller;
/**
 * Search Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    Search
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Search extends Front {

    public function index($code=false, $sort='name', $dir="ASC", $page = 0)
    {

        $pagination_base_url = site_url('search/'.$code.'/'.$sort.'/'.$dir);

        //how many products do we want to display per page?
        //this is configurable from the admin settings page.
        $per_page = config_item('products_per_page');

        \CI::load()->model('Search');

        //check to see if we have a search term
        if(!$code)
        {
            //if the term is in post, save it to the db and give me a reference
            $term = \CI::input()->post('term', true);
            if(empty($term))
            {
                //if there is still no search term throw an error
                $data['error'] = lang('search_error');
                $this->view('search_error', $data);
                return;
            }
            else
            {
                $code = \CI::Search()->recordTerm($term);

                // no code? redirect so we can have the code in place for the sorting.
                // I know this isn't the best way...
                redirect('search/'.$code.'/'.$sort.'/'.$dir.'/'.$page);
            }
        }
        else
        {
            //if we have the md5 string, get the term
            $term = \CI::Search()->getTerm($code);
        }

        $data['sort'] = $sort;
        $data['dir'] = $dir;
        $data['code'] = $code;
        $data['page'] = $page;

        if(empty($term))
        {
            //if there is still no search term throw an error
            $this->view('search_error', $data);
            return;
        }
        else
        {


            $result = \CI::Products()->search_products($term, $per_page, $page, $sort, $dir);

            $config['total_rows'] = $result['count'];

            \CI::load()->library('pagination');
            $config['base_url'] = $pagination_base_url;
            $config['uri_segment'] = 5;
            $config['per_page'] = $per_page;
            $config['num_links'] = 3;
            $config['total_rows'] = $result['count'];

            \CI::pagination()->initialize($config);

            $data['products'] = $result['products'];

            $data['category'] = (object)['name'=>str_replace('{term}', $term, lang('search_title'))];

            $this->view('search', $data);
        }
    }
    public function searchproduct($term = null, $page=1, $sort='name', $dir='ASC'){
        $per_page = config_item('products_per_page');
        if(isset($_POST['search']))
        {
           $term = $_POST['search'];
           $data['searchinput'] = $_POST['search'];
           $result = \CI::Products()->search_products($term, $per_page, $page, $sort,$dir );
        }
        else{
            $result = \CI::Products()->search_products_alpha($term, $per_page, $page, $sort,$dir );
        }
        
        //$result = \CI::Products()->search_products($term, $per_page, $page, $sort,$dir );
        /*print_r($result);
        exit();*/
            $config['total_rows'] = $result['count'];

            \CI::load()->library('pagination');
           // $config['base_url'] = $pagination_base_url;
            $config['uri_segment'] = 5;
            $config['per_page'] = $per_page;
            $config['num_links'] = 3;
            $config['total_products'] = $result['count'];

            $data['pagecall'] = 'search';
            $data['slug'] = $term;
            $data['sort'] = $sort;
            $data['dir'] = $dir;
            $data['page'] = $page;
            $data['per_page'] = $per_page;
            $data['total_products'] = $result['count'];
            \CI::pagination()->initialize($config);

            $data['products'] = $result['products'];

            $data['category'] = (object)['name'=>str_replace('{term}', $term, lang('search_title'))];

            $this->view('categories/category', $data);
    }

    public function searchproductby_price($stprice,$endprice,$term = null, $page=1, $sort='name', $dir='ASC'){
        /*echo "sdsfasdf";
        exit();*/
        $per_page = config_item('products_per_page');
        $stprice = $_POST['st_price'];
        $endprice = $_POST['end_price'];
        $slug = $_POST['sr_slug'];
        $sort = $_POST['sr_sort'];
        $dir = $_POST['sr_dir'];
        //exit();
            $result = \CI::Products()->search_products_byprice($stprice, $endprice, $slug, $per_page, $page, $sort,$dir );
        
            $config['total_rows'] = $result['count'];

            \CI::load()->library('pagination');
           // $config['base_url'] = $pagination_base_url;
            $config['uri_segment'] = 5;
            $config['per_page'] = $per_page;
            $config['num_links'] = 3;
            $config['total_products'] = $result['count'];

            $data['pagecall'] = 'search';
            $data['serchbyprice'] = 'serchbyprice';
            $data['stprice'] = $stprice;
            $data['endprice'] = $endprice;
            $data['slug'] = $slug;
            $data['sort'] = $sort;
            $data['dir'] = $dir;
            $data['page'] = $page;
            $data['per_page'] = $per_page;
            $data['total_products'] = $result['count'];
            \CI::pagination()->initialize($config);

            $data['products'] = $result['products'];
            /*print_r($data['products']);
            exit();*/

            $data['category'] = (object)['name'=>str_replace('{term}', $term, lang('search_title'))];

            //$this->view('categories/category', $data);
            return $this->partial('categories/products', $data);
    }



    function searchallvaluseajax($page=1){
        /*echo 'fff';
        exit();*/
        $per_page = config_item('products_per_page');
        $stprice = $_POST['st_price_sub'];
        $endprice = $_POST['end_price_sub'];
        $slug = $_POST['sr_slug'];
        $sort = $_POST['sr_sort'];
        $dir = $_POST['sr_dir'];
        $page = $_POST['sr_page'];
        $droporder_list = $_POST['sr_droporder_list'];
        $sr_serach_input = $_POST['sr_serach_input'];
        $sr_alpha_list = $_POST['sr_alpha_list'];        
        $allctegary = $_POST['allctegary'];
        $allprductctegary = $_POST['allprductctegary'];
        $pr_size = $_POST['prsize'];            
        $pr_color = $_POST['prcolor'];    
        
        if($droporder_list == 1){
            $sort = 'name'; $dir = 'ASC';
        }else if($droporder_list == 2){
            $sort = 'name'; $dir = 'DESC';
        }else if($droporder_list == 3){
            $sort = 'price_1'; $dir = 'ASC';
        }else if($droporder_list == 4){
            $sort = 'price_1'; $dir = 'DESC';
        }

        $result = \CI::Products()->search_all_products($stprice, $endprice, $slug, $per_page, $page, $sort,$dir, $sr_serach_input, $sr_alpha_list, $allctegary, $allprductctegary, $pr_size, $pr_color);
        
        //$config['total_rows'] = $result['products'];
        \CI::load()->library('pagination');
       // $config['base_url'] = $pagination_base_url;
        $config['uri_segment'] = 5;
        $config['per_page'] = $per_page;
        $config['num_links'] = 3;
        $config['total_products'] = $result['productcnt'];


        $data['sr_droporder_list'] = $droporder_list;
        $data['sr_serach_input'] = $sr_serach_input;
        $data['sr_alpha_list'] = $sr_alpha_list;


        $data['pagecall'] = 'search';
        $data['serchbyprice'] = 'serchbyprice';
        $data['stprice'] = $stprice;
        $data['endprice'] = $endprice;
        $data['slug'] = $slug;
        $data['sort'] = $sort;
        $data['dir'] = $dir;
        $data['page'] = $page;
        $data['per_page'] = $per_page;
        $data['total_products'] = $result['productcnt'];
        \CI::pagination()->initialize($config);

        $data['products'] = $result['products'];
        $data['category'] = (object)['name'=>str_replace('{term}', $term, lang('search_title'))];

        //$this->view('categories/category', $data);
        return $this->partial('categories/products', $data);

    }











}
