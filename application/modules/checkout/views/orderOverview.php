<h6>Review Your Order</h6>
<p class="subtitle">Please review and submit order.</p>
<p class="desc">Please review your information to verify that it is correct. Make any necessary changes by clicking â€œchangeâ€� in the area where the change is required.</p>
<p  class="desc">Upon placing your order, you will be taken to a payment page where you will be able to make your payment using your selected method.</p>
<p  class="desc">Target US does not handle you banking or credit card information. To insure the highest level of security we use third party solutions from Moneris for credit payments, and are partnered with most major Canadian Banks supporting online bank payments.</p>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive wizard">
</div>
<div class="row shipping-address">
    <div class="col-md-6 col-sm-6 col-xs-9">
        <h4>Shipping Address</h4>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-3">
        <a href="#" class="pull-right">
            <img src="<?php echo base_url(); ?>assets/img/Edit File-100.png" class="edit-img">
            <br/>
            <span>Change</span>
        </a>
    </div>
    <?php if(!empty($addresses))
        {
            $this->show('checkout/address_list', ['addresses'=>$addresses,'hideButtons'=>$hideButtons]);
        }
   ?>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive wizard">
</div>
<div class="row shipping-address">
    <div class="col-md-6 col-sm-6 col-xs-9">
        <h4>Payment Method</h4>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-3">
        <a href="#" class="pull-right">
            <img src="<?php echo base_url(); ?>assets/img/Edit File-100.png" class="edit-img">
            <br/>
            <span>Change</span>
        </a>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <p class="desc">You have selected <?php echo $paymentmethod['name'];?> and <?php echo $shippingmethod;?> for your payment and shipping methods.</p>                                     
    </div>
</div>
<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
    <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive wizard">
</div>
<div class="row shipping-address">
    <div class="col-md-6 col-sm-6 col-xs-9">
        <h4>Order Detail</h4>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-3">
        <a href="#" class="pull-right">
            <img src="<?php echo base_url(); ?>assets/img/Edit File-100.png" class="edit-img">
            <br/>
            <span>Change</span>
        </a>
    </div>
</div>    
<?php $this->show('cart_summary', ['inventoryCheck'=>$inventoryCheck,'hideButtons'=>$hideButtons]); ?>
<div class="col-md-12 check-box table-mar">
    <input id="orderterms" type="checkbox"/>
    <label for="orderterms">Register after accepting Terms of Use and Privacy Policy</label>
    <a href="#" class="policy">Return & Refund Policy</a>
    <a href="#" class="policy">Shipping & Delivery Standards</a>
    <label id="terms_error"></label>
</div>


<?php $rr = \CI::session()->userdata('cartPaymentMethod');
echo $rr['name'];
        if($rr['name'] == 'Moneris'){ ?>
    <ul class="list-inline pull-right">
        <li>                
            <?php
                $gtot = GC::getGrandTotal();
                $store_id = 'D7BM441100';
                $hpp_key = 'hpDT7Y1UHQ96';
                $charge_total = $gtot;
                $url = "https://www3.moneris.com/HPPDP/index.php";
                $dataToSend = "ps_store_id=$store_id&hpp_key=$hpp_key&charge_total=$charge_total&hpp_preload=";
                $ch=curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch, CURLOPT_HEADER,0);
                curl_setopt($ch, CURLOPT_POST,1);
                curl_setopt($ch, CURLOPT_POSTFIELDS,$dataToSend);
                //curl_setopt($ch, CURLOPT_TIMEOUT,0);
                //curl_setopt($ch, CURLOPT_USERAGENT,$gArray['API_VERSION']);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                if(!$response){
                    $response = "error preloading page";
                }else{
                    /*print_r($response);
                    exit();*/
                    $xmlString = new \SimpleXMLElement($response);

                    $ticket = $xmlString->ticket;
                    $order_id = $xmlString->order_id;
                    $response_code = $xmlString->response_code;

                    //echo "data received";
                    //echo "<br>ticket".$ticket.'<br>order-id'.$order_id.'<br>response code'.$response_code.'<br><br><br>';

                    if($response_code < 50){
                        
                        //echo 'data successfully loaded<br>';
                        $oncliloader = "formval()";
                        echo '<form METHOD="POST" action = "https://www3.moneris.com/HPPDP/index.php">';
                        echo '<input type="HIDDEN" name="hpp_id" value="'.$store_id.'">';
                        echo '<input type="hidden" name="hpp_preload" >';
                        echo '<input type="hidden" name="ticket" value="'.$ticket.'">';
                        echo '<button type="SUBMIT" onclick="'.$oncliloader.'" name="SUBMIT" class="login-btn green-bg pull-right table-mar next-step">
                                <span><img src="'.base_url().'assets/img/submit.png" data-pin-nopin="true"></span>
                                <h1>Place Order</h1>
                            </button>';
                        //echo '<input type="SUBMIT" name="SUBMIT" value="click to process">';
                        echo '</form>';
                    }else{
                        echo 'data was not successfully loaded';
                    }

                } 
             ?>
        </li>       
    </ul>
<?php }else{ ?>
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="frmPayPal1" id="frmPayPal1">
    <input type="hidden" name="business" value="rajavel.b-facilitator@awebon.com">
    <input type="hidden" name="cmd" value="_xclick">
    <?php $cartItems = GC::getCartItems();
        foreach ($cartItems as $item)
        {
            if($item->type == 'product')
            {
                $charges['products'][] = $item;
            }
        }
        foreach($charges['products'] as $product){
             $dgs = $product->quantity; 
        }
     ?>
    <input type="hidden" name="quantity" value="<?php echo $dgs; ?>">
    <input type="hidden" name="item_name" value="TargetUs Products">
    <input type="hidden" name="item_number" value="1">
    <input type="hidden" name="amount" value="<?php echo GC::getGrandTotal(); ?>">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="cancel_return" value="<?php echo site_url('/checkout');?>">
    <input type="hidden" name="return" value="<?php echo site_url('/paypal/process-payment');?>">
    <input type="submit" id="mypayjsj" value="" class="post-link pay-now-btn btn btn-green" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="display:none;">
    <ul class="list-inline pull-right">
        <li>
            <button onclick="return formval();" type="submit" name="submit" class="login-btn green-bg pull-right table-mar next-step">
                <span><img src="<?php echo base_url(); ?>assets/img/submit.png" data-pin-nopin="true"></span>
                <h1>Place Order</h1>
            </button>
        </li>   
    </ul>                       
</form>
<?php } ?>
<script type="text/javascript">
function formval(){
    //testHoldon('sk-dot')
    if($("#orderterms").prop('checked') == false){
        $("#terms_error").html("<div class='form-error'><i class='fa fa-exclamation-triangle'></i><p>Please accept Terms of Use and Privacy Policy</p></div>");
        return false;
    }
}
</script>
                            
