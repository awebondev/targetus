<div class="container">
    <div class="row">   
    <div class="col-md-12">
        <div class="breadcrumb">                    
            <span><img src="<?php echo base_url(); ?>assets/img/home.png"></span>               
            <h6>Shopping Cart / Checkout</h6>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div id="main-container" class="categories">
            <h6>Categories</h6>                     
            <div id="tree-contaeinerr">
                <ul class="checkoutt">                    
                    <?php foreach($catlist as $catelistj){ ?>
                        <li><a href="<?php echo base_url().'category/'.$catelistj->slug; ?>">
                        <!-- <input class="catelist_wiz" name="selectcatej" value="<?php echo $catelistj->slug; ?>" type="checkbox" >  -->                       
                        <label> <?php echo $catelistj->name; ?></label></a>
                        </li>
                    <?php  } ?>                    
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-12">
        <section>
            <div class="wizard">
                <div class="wizard-inner">
                    <div class="connecting-line"></div>
                    <ul class="nav nav-tabs" role="tablist">

                        <li role="presentation" class="active">
                            <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1">
                                <span class="round-tab">
                                    <i class="fa fa-map-marker"></i>
                                </span>                            
                            </a>
                        </li>

                        <li role="presentation" class="disabled">
                            <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2">
                                <span class="round-tab">
                                    <i class="fa fa-money"></i>
                                </span>
                            </a>
                        </li>
                        <li role="presentation" class="disabled">
                            <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3">
                                <span class="round-tab">
                                    <i class="fa fa-pencil"></i>
                                </span>
                            </a>
                        </li>

                        <li role="presentation" class="disabled">
                            <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
                                <span class="round-tab">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
               
                    <div class="tab-content" id="checkout">
                        <div class="tab-pane active" role="tabpanel" id="step1">
					         <div class="checkoutAddress">
					        <?php if(!empty($addresses))
					        {
					            $this->show('checkout/address_list', ['addresses'=>$addresses]);
					        }
					        else
					        {
					            ?>
					            <script>
					                $('.checkoutAddress').load('<?php echo site_url('addresses/form');?>');
					            </script>
					            <?php //(new GoCart\Controller\Addresses)->form();
					        }
					        ?>
					        </div>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step2">
                            <div class="white-strips">
                                <div id="checkoutAddresss">
                                    <script>
                                        //$('#checkoutAddresss').load('<?php echo site_url('checkout/orderpayment');?>');
                                    </script>
                                </div>
                               <?php  //$this->show('checkout/orderpayment');  ?>
                            </div>        						
                        </div>
                        <div class="tab-pane" role="tabpanel" id="step3">
                            <div class="white-strips">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <h6>Your Order</h6>
                                    <p class="subtitle">If you would like to edit your order, please go to your shopping cart.</p>
                                </div>
								<div class="col-md-12 col-sm-12 hidden-xs table-mar" id="cart-summary">
								</div>
                                       
                            </div>
                            <ul class="list-inline pull-right">
                                <li>
                                    <a href="#" class="login-btn green-bg pull-right table-mar prev-step">
                                        <span><img src="<?php echo base_url(); ?>assets/img/pre.png" data-pin-nopin="true"></span>
                                        <h1>Preview</h1>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="login-btn green-bg pull-right table-mar orderOverviewStep">
                                        <span><img src="<?php echo base_url(); ?>assets/img/next.png" data-pin-nopin="true"></span>
                                        <h1>Next</h1>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" role="tabpanel" id="complete">
                            <div class="white-strips" id="orderoverview">

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                               
                <!-- </form> -->
                <!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" name="frmPayPal1" id="frmPayPal1"> -->
                                
                                <!-- <input type="submit" value="" class="post-link pay-now-btn btn btn-green" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">  -->
            </div>
        </section>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.catelist_wiz').on('click', function() {
        window.location.href="<?php echo base_url().'category/'; ?>"+this.value;           
    });


    $("#adnext1").hide();
   // alert($("#addr_success").val());
if($("#addr_success").val() == '1'){
    // alert($("#addr_success").val());
    $("#mypayjsj").show();
        $("#mypayjsj").click();
     $("#mypayjsj").hide();
}
});
  function address_validation(){
    //alert('hi');
    if($("#recipient").val() == ""){
      $("#recipienter").html('Name Required');  
      $("#recipient").focus();
      return false;
    }$("#recipienter").html('');
    if($("#city").val() == ""){
      $("#cityer").html('City Required');  
      $("#city").focus();
      return false;
    }$("#cityer").html('');
    $("#adnext1").show();
    $("#adnext1").click();    
    $("#adnext1").hide();
    
    /*$("#adnext").attr("class", "next-step login-btn green-bg pull-right table-mar");
    $("#adnext").click();*/
    return true;
  }
	  $(".save-order-address").click(function (e) {
		  alert('save-order-address');
	        $('.cartSummary').spin();
	        e.preventDefault();
	        $.post($(this).attr('action'), $(this).serialize(), function(data){
	            
	        });
	      var $active = $('.wizard .nav-tabs li.active');
	      $active.next().removeClass('disabled');
	      nextTab($active);
	
	  });

      
      

        $('.orderOverviewStep').on('click', function(event){
            testHoldon('sk-dot');
        	$('#orderoverview').spin();
        	$.post('<?php echo site_url('/checkout/order-overview');?>', '', function(data){
                HoldOn.close();
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
                $('#orderoverview').html(data);
            });
        })
      
      	
	function nextTab(elem) {
	  $(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
	  $(elem).prev().find('a[data-toggle="tab"]').click();
	}

    function getCartSummaryy(callback)
    {
        $('#cart-summary').spin();
        $.post('<?php echo site_url('/checkout/ship-payment-method');?>', function(data) {
            $('#cart-summary').html(data);
            if(callback != undefined)
            {
                callback();
            }
        });
    }
    function getCartSummary(callback)
    {
        //update shipping too
        getShippingMethods();

        $('#orderSummary').spin();
        $.post('<?php echo site_url('cart/summary');?>/hide', function(data) {
            $('#cart-summary').html(data);
            //$("#breadcrumb").hide();
            if(callback != undefined)
            {
                callback();
            }
        });
    }
    function getShippingMethods()
    {
        $('#shippingMethod').load('<?php echo site_url('checkout/shipping-methods');?>');
    }
    function getPaymentMethods()
    {
        $('#paymentMethod').load('<?php echo site_url('checkout/payment-methods');?>');
    }

    <?php if(validation_errors()):
        $errors = \CI::form_validation()->get_error_array(); ?>

        var formErrors = <?php echo json_encode($errors);?>
        
        for (var key in formErrors) {
            if (formErrors.hasOwnProperty(key)) {
                $('[name="'+key+'"]').parent().append('<div class="form-error text-red"><i class="fa fa-exclamation-triangle"></i> '+formErrors[key]+'</div>')
            }
        }
    <?php endif;?>

</script>