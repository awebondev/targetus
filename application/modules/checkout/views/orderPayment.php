<div class="col-md-12" id="cart-summaryship"><a style="color:red;"><?php echo validation_errors(); ?></a>
								<?php echo form_open('/checkout/ship-payment-method', ['id'=>'shippaymentform']);?>                                                
                                <div id="shippingMethod">
					            	<script>
                                         /*$.post('<?php echo site_url('checkout/shipping-methods');?>', function(data){
                                            console.log(data);
                                            alert(data);
                                             if(data == '1'){*/
                                                $('#shippingMethod').load('<?php echo site_url('checkout/shipping-methods');?>');
                                             /*}else{
                                                 window.location.href ="<?php echo site_url('checkout/checkout-wizard');?>";

                                             }

                                         });*/
					            		
						            </script>                                 
                                </div>

                                <div class="table-mar col-xs-12 col-md-12 col-sm-12">
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-xs-12">
                                        <center>
                                            <img src="<?php echo base_url(); ?>assets/img/divided-logo.png" class="img-responsive" data-pin-nopin="true">
                                        </center>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-12">
                                        <img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
                                    </div>
                                </div>

        						<div id="paymentMethod">
					            	<script>
					            		$('#paymentMethod').load('<?php echo site_url('checkout/payment-methods');?>');  
						            </script>          						
        						</div>
                                <ul class="list-inline pull-right">
                                    <li>
                                        <button type="submit" name="submit" class="login-btn green-bg pull-right table-mar">
                                            <span><img src="<?php echo base_url(); ?>assets/img/submit.png" data-pin-nopin="true"></span>
                                            <h1>Next Step</h1>
                                        </button>
                                    </li>
                                </ul>
                                </form>
                            </div> 
                            <script type="text/javascript">                                
                                $('#shippaymentform').on('submit', function(event){
                                     // $('#cart-summaryship').spin();
                                     testHoldon('sk-dot');
                                      event.preventDefault();
                                      $.post($(this).attr('action'), $(this).serialize(), function(data){
                                        //console.log(data);
                                            if(data == 1)
                                           {
                                            HoldOn.close();
                                            //$('#cart-summary').hide();
                                            $.post('<?php echo site_url('/checkout/afterpaymentaddr');?>', '', function(data){
                                                $('#cart-summary').html(data);
                                                $('.quantity  a').attr("onclick","");
                                            });


                                                var $active = $('.wizard .nav-tabs li.active');
                                              $active.next().removeClass('disabled');
                                              nextTab($active);
                                              //$('#cart-summary').html(data);
                                           }else{   
                                            //alert("error");

                                            //$('#cart-summary').hide();
                                            HoldOn.close();
                                              $('#cart-summaryship').html(data);
                                           }
                                      });                   
                                  });

                                <?php if(validation_errors()):

                                    $errors = \CI::form_validation()->get_error_array(); ?>
                                    //alert('error');
                                    var formErrors = <?php echo json_encode($errors);?>
                                    
                                    for (var key in formErrors) {
                                        if (formErrors.hasOwnProperty(key)) {
                                            $('[name="'+key+'"]').parent().append('<div class="form-error text-red"><i class="fa fa-exclamation-triangle"></i> '+formErrors[key]+'</div>')
                                        }
                                    }
                                <?php endif;?>
                            </script>
