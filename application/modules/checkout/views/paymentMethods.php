<div class="col-md-12 col-sm-12 col-xs-12">
    <h6><?php echo lang('payment_methods');?></h6>
    <p class="subtitle">Please choose the method you wish to use to pay for your order.</p>
</div>

<?php if(count($modules) == 0):?>
    <div class="alert">
        <?php echo lang('error_no_payment_method');?>
    </div>
<?php elseif(GC::getGrandTotal() == 0):?>
    <div class="alert">
        <?php echo lang('no_payment_needed');?>
    </div>

    <button class="blue" onclick="SubmitNoPaymentOrder()"><?php echo lang('submit_order');?></button>

    <script>
    function SubmitNoPaymentOrder()
    {

        $.post('<?php echo base_url('/checkout/submit-order');?>', function(data){
            if(data.errors != undefined)
            {
                var error = '<div class="alert red">';
                $.each(data.errors, function(index, value)
                {
                    error += '<p>'+value+'</p>';
                });
                error += '</div>';

                $.gumboTray(error);
            }
            else
            {
                if(data.orderId != undefined)
                {
                    window.location = '<?php echo site_url('order-complete/');?>/'+data.orderId;
                }
            }
        }, 'json');

    }
    </script>

<?php else: ?>
    <div class="paymentError"></div>
    <div class="col-nest">
        <div class="col" data-cols="1/3">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-mar">
                <div class="table">
                <input type="hidden" name="paymentMethod" value="">
                <?php foreach ($modules as $key => $module):?>
                    <?php if($module['class']->isEnabled()):?>

                        <div class="col-md-6 col-sm-6 col-xs-12 left-slider" onclick="$(this).find('input').prop('checked', true).trigger('change');">
                            <input type="radio" name="paymentMethod" value="<?php echo $key;?>">
                            <label for="radio2"></label>
                            <span class="visapay"> 
                                <label><!--?php echo $module['class']->getName();?-->
                                    <!-- <span class="pf pf-diners"></span>
                                    <span class="pf pf-mastercard"></span>
                                    <span class="pf pf-visa"></span>
                                    <span class="pf pf-discover"></span> -->
                                    <?php echo $key;?>
                                </label>
                            </span>                            
                        </div>
                    <?php endif;?>
                <?php endforeach;?>
                <?php echo form_error('paymentMethod'); ?>
                </div>
            </div>
        </div>
<?php endif;?>
