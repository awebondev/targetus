

<?php
        $charges = [];

        $charges['giftCards'] = [];
        $charges['coupons'] = [];
        $charges['tax'] = [];
        $charges['shipping'] = [];
        $charges['products'] = [];

        foreach ($order->items as $item)
        {
            if($item->type == 'gift card')
            {
                $charges['giftCards'][] = $item;
                continue;
            }
            elseif($item->type == 'coupon')
            {
                $charges['coupons'][] = $item;
                continue;
            }
            elseif($item->type == 'tax')
            {
                $charges['tax'][] = $item;
                continue;
            }
            elseif($item->type == 'shipping')
            {
                $charges['shipping'][] = $item;
                continue;
            }
            elseif($item->type == 'product')
            {
                $charges['products'][] = $item;
            }
        }
        ?>

        <?php foreach($charges['products'] as $product):

            $photo = theme_img('no_picture.png', lang('no_image_available'));
            $product->images = array_values(json_decode($product->images, true));

            if(!empty($product->images[0]))
            {
                foreach($product->images as $photo)
                {
                    if(isset($photo['primary']))
                    {
                        $primary = $photo;
                    }
                }
                if(!isset($primary))
                {
                    $tmp = $product->images; //duplicate the array so we don't lose it.
                    $primary = array_shift($tmp);
                }

                $photo = '<img src="'.base_url('product_photo/'.$primary['filename']).'"/>';
            }
             endforeach;
            ?>

<div class="container">
    <div class="row">    
        <div class="col-md-12 col-sm-12 col-xs-12" id="complete">
            <h3>Hi User,</h3>
            <p>Greetings From <a href="#">Targetus.ca</a></p>
            <p>Thankyou for you <?php echo lang('order_number');?>: <?php echo $order->order_number;?></p>
        </div>
        <div class="col-md-12 col-sm-12 hidden-xs table-mar">
            <div class="product-table">
                <div class="col-md-2 no-padding">
                    <div class="title table-propotation">
                        Images
                    </div>
                    <div class="pro-img table-propotation">
                        <div class="orderPhoto">
                            <?php echo $photo;?>
                            <?php echo (!empty($product->sku))?'<div class="orderItemSku">'.lang('sku').': '.$product->sku.'</div>':''?>
                        </div>
                    </div>                                          
                </div>
                <div class="col-md-4 no-padding ">
                    <div class="title table-propotation">
                        Product Name
                    </div>
                    <div class="unit-price-1 table-propotation">                            
                        <p><?php echo $product->name; ?></p>
                    </div>                                  
                </div>
                <div class="col-md-2 no-padding">
                    <div class="title table-propotation">
                        Quantity
                    </div>
                    <div class="unit-price-2 table-propotation">
                        <p>(<?php echo $product->quantity.'  &times; '.format_currency($product->total_price);?>)</p>
                        <?php if(!empty($product->coupon_code)):?>                                    
                        <?php endif;?>
                    </div>                                          
                </div>
                <div class="col-md-2 no-padding">
                    <div class="title table-propotation">
                        Unit Price
                    </div>
                    <div class="unit-price table-propotation">
                        <p><?php echo format_currency( ($product->total_price * $product->quantity) - ($product->coupon_discount * $product->coupon_discount_quantity) ); ?></p>
                    </div>                                          
                </div>
                <div class="col-md-2 no-padding">
                    <div class="title total table-propotation">
                        Total
                    </div>
                    <div class="total table-propotation">
                        <p><?php echo format_currency( ($product->total_price * $product->quantity) - ($product->coupon_discount * $product->coupon_discount_quantity) ); ?></p>
                    </div>                                          
                </div>
            </div>
        </div> 


        <div class="hidden-lg hidden-md hidden-sm col-xs-12 table-mar">
            <div class="product-table hidden-lg hidden-md hidden-sm">
                <div class="col-xs-6 no-padding">
                    <div class="title table-propotation ">
                        Imagess
                    </div>
                    <div class="pro-img table-propotation set-mobile ">
                        <div class="orderPhoto">
                            <?php echo $photo;?>
                            <?php echo (!empty($product->sku))?'<div class="orderItemSku">'.lang('sku').': '.$product->sku.'</div>':''?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 no-padding ">
                    <div class="title table-propotation">
                        Product Name
                    </div>
                    <div class="unit-price-1 table-propotation">
                        <p><?php echo $product->name; ?></p>
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                    <div class="title table-propotation">
                        Quantity
                    </div>
                    <div class="unit-price-2  table-propotation">
                        <p>(<?php echo $product->quantity.'  &times; '.format_currency($product->total_price);?>)</p>
                        <?php if(!empty($product->coupon_code)):?>                                    
                        <?php endif;?>                                
                    </div>
                </div>
                <div class="col-xs-6 no-padding">
                    <div class="title table-propotation">
                        Unit Price
                    </div>
                    <div class="unit-price table-propotation">
                        <p><?php echo format_currency( ($product->total_price * $product->quantity) - ($product->coupon_discount * $product->coupon_discount_quantity) ); ?></p>
                    </div>                        
                </div>
                <div class="col-xs-12 no-padding">
                    <div class="title total table-propotation">
                        Total
                    </div>
                    <div class="total table-propotation mobiles">
                        <p><?php echo format_currency( ($product->total_price * $product->quantity) - ($product->coupon_discount * $product->coupon_discount_quantity) ); ?></p>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
            <div class="row product-table">
                <div class="col-md-6 col-sm-6 col-xs-6 left-cat"><?php echo lang('subtotal');?></div>
                <div class="col-md-6 col-sm-6 col-xs-6 right-cat"><?php echo format_currency($order->subtotal);?></div>
                <div class="col-md-6 col-sm-6 col-xs-6 left-cat"><?php echo lang('shipping');?>: <?php echo $shipping->name; ?></div>
                <div class="col-md-6 col-sm-6 col-xs-6 right-cat"><?php echo format_currency($shipping->total_price); ?></div>                
                <div class="col-md-6 col-sm-6 col-xs-6 left-cat"><?php echo lang('grand_total');?></div>
                <div class="col-md-6 col-sm-6 col-xs-6 right-cat"><?php echo format_currency($order->total);?></div>                                
            </div>                                      
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12" id="address_detial">
            <div class="address_strip">Address Detials</div>
            <div class="col-md-4 no-padding complete_border">
                <div class="shipping_add">
                    <h3><?php echo lang('shipping_address');?></h3>
                   <p> <?php echo format_address([
                    'company'=>$order->shipping_company,
                    'firstname'=>$order->shipping_firstname,
                    'lastname'=>$order->shipping_lastname,
                    'phone'=>$order->shipping_phone,
                    'email'=>$order->shipping_email,
                    'address1'=>$order->shipping_address1,
                    'address2'=>$order->shipping_address2,
                    'city'=>$order->shipping_city,
                    'zone'=>$order->shipping_zone,
                    'zip'=>$order->shipping_zip,
                    'country_id'=>$order->shipping_country_id
                    ]);?></p>
                </div>
            </div>
            <div class="col-md-4 no-padding complete_border">
                <div class="payment_info">
                    <h3><?php echo lang('payment_information');?></h3>
                    <?php foreach($order->payments as $payment):?>
                    <div><?php echo $payment->description;?></div>
                    <?php endforeach;?>
                </div>
            </div>
            <div class="col-md-4 no-padding complete_border_1">
                <div class="billing_add">
                    <h3><?php echo lang('billing_address');?></h3>
                    <p><?php echo format_address([
                    'company'=>$order->billing_company,
                    'firstname'=>$order->billing_firstname,
                    'lastname'=>$order->billing_lastname,
                    'phone'=>$order->billing_phone,
                    'email'=>$order->billing_email,
                    'address1'=>$order->billing_address1,
                    'address2'=>$order->billing_address2,
                    'city'=>$order->billing_city,
                    'zone'=>$order->billing_zone,
                    'zip'=>$order->billing_zip,
                    'country_id'=>$order->billing_country_id
                    ]);?></p>
                </div>
            </div>
        </div>
    </div>
</div>
