<div class="col-md-12 col-sm-12 col-xs-12">
    <h6><?php echo lang('shipping_method');?></h6><?php echo validation_errors(); ?>
    <p class="subtitle">Please choose the method you wish to ship for your order.</p>                                   
    <a href="#" class="notify">This order will be use Flat Rate Shipping or by pick up</a>
</div>
<input type="hidden" id="afetraddrval" value="<?php echo $sj = \CI::session()->userdata('myaddr'); ?>" >
<?php if($requiresShipping):?>
    <div class="shippingError"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-mar">
        <div class="table">
        <input type="hidden" name="shippingMethod" value="">
            <?php
            
            $selectedShippingMethod = \GC::getShippingMethod();
            //print_r($rates);

            foreach ($rates as $key => $rate):
                $hash = md5(json_encode(['key'=>$key, 'rate'=>$rate]));?>
                <div class="col-md-4 col-sm-6 col-xs-12 left-slider" onclick="$(this).find('input').prop('checked', true).trigger('change');">
                    <input type="radio" name="shippingMethod" value="<?php echo $hash;?>" <?php echo (is_object($selectedShippingMethod) && $hash == $selectedShippingMethod->description)?'checked':'';?>>
                    <label for="radio1"></label> 
                    <span class="flatpick">              
                        <label><?php echo format_currency($rate);?></label>     
                        <label>(<?php echo $key;?>)</label>                        
                    </span>                    
                </div>
            <?php endforeach;?>
            <?php echo form_error('shippingMethod'); ?>
        </div>    
    </div>
    <script>
        $(document).ready(function(){
            HoldOn.close();
            if($("#afetraddrval").val()=='1b'){
                window.location.href ="<?php echo site_url('checkout/checkout-wizard');?>";
                $("#error_d").val('ff');
            }
        });
    </script>
<?php else: ?>
    <div class="alert">
        <?php echo lang('no_shipping_needed');?>
    </div>
<?php endif;?>
