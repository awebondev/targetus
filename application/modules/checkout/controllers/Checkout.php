<?php namespace GoCart\Controller;
/**
 * Checkout Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    Checkout
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Checkout extends Front {

    public $customer;

    public function __construct()
    {
        parent::__construct();

        if (config_item('require_login'))
        {
            \CI::Login()->isLoggedIn('checkout');
        }

        $this->customer = \CI::Login()->customer();
    }

    public function index()
    {
        if(\GC::totalItems() > 0)
        {
            $data['addresses'] = \CI::Customers()->get_address_list($this->customer->id);
            $crship = \GC::getShippingMethod(); 
        if(!($hide)){   
            if($crship){
               $upsship = \CI::Orders()->deteordership($crship->id);               
            }            
        }    
            $this->view('checkout', $data);
        }
        else
        {
            $this->view('emptyCart');
        }

    }

    public function submitOrder()
    {
        $errors = \GC::checkOrder();

        if(\GC::getGrandTotal() > 0)
        {
            $errors['payment'] = lang('error_choose_payment');
        }

        if(count($errors) > 0)
        {
            echo json_encode(['errors'=>$errors]);
            return false;
        }
        else
        {
            $payment = [
                'order_id' => \GC::getAttribute('id'),
                'amount' => \GC::getGrandTotal(),
                'status' => 'processed',
                'payment_module' => '',
                'description' => lang('no_payment_needed')
            ];

            \CI::Orders()->savePaymentInfo($payment);

            $orderId = \GC::submitOrder();

            //send the order ID
            echo json_encode(['orderId'=>$orderId]);
            return false;
        }
    }

    public function orderComplete($orderNumber)
    {

         $order = \CI::Orders()->getOrderbyProduct($orderNumber);
          //Configuration
          $set_url = \CI::Settings()->get_settings('PickUpRate');
         
          $access = $set_url['access'];
          $userid = $set_url['userid'];
          $passwd = $set_url['passwd'];
          
          $wsdl = base_url()."assets/shipaddreessconform/Ship.wsdl";
          $operation = "ProcessShipment";
          if($set_url['liveurl']==1){
            $endpointurl = 'https://onlinetools.ups.com/webservices/Ship';
          }else{
            $endpointurl = 'https://wwwcie.ups.com/webservices/Ship';
          }     
          //$outputFileName = 'D:\xampp\htdocs\targetus\assets\shipaddreessconformXOLTResult.xml';  
          try
          {

            $mode = array
            (
                 'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                 'trace' => 1
            );
            // initialize soap client
            $client = new \SoapClient($wsdl , $mode);
            //set endpoint url
            $client->__setLocation($endpointurl);
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new \SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
            $client->__setSoapHeaders($header);

            if(strcmp($operation,"ProcessShipment") == 0 )
            {                
            $resp = $client->__soapCall('ProcessShipment',array(\CI::Customers()->processShipment($order)));
            $shiptrackvalue = $resp->ShipmentResults->PackageResults->TrackingNumber;            
            $ordercomplete = \CI::Orders()->saveship_trackid($orderNumber,$shiptrackvalue);
            }
            else if (strcmp($operation , "ProcessShipConfirm") == 0)
            {
                    //get response
            $resp = $client->__soapCall('ProcessShipConfirm',array(\CI::Customers()->processShipConfirm()));
                 //get status
                echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
               
            }
            else
            {
                $resp = $client->__soapCall('ProcessShipeAccept',array(\CI::Customers()->processShipAccept()));
                //get status
                echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";            
            }

          }
         
          catch(\SoapFault $e)
          {
            //$sj = \CI::session()->set_userdata('myaddr','1b'); 
            //print_r($e);
            echo $addrer = $e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description;
            //echo 'Your address is invalid';
            //print_r($e);
            
            //redirect('checkout/checkout-wizard');
            //return ['UPS Shipping'=> $sj];
          }


       
        $orderCustomer = \CI::Customers()->get_customer($order->customer_id);
        if($orderCustomer->is_guest || $orderCustomer->id == $this->customer->id)
        {
            $this->view('orderComplete', ['order'=>$order]);
        }
        else
        {
            if(!\CI::Login()->isLoggedIn(false,false))
            {
                redirect('login');
            }
            else
            {
                throw_404();
            }
        }
    }

    public function orderCompleteEmail($orderNumber)
    {
        $order = \CI::Orders()->getOrder($orderNumber);
        $this->partial('order_summary_email', ['order'=>$order]);
    }

    public function addressList()
    {
        $data['addresses'] = \CI::Customers()->get_address_list($this->customer->id);
        $this->partial('checkout/address_list', $data);
    }

    public function address()
    {
        $type = \CI::input()->post('type');
        $id = \CI::input()->post('id');

        $address = \CI::Customers()->get_address($id);

        if($address['customer_id'] != $this->customer->id)
        {
            echo json_encode(['error'=>lang('error_address_not_found')]);
        }
        else
        {
            if($type == 'shipping')
            {
                \GC::setAttribute('shipping_address_id',$id);
            }
            elseif($type == 'billing')
            {
                \GC::setAttribute('billing_address_id',$id);
            }

            \GC::saveCart();


            echo json_encode(['success'=>true]);
        }
    }

    public function shippingMethods()
    {
        if(\GC::orderRequiresShipping())
        {
            $this->partial('shippingMethods', [
                'rates'=>\GC::getShippingMethodOptions(),
                'requiresShipping'=>true
            ]);
        }
        else
        {
            $this->partial('shippingMethods', ['rates'=>[], 'requiresShipping'=>false]);
        }
    }

    public function setShippingMethod()
    {
        $rates = \GC::getShippingMethodOptions();
        $hash = \CI::input()->post('method');

        foreach($rates as $key=>$rate)
        {
            $test = md5(json_encode(['key'=>$key, 'rate'=>$rate]));
            if($hash == $test)
            {
                \GC::setShippingMethod($key, $rate, $hash);

                //save the cart
                \GC::saveCart();

                echo json_encode(['success'=>true]);
                return false;
            }
        }


        echo json_encode(['error'=>lang('shipping_method_is_no_longer_valid')]);
    }

    public function paymentMethods()
    {
        global $paymentModules;

        $modules = [];

        $enabled_modules = \CI::Settings()->get_settings('payment_modules');

        foreach($paymentModules as $paymentModule)
        {
            if(array_key_exists($paymentModule['key'], $enabled_modules))
            {
                $className = '\GoCart\Controller\\'.$paymentModule['class'];
                $modules[$paymentModule['key']] = $paymentModule;
                $modules[$paymentModule['key']]['class'] = new $className;    
            }
        }

        ksort($modules);
        $this->partial('paymentMethods', ['modules'=>$modules]);
    }
    
    public function checkoutWizard(){
        $data['catlist'] = \CI::Categories()->get_categories(0);
    	$this->view('checkoutwizard',$data);
    }

    public function orderPayment(){
        $data['catlist'] = '';
        $this->partial('orderPayment',$data);
    }
    
    public function saveShipPaymentMethod(){
    	global $paymentModules;
    	$modules = [];    	
    	$enabled_modules = \CI::Settings()->get_settings('payment_modules');
    	    	
    	
        //exit();
    	//$data = '';
        \CI::load()->library('form_validation');
        \CI::form_validation()->set_rules('shippingMethod', 'shipping method', 'trim|required|max_length[322]');
        \CI::form_validation()->set_rules('paymentMethod', 'payment method', 'trim|required|max_length[312]');

        if (\CI::form_validation()->run() == FALSE)
        {
            /*echo form_error('shippingMethod');
            echo 'gg'.form_error('paymentMethod');
            echo validation_errors();
            exit();*/
            //$selectedPaymentModule = null;
            $this->partial('orderPayment');

        }
        else
        {
            $rates = \GC::getShippingMethodOptions();
            $hash = \CI::input()->post('shippingMethod');

        	foreach($rates as $key=>$rate)
        	{
        		$test = md5(json_encode(['key'=>$key, 'rate'=>$rate]));
        		if($hash == $test)
        		{
        			\GC::setShippingMethod($key, $rate, $hash);
        	
        			//save the cart
        			\GC::saveCart();
                    
                    
                    
        		}
        	}

            $selectedPaymentModule = null;
            foreach($paymentModules as $paymentModule)
            {
                if($paymentModule['key'] == \CI::input()->post('paymentMethod')){
                    $selectedPaymentModule = $paymentModule;
                    break;
                }

            }
            \CI::session()->set_userdata('cartPaymentMethod',$selectedPaymentModule);
            echo 1;
            /*$data['inventoryCheck'] = \GC::checkInventory();
            $data['hideButtons'] = true;
            $this->partial('cart_summary', $data);*/
        }
		

    	
    }
    public function afterpaymentaddr(){
            $data['inventoryCheck'] = \GC::checkInventory();
            $data['hideButtons'] = true;
            $this->partial('cart_summary', $data);
    }
    
    public function showOrderOverview(){
    	$data['inventoryCheck'] = \GC::checkInventory();
    	$data['hideButtons'] = true;	
    	$data['addresses'] = [\CI::Customers()->get_address_list($this->customer->id)];
    	$data['shippingmethod'] = \GC::getShippingMethod()->name;
    	$data['paymentmethod'] =  \CI::session()->userdata('cartPaymentMethod');
    	$cart_address_id =  \CI::session()->userdata('cart_address_id');
    	if(!empty($cart_address_id)){
	    	\GC::setAttribute('shipping_address_id',$cart_address_id);
	    	\GC::setAttribute('billing_address_id',$cart_address_id);
	    	\GC::saveCart();
    	}
    	$this->partial('orderOverview', $data);    	
    }

    public function orderpaymentresult(){
        $sj = \CI::session()->userdata('myaddr');
        if($sj != '1b'){
            echo '1';
        }else{
            echo 'sj';
        }
    }

    public function monerispay(){
        
        $set_url = \CI::Settings()->get_settings('Monerisconfig');
                
        $store_id = $set_url['mon_storeid'];;
        $hpp_key = $set_url['mon_hpkey'];
        $charge_total = $set_url['mon_chargetotal'];
        $url = $set_url['mon_url'];
        $dataToSend = "ps_store_id=$store_id&hpp_key=$hpp_key&charge_total=$charge_total&hpp_preload=";
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HEADER,0);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$dataToSend);
        //curl_setopt($ch, CURLOPT_TIMEOUT,0);
        //curl_setopt($ch, CURLOPT_USERAGENT,$gArray['API_VERSION']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        if(!$response){
            $response = "error preloading page";
        }else{
            /*print_r($response);
            exit();*/
            $xmlString = new \SimpleXMLElement($response);

            $ticket = $xmlString->ticket;
            $order_id = $xmlString->order_id;
            $response_code = $xmlString->response_code;

            echo "data received";
            echo "<br>ticket".$ticket.'<br>order-id'.$order_id.'<br>response code'.$response_code.'<br><br><br>';

            if($response_code < 50){
                echo 'data successfully loaded<br>';
                echo '<form METHOD="POST" action = "https://www3.moneris.com/HPPDP/index.php">';
                echo '<input type="HIDDEN" name="hpp_id" value="'.$store_id.'">';
                echo '<input type="hidden" name="hpp_preload" >';
                echo '<input type="hidden" name="ticket" value="'.$ticket.'">';
                echo '<input type="SUBMIT" name="SUBMIT" value="click to process">';
                echo '</form>';
            }else{
                echo 'data was not successfully loaded';
            }

        }

    }

 
    
}
