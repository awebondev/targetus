<div class="page-header">
    <h2>Credit Card Config:</h2>    
</div>

<div class="row">
<?php echo form_open_multipart('admin/Credit_Card/form'); ?>
    <div class="col-md-6">
        <div class="form-group">
            <label for="enabled"><?php echo lang('moneris_storeid');?> </label>
            <?php echo form_input(['name'=>'mon_storeid', 'value'=>assign_value('mon_storeid', $mon_storeid), 'class' => 'form-control' ]); ?>
        </div>
        <div class="form-group">
            <label for="enabled"><?php echo lang('moneris_hpkey');?> </label>
            <?php echo form_input(['name'=>'mon_hpkey', 'value'=>assign_value('mon_hpkey', $mon_hpkey), 'class' => 'form-control' ]); ?>
        </div> 
        <div class="form-group">
            <label for="enabled"><?php echo lang('moneris_chargetot');?> </label>
            <?php echo form_input(['name'=>'mon_chargetotal', 'value'=>assign_value('mon_chargetotal', $mon_chargetotal), 'class' => 'form-control' ]); ?>
        </div>
        <div class="form-group">
            <label for="enabled"><?php echo lang('moneris_url');?> </label>
            <?php echo form_input(['name'=>'mon_url', 'value'=>assign_value('mon_url', $mon_url), 'class' => 'form-control' ]); ?>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
        </div>
    </div>   
</form>
</div> 
<script type="text/javascript">
/*$('#country').change(function(){
            $('#zone').load('<?php echo site_url('addresses/get-zone-options');?>/'+$('#country').val());
        });
$('form').submit(function() {
    $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
});
$(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $("#liveurl").val(1);
            }
            else if($(this).prop("checked") == false){
                $("#liveurl").val(0);
            }
        });
    });*/
</script>