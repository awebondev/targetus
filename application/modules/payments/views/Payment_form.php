<div class="container">
    <div class="row">   
    <div class="col-md-12">
        <div class="breadcrumb">                    
            <span><img src="<?php echo base_url(); ?>assets/img/home.png"></span>               
            <h6>Shopping Cart / Checkout</h6>
        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-12">
        <div id="main-container" class="categories">
            <h6>Categories</h6>                     
            <div id="tree-container"></div>
        </div>
    </div>
    <div class="col-md-9 col-sm-9 col-xs-12">
        <section>
            <div class="wizard">
               

                <form role="form">
                    <div class="tab-content" id="checkout">
                        <div class="tab-pane active" role="tabpanel" id="step1">
                            <div class="col-md-12 col-sm-12 col-xs-12">                                
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <form method="post" >                                      
                                    <div class="white-strips">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <h6>Payment Details<?php echo validation_errors(); ?></h6>
                                        <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive wizard">
                                    </div>                                  
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Credit caed holder name:</label>                           
                                                <input class="form-control" name="ccname" type="input" placeholder="card holder name">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>CC Number:</label>
                                                <input class="form-control" name="ccnumber" type="input" placeholder="card number">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Expiry date:</label>                           
                                                <input class="form-control" name="expdate" type="input" placeholder="Expiry date">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label>Security code:</label>
                                                <input class="form-control" name="ccsecode" type="input" placeholder="secure code">
                                            </div>
                                        </div>
                                        <ul class="list-inline pull-right">
                                            <li>
                                                <a href="#" class="login-btn green-bg pull-right table-mar prev-step">
                                                    <span><img src="<?php echo base_url(); ?>assets/img/cancel.png" data-pin-nopin="true"></span>
                                                    <h1>Cancel</h1>
                                                </a>
                                            </li>
                                            <li>
                                                <button type="submit" class="login-btn green-bg pull-right table-mar next-step">
                                                    <span><img src="<?php echo base_url(); ?>assets/img/submit.png" data-pin-nopin="true"></span>
                                                    <h1>pay now</h1>
                                                </button>
                                            </li>
                                        </ul>                                                                                                                           
                                    </div>                                              
                                </form>
                            </div>
                            
                        </div>
                        
                        
                        
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </section>
        </div>
    </div>
</div>
