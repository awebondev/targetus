<?php namespace GoCart\Controller;
/**
 * AdminPayments Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    AdminPayments
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class AdminPayments extends Admin {

    public function index()
    {
        \CI::auth()->check_access('Admin', true);

        \CI::lang()->load('settings');
        \CI::load()->helper('inflector');
        
        global $paymentModules;

        $data['payment_modules'] = $paymentModules;
        $data['enabled_modules'] = \CI::Settings()->get_settings('payment_modules');

        $data['page_title'] = lang('common_payment_modules');
        $this->view('payment_index', $data);
    }
    public function monerisconfig(){
        \CI::load()->helper('form');
        \CI::load()->library('form_validation');

        \CI::form_validation()->set_rules('mon_storeid', 'lang:moneris_storeid', 'required');
        \CI::form_validation()->set_rules('mon_hpkey', 'lang:moneris_hpkey', 'required');
        \CI::form_validation()->set_rules('mon_chargetotal', 'lang:moneris_chargetot', 'required');
        \CI::form_validation()->set_rules('mon_url', 'lang:moneris_url', 'required');

        if (\CI::form_validation()->run() == FALSE)
        {
            $settings = \CI::Settings()->get_settings('Monerisconfig');

            $this->view('moneris_pay', $settings);
        }
        else
        {
            
            \CI::Settings()->save_settings('Monerisconfig', \CI::input()->post());
            redirect('admin/payments');
        }
        
    }
}