<?php namespace GoCart\Controller;
/**
 * Addresses Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    Addresses
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Userpayments extends Front {

    var $customer;

    public function __construct()
    {
        parent::__construct();

        \CI::load()->model(array('Locations'));
        $this->customer = \CI::Login()->customer();
    }

    public function index()
    {
        //make sure they're logged in
        \CI::Login()->isLoggedIn('my_account/');
        $data['customer'] = $this->customer;
        $data['addresses'] = \CI::Customers()->get_address_list($this->customer->id);

        $this->partial('addresses', $data);
    }

    public function paypalreturn(){
        //$data['orderid'] = $orderid;
        //$data['amount'] = $amt;

        // \CI::Customers()->save_payments($data);

         /*$errors = \GC::checkOrder();
        if(count($errors) > 0)
        {
            echo json_encode(['errors'=>$errors]);
            return false;
        }
        else
        {*/
            $payment = [
                'order_id' => \GC::getAttribute('id'),
                'amount' => \GC::getGrandTotal(),
                'status' => 'processed',
                'payment_module' => 'Cod',
                'description' => lang('charge_on_delivery')
            ];

            \CI::Orders()->savePaymentInfo($payment);

            $orderId = \GC::submitOrder();

            \CI::session()->unset_userdata('order_id');
            \CI::session()->unset_userdata('order_amount');

            //send the order ID
            echo json_encode(['orderId'=>$orderId]);
            return false;
        /*}*/
    }



    //payment functions


    public function getSecureToken($amt, $billing_info, $shipping_info){
    $credStr = $this->getCredStr();
    $constStr = $this->getConstStr();
    $billingStr = $this->getString($billing_info);
    $shippingStr = $this->getString($shipping_info);
    
    $reqStr = $credStr . $billingStr . $shippingStr . "&AMT=" . $amt . $constStr;
 
    $response = $this->PPHttpPost(PAYFLOW_ENDPOINT, $reqStr);
    
    return $response;
    
    }
    /*PPHttpPost(string, string)  
     *  
     *PPHttpPost takes in two strings, and   
     *makes a curl request and returns the result.  
     *  
     *@return array $httpResponseAr  
     */ public function PPHttpPost($endpoint, $nvpstr){ 
     // setting the curl parameters. 
     $ch = curl_init(); 
     curl_setopt($ch, CURLOPT_URL, $endpoint); 
     curl_setopt($ch, CURLOPT_VERBOSE, 1); 
     // turning off the server and peer verification(TrustManager Concept). 
     curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
     curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE); 
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
     curl_setopt($ch, CURLOPT_POST, 1); 
     // setting the NVP $my_api_str as POST FIELD to curl 
     curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpstr); 
     // getting response from server 
     $httpResponse = curl_exec($ch); 
     if(!$httpResponse) {   
            $response = "API failed: ".curl_error($ch).'('.curl_errno($ch).')';   
            return $response; 
            } 
        $httpResponseAr = explode("&", $httpResponse); 
        return $httpResponseAr;
        }//end PPHttpPost function
 
    /*getString(array)
     *
     *Takes in an array and turns it into a string:
     *    array(
     *      ['NAME'] => "Value",
     *      ['NAME2'] => "Value2"
     *      );
     *
     *The above array turns into:
     * 
     * NAME=Value&NAME2=Value2
     *
     *@return string $string
     */
    public function getString($array){
        $string = "";
        if(is_array($array)){
                foreach($array as $k => $v){
                    $string .= "&" . $k . "=" . $v;
                    }
        }
        else { $string = ""; }
    return $string;
    }//end getString    
 
    /*getCredStr()
    *
    *Builds the Credential String based on the variables set in the constructor above.
    *
    *@return string $string
    */
    public function getCredStr(){
        $string = "USER=" . PAYFLOW_USER . "&VENDOR=" . PAYFLOW_VENDOR . "&PARTNER=" . PAYFLOW_PARTNER . "&PWD=" . PAYFLOW_PWD;
        return $string;
    }//end setCredStr()
 
    /*getConstStr().
     *
     *Builds the constant String based on the variables set in the constructor above.
     *
     *@return string $string;
     */
    public function getConstStr(){
 
        $returnURL = PAYFLOW_BASEURL . "pages/member/payment_success"; //your return url
        $cancelURL = PAYFLOW_BASEURL . "pages/member/payment_success"; //your cancel url
        $errorURL = PAYFLOW_BASEURL . "pages/member/payment_success"; //your error url
        $silentTran = "TRUE"; //should be True with this script; it is required for Transparent Redirect
        $trxtype = "A"; //"A" = Auth, "S" = Sale
        $currency = "USD"; //Currency Code "USD", "CAD", etc.
        $verbosity = "HIGH"; //should be "HIGH" with this script
        $createsecuretoken = "Y"; //SecureToken must be used with Transparent Redirect
        $tender = "C"; //Tender should equal C with Transparent Redirect
        
        $secureTokenID = md5(time());
        $string = "&RETURNURL=" . $returnURL . "&CANCELURL=" . $cancelURL . "&ERRORURL=" . $errorURL
                  . "&TRXTYPE=" . $trxtype . "&SILENTTRAN=" . $silentTran . "&CURRENCY=" . $currency
                  . "&VERBOSITY=" . $verbosity . "&CREATESECURETOKEN=" . $createsecuretoken . "&TENDER=" . $tender . "&SECURETOKENID="
                  . $secureTokenID;
        return $string;
    }//end getConstStr
 
 
     /*getMySecureToken(array)  
      *  
      *This takes in the response array from the   
      *PPHttpPost call, and parses out the SecureToken  
      *  
      *This is used because the securetoken may contain  
      *an "=" sign  
      *  
      *@return string $secure_token  
      */ 
     public function getMySecureToken($response){  
        $secure_token = $response[2];  
        $secure_token = substr($secure_token, -25);  
        return $secure_token; 
    }//end getMySecureToken()
 
    /*parseResponse(array)  
      *  
      *This function parses out the response without taking  
      *into account that the securetoken may contain an "="  
      *sign. The only thing you need from this is the   
      *SecureTokenID.  
      *  
      *@return array $parsed_response  
      */ 
      public function parseResponse($response){ 
            $parsed_response = array(); 
                foreach ($response as $i => $value)  {  
                    $tmpAr = explode("=", $value);  
                        if(sizeof($tmpAr) > 1) {   
                        $parsed_response[$tmpAr[0]] = $tmpAr[1];  
                        } 
                    } 
                    return $parsed_response; 
        }//end parseResponse
 
    //END PAYMENT FUNCTION 








    public function payments($id = 0)
    {

        \CI::load()->library('form_validation');
        \CI::form_validation()->set_rules('ccname', 'ccname', 'required|trim|max_length[100]');
        \CI::form_validation()->set_rules('ccnumber', 'ccnumber', 'trim|required|max_length[20]');
        \CI::form_validation()->set_rules('expdate', 'expdate', 'trim|required|max_length[32]');
        \CI::form_validation()->set_rules('ccsecode', 'ccsecode', 'trim|required|valid_email|max_length[5]');
        

        if (\CI::form_validation()->run() == FALSE)
        {
            $this->view('payment_form');
        }
        else
        {
            
            $a = [];
            $a['id'] = '';
            $a['ccname'] = \CI::input()->post('ccname');
            $a['ccnumber'] = \CI::input()->post('ccnumber');
            $a['expdate'] = \CI::input()->post('expdate');
            $a['ccsecode'] = \CI::input()->post('ccsecode');
            print_r($a);
            /*\CI::Customers()->save_address($a);
            echo 1;*/
        }
    }

    public function delete($id)
    {
        \CI::Customers()->delete_address($id, $this->customer->id);
        echo 1;
    }

    public function getZoneOptions($id)
    {
        $zones  = \CI::Locations()->get_zones_menu($id);

        foreach($zones as $id=>$z):?>

            <option value="<?php echo $id;?>"><?php echo $z;?></option>

        <?php endforeach;
    }
}
