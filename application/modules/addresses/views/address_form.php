<?php $dd = \CI::session()->set_userdata('myaddr','1b'); ?>
<div id="addressFormWrapper" class="col-md-12 col-sm-12 col-xs-12">
	<div class="white-strips">
        <div class="col-md-12 col-sm-12 col-xs-12">   
        <!-- <input type='text' id="error_d" value=""> -->
            <p style="color:red;"><?php echo $sj = \CI::session()->userdata('addr_error'); ?></p>
            <h6>Confirm Shipping Address</h6>
            <p class="subtitle">Please confirm your shipping address details.</p>
            <img src="<?php echo base_url(); ?>assets/img/line-2.png" class="img-responsive wizard">
        </div>    
        <div id="addressError" class="alert red hide"></div>
        
        <?php echo form_open('addresses/form/'.$id, ['id'=>'addressForm']);?>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <select class="form-control" id="address_list" name="address_list" >
                    <option value="">select your address</option>
                    <?php foreach($alladdress as $data){ ?>
                    <option value="<?php echo $data->id; ?>"><?php echo $data->address1.','.$data->city; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
            	<label><?php echo lang('address_company');?></label>
                <?php echo form_input(['name'=>'company','id'=>'company', 'class'=>'form-control', 'value'=> assign_value('company',$company)]);?>
            </div>
        </div>
        <input type="hidden" name="hidid" id="hidid" value="<?php echo $cid; ?>">
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="required"><?php echo lang('address_firstname');?></label>
                <?php echo form_input(['name'=>'firstname','id'=>'firstname', 'class'=>'form-control', 'autocomplete'=>'fname',  'value'=> assign_value('firstname',$firstname)]);?>
            </div>
		</div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="required"><?php echo lang('address_lastname');?></label>
                <?php echo form_input(['name'=>'lastname','id'=>'lastname', 'class'=>'form-control', 'autocomplete'=>'lname', 'value'=> assign_value('lastname',$lastname)]);?>
            </div>
        </div>
        
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="required"><?php echo lang('address_email');?></label>
                <?php echo form_input(['name'=>'email','id'=>'email', 'class'=>'form-control', 'autocomplete'=>'email', 'value'=>assign_value('email',$email)]);?>
            </div>
       </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="required"><?php echo lang('address_phone');?></label>
                <?php echo form_input(['name'=>'phone','id'=>'phone', 'class'=>'form-control', 'autocomplete'=>'tel', 'value'=> assign_value('phone',$phone)]);?>
            </div>
        </div>
       <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="required"><?php echo lang('address');?></label>
                <?php
                echo form_input(['name'=>'address1','id'=>'address1', 'class'=>'form-control check-box', 'autocomplete'=>'street-address',  'value'=>assign_value('address1',$address1)]);
                echo form_input(['name'=>'address2', 'id'=>'address2','class'=>'form-control', 'autocomplete'=>'street-address',  'value'=> assign_value('address2',$address2)]);
                ?>
            </div>
        </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label><?php echo lang('address_country');?></label>
                <?php echo form_dropdown('country_id',  $countries_menu, assign_value('country_id', $country_id), $ar = array('id'=>'country_id','class'=>'form-control'));?>
            </div>
        </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label class="required"><?php echo lang('address_city');?></label>
                <?php echo form_input(['name'=>'city','id'=>'city', 'autocomplete'=>'locality', 'class'=>'form-control', 'value'=>assign_value('city',$city)]);?>
            </div>
       </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">
                <label><?php echo lang('address_state');?></label>
                <?php echo form_dropdown('zone_id', $zones_menu, assign_value('zone_id', $zone_id), $ar = array('id'=>'zone_id','class'=>'form-control'));?>
            </div>
        </div>
       <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form-group">        
                <label class="required"><?php echo lang('address_zip');?></label>
                <?php echo form_input(['maxlength'=>'10', 'class'=>'form-control', 'autocomplete'=>'postal-code', 'name'=>'zip', 'id'=>'zip', 'value'=> assign_value('zip',$zip)]);?>
            </div>
        </div>
        <ul class="list-inline pull-right">
            <li>
                <button type="submit" name="submit" class="login-btn green-bg pull-right table-mar">
                    <span><img src="<?php echo base_url(); ?>assets/img/submit.png" data-pin-nopin="true"></span>
                    <h1>Next Step</h1>
                </button>
            </li>
        </ul>
    </form>
    
    </div>
</div>

    <script>
    $(function(){
        $('#country_id').change(function(){
            $('#zone_id').load('<?php echo site_url('addresses/get-zone-options');?>/'+$('#country_id').val());
        });

        $('#addressForm').on('submit', function(event){
            $('.addressFormWrapper').spin();
            event.preventDefault();
            $.post($(this).attr('action'), $(this).serialize(), function(data){
                if(data == 1)
                {
                    testHoldon('sk-dot');
                    $('#checkoutAddresss').load('<?php echo site_url('checkout/orderpayment');?>');
                    var $active = $('.wizard .nav-tabs li.active');
                    $active.next().removeClass('disabled');
                    nextTab($active);
                }else{
                    HoldOn.close();
                    $('#addressFormWrapper').html(data);
                }
            });
        })
    });

    <?php if(validation_errors()):
        $errors = \CI::form_validation()->get_error_array(); ?>

        var formErrors = <?php echo json_encode($errors);?>
        
        for (var key in formErrors) {
            if (formErrors.hasOwnProperty(key)) {
                $('[name="'+key+'"]').parent().append('<div class="form-error text-red"><i class="fa fa-exclamation-triangle"></i><p>'+formErrors[key]+
                    '</p></div>')
            }
        }
    <?php endif;?>
    
    </script>
