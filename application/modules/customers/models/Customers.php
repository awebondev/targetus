<?php
Class Customers extends CI_Model
{
    public function createGuest()
    {
        return $this->save([
            'id'=>false,
            'firstname'=>'',
            'lastname'=>'',
            'email'=>'',
            'email_subscribe'=>1,
            'phone'=>'',
            'company'=>'',
            'password'=>'',
            'active'=>1,
            'group_id'=>1,
            'confirmed'=>0,
            'is_guest'=>1,
        ]);
    }

    public function get_customers($limit=0, $offset=0, $order_by='id', $direction='DESC',$cusearch=null)
    {
        CI::db()->where('is_guest', 0)->order_by($order_by, $direction);
        if($limit>0)
        {
            CI::db()->limit($limit, $offset);
        }
        if($cusearch){
          CI::db()->like('lastname', $cusearch);
          CI::db()->or_like('firstname', $cusearch);
          CI::db()->or_like('email', $cusearch);
        }
        $result = CI::db()->get('customers');
        return $result->result();
    }

    public function get_customer_export($limit=0, $offset=0, $order_by='id', $direction='DESC')
    {
        return CI::db()->where('is_guest', 0)->get('customers')->result();
    }

    public function count_customers()
    {
        return CI::db()->where('is_guest', 0)->count_all_results('customers');
    }

    public function get_customer($id)
    {

        $result = CI::db()->get_where('customers', array('id'=>$id));
        return $result->row();
    }

    public function get_address_list($id)
    {
        return CI::db()->where('deleted', 0)->
                order_by('country', 'ASC')->
                order_by('zone', 'ASC')->
                order_by('city', 'ASC')->
                order_by('address1', 'ASC')->
                order_by('address2', 'ASC')->
                order_by('company', 'ASC')->
                order_by('firstname', 'ASC')->
                order_by('lastname', 'ASC')->
                where('customer_id', $id)->
                get('customers_address_bank')->row_array();
    }

    public function get_address_listbyid($id)
    {
        return CI::db()->where('deleted', 0)->
                order_by('country', 'ASC')->
                order_by('zone', 'ASC')->
                order_by('city', 'ASC')->
                order_by('address1', 'ASC')->
                order_by('address2', 'ASC')->
                order_by('company', 'ASC')->
                order_by('firstname', 'ASC')->
                order_by('lastname', 'ASC')->
                where('id', $id)->
                get('customers_address_bank')->row_array();
    }

    public function count_addresses($id)
    {
        return CI::db()->where('deleted', 0)->where('customer_id', $id)->from('customers_address_bank')->count_all_results();
    }

    public function get_alladdress_byuser($id)
    {
        return CI::db()->where('deleted', 0)->where('customer_id', $id)->from('customers_address_bank')->get()->result();
    }

    /*public function cust_addresses($id)
    {
        return CI::db()->where('customer_id', $id)->get('customers_address_bank')->row_array();
    }*/

    public function get_address($address_id)
    {
        return CI::db()->where('id', $address_id)->get('customers_address_bank')->row_array();
    }

    public function save_address($data)
    {
        if(!empty($data['id']))
        {
            /***************************
            when saving an address that already exists, make sure it's not in use before updating it.
            if it is in use, set the previous instance to deleted and insert the changes as a new record
            ****************************/
            $used = CI::db()->where('shipping_address_id', $data['id'])->or_where('billing_address_id', $data['id'])->count_all_results('orders');

            if($used > 0)
            {
                CI::db()->where('id', $data['id']);
                CI::db()->update('customers_address_bank', ['deleted'=>1]);
                
                $data['id'] = false;// set ID to false
                CI::db()->insert('customers_address_bank', $data);
                return CI::db()->insert_id();
            }
            else
            {
                CI::db()->where('id', $data['id']);
                CI::db()->update('customers_address_bank', $data);
                return $data['id'];
            }

        } else {
            CI::db()->insert('customers_address_bank', $data);
            return CI::db()->insert_id();
        }
    }

    public function delete_address($id, $customer_id)
    {
        CI::db()->where(array('id'=>$id, 'customer_id'=>$customer_id))->update('customers_address_bank', ['deleted'=>1]);
        return $id;
    }

    public function save($customer)
    {
        if($customer['id'])
        {
            CI::db()->where('id', $customer['id']);
            CI::db()->update('customers', $customer);
            return $customer['id'];
        }
        else
        {
            CI::db()->insert('customers', $customer);
            return CI::db()->insert_id();
        }
    }

    public function deactivate($id)
    {
        $customer = array('id'=>$id, 'active'=>0);
        $this->save_customer($customer);
    }

    public function delete($id)
    {
        /*
        deleting a customer will remove all their orders from the system
        this will alter any report numbers that reflect total sales
        deleting a customer is not recommended, deactivation is preferred
        */

        //this deletes the customers record
        CI::db()->where('id', $id);
        CI::db()->delete('customers');

        // Delete Address records
        CI::db()->where('customer_id', $id);
        CI::db()->delete('customers_address_bank');

        //get all the orders the customer has made and delete the items from them
        CI::db()->select('id');
        $result = CI::db()->get_where('orders', array('customer_id'=>$id));
        $result = $result->result();
        foreach ($result as $order)
        {
            CI::db()->where('order_id', $order->id);
            CI::db()->delete('order_items');
        }

        //delete the orders after the items have already been deleted
        CI::db()->where('customer_id', $id);
        CI::db()->delete('orders');
    }

    public function check_email($str, $id=false)
    {
        CI::db()->select('email');
        CI::db()->from('customers');
        CI::db()->where('is_guest', 0)->where('email', $str);
        if ($id)
        {
            CI::db()->where('id !=', $id);
        }
        $count = CI::db()->count_all_results();

        if ($count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function reset_password($email)
    {
        CI::load()->library('encrypt');
        $customer = $this->get_customer_by_email($email);
        if ($customer)
        {
            CI::load()->helper('string');
            CI::load()->library('email');

            $newPassword = random_string('alnum', 8);
            $customer['password'] = sha1($newPassword);
            $this->save($customer);

            GoCart\Emails::resetPasswordCustomer($newPassword, $email);
            
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_customer_by_email($email)
    {
        $result = CI::db()->get_where('customers', array('email'=>$email));
        return $result->row_array();
    }

    // Customer groups public functions
    public function get_groups()
    {
        return CI::db()->get('customer_groups')->result();
    }

    public function get_group($id)
    {
        return CI::db()->where('id', $id)->get('customer_groups')->row();
    }

    public function delete_group($id)
    {
        CI::db()->where('id', $id);
        CI::db()->delete('customer_groups');
    }

    public function save_group($data)
    {
        if(!empty($data['id']))
        {
            CI::db()->where('id', $data['id'])->update('customer_groups', $data);
            return $data['id'];
        } else {
            CI::db()->insert('customer_groups', $data);
            $groupId = CI::db()->insert_id();

            //create the new fields.
            CI::load()->dbforge();
            $fields = [
                'enabled_'.$groupId=>[
                    'type'=>'TINYINT',
                    'constraint'=>'1',
                    'default'=>'1'
                ],
                'price_'.$groupId=>[
                    'type'=>'DECIMAL', 
                    'constraint'=>'10,2',
                    'default'=>'0.00'
                ],
                'saleprice_'.$groupId=>[
                    'type'=>'DECIMAL', 
                    'constraint'=>'10,2',
                    'default'=>'0.00'
                ]
            ];
            CI::dbforge()->add_column('products', $fields);
            CI::dbforge()->add_column('order_items', $fields);

            $fields = [
                'enabled_'.$groupId=>[
                    'type'=>'TINYINT',
                    'constraint'=>'1',
                    'default'=>'1'
                ]
            ];
            CI::dbforge()->add_column('categories', $fields);
        }
    }
     public function processRate($sj=null)
    {
        $sj = [];
        $sj = CI::session()->userdata('current_pr_det');
        //print_r($sjs);
        //echo 'fgf'.$sjs.'ff';

        $cartItems = \GC::getCartItems();

        /*print_r($cartItems);
        exit();*/
            foreach ($cartItems as $item)
            {
                $charges['products'][] = $item;
            }
           
            $set_url = \CI::Settings()->get_settings('PickUpRate');
            $country = \CI::Locations()->get_country($set_url['country']);
            $zone    = \CI::Locations()->get_zone($set_url['zone']);
                
            $zonecode = $zone->code;  // save the state for output formatted addresses
            $countrycode = $country->iso_code_2; // some shipping libraries require the code
                    
                 
            //$access = $set_url['address'];
            
          //create soap request
          $option['RequestOption'] = 'Shop';
          $request['Request'] = $option;

          $pickuptype['Code'] = '01';
          $pickuptype['Description'] = 'Daily Pickup';
          $request['PickupType'] = $pickuptype;

          $customerclassification['Code'] = '01';
          $customerclassification['Description'] = 'Classfication';
          $request['CustomerClassification'] = $customerclassification;

          $shipper['Name'] = $set_url['sname'];
          $shipper['ShipperNumber'] = $set_url['shipper_number']; //old number 222006
          $address['AddressLine'] = array
          (
              $set_url['address'].','.$set_url['city'].','.$zonecode.' '.$set_url['zipcode'].' '.$country->name
          );
          $address['City'] = $set_url['city'];
          $address['StateProvinceCode'] = $zonecode;
          $address['PostalCode'] = $set_url['zipcode'];
          $address['CountryCode'] = $countrycode;
          $shipper['Address'] = $address;
          $shipment['Shipper'] = $shipper;

          $shipto['Name'] = $sj['firstname'].' '.$sj['lastname'];
          $addressTo['AddressLine'] = $sj['address1'].','.$sj['address2'];
          $addressTo['City'] = $sj['city'];
          $addressTo['StateProvinceCode'] = $sj['zone'];
          $addressTo['PostalCode'] = $sj['zip'];
          $addressTo['CountryCode'] = $sj['country_code'];
          $addressTo['ResidentialAddressIndicator'] = '';
          $shipto['Address'] = $addressTo;
          $shipment['ShipTo'] = $shipto;

          $shipfrom['Name'] = $set_url['sname'];
          $addressFrom['AddressLine'] = array
          (
              $set_url['address'].','.$set_url['city'].','.$zonecode.' '.$set_url['zipcode'].' '.$country->name
          );
          $addressFrom['City'] = $set_url['city'];
          $addressFrom['StateProvinceCode'] = $zonecode;
          $addressFrom['PostalCode'] = $set_url['zipcode'];
          $addressFrom['CountryCode'] = $countrycode;
          $shipfrom['Address'] = $addressFrom;
          $shipment['ShipFrom'] = $shipfrom;
          /*print_r($address);
          print_r($addressFrom);
          exit();*/
        
          $service['Code'] = '03';
          $service['Description'] = 'Service Code';
          $shipment['Service'] = $service;   

          $shipment['Package'] = [];

        foreach($charges['products'] as $product){    
          $packaging1['Code'] = '02';
          $packaging1['Description'] = 'Rate';
          $package1['PackagingType'] = $packaging1;
          $dunit1['Code'] = 'IN';
          $dunit1['Description'] = 'inches';
          $dimensions1['Length'] = $totweight = $product->length*$product->quantity;
          $dimensions1['Width'] = $totquan = $product->width*$product->quantity;
          $dimensions1['Height'] = '10';
          $dimensions1['UnitOfMeasurement'] = $dunit1;
          $package1['Dimensions'] = $dimensions1;
          $punit1['Code'] = 'LBS';
          $punit1['Description'] = 'Pounds';
          $packageweight1['Weight'] = $totweigt = $product->weight*$product->quantity;
          $packageweight1['UnitOfMeasurement'] = $punit1;
          $package1['PackageWeight'] = $packageweight1;
          $shipment['Package'][] = $package1;
        }

          /*$packaging2['Code'] = '02';
          $packaging2['Description'] = 'Rate';
          $package2['PackagingType'] = $packaging2;
          $dunit2['Code'] = 'IN';
          $dunit2['Description'] = 'inches';
          $dimensions2['Length'] = '3';
          $dimensions2['Width'] = '5';
          $dimensions2['Height'] = '8';
          $dimensions2['UnitOfMeasurement'] = $dunit2;
          $package2['Dimensions'] = $dimensions2;
          $punit2['Code'] = 'LBS';
          $punit2['Description'] = 'Pounds';
          $packageweight2['Weight'] = '2';
          $packageweight2['UnitOfMeasurement'] = $punit2;
          $package2['PackageWeight'] = $packageweight2;
*/
         // $shipment['Package'] = array( $package1 , $package2 );
          $shipment['ShipmentServiceOptions'] = '';
          $shipment['LargePackageIndicator'] = '';
          $request['Shipment'] = $shipment;
          /*echo "Request.......\n";
          print_r($request);
          echo "\n\n";*/
          return $request;
      }

      function processShipment($order)
      {
            $set_url = \CI::Settings()->get_settings('PickUpRate');
            $country = \CI::Locations()->get_country($set_url['country']);
            $zone    = \CI::Locations()->get_zone($set_url['zone']);
                
            $zonecode = $zone->code;  // save the state for output formatted addresses
            $countrycode = $country->iso_code_2; // some shipping libraries require the code
                    

        $sj = \CI::session()->userdata('current_pr_det');
        $set_url = \CI::Settings()->get_settings('PickUpRate');
        //$access = $set_url['address'];
        //create soap request
          $requestoption['RequestOption'] = 'nonvalidate';
          $request['Request'] = $requestoption;

          $shipment['Description'] = 'Ship WS test';
          $shipper['Name'] = $set_url['sname'];
          $shipper['AttentionName'] = 'ca';
          $shipper['TaxIdentificationNumber'] = '123456';
          $shipper['ShipperNumber'] = $set_url['shipper_number'];
          $address['AddressLine'] = $set_url['address'];
          $address['City'] = $set_url['city'];
          $address['StateProvinceCode'] = $zonecode;
          $address['PostalCode'] = $set_url['zipcode'];
          $address['CountryCode'] = $countrycode;
          //exit();
          $shipper['Address'] = $address;
          $phone['Number'] = $set_url['phnumber'];
          $phone['Extension'] = '1';
          $shipper['Phone'] = $phone;
          $shipment['Shipper'] = $shipper;
          $shipto['Name'] = $sj['firstname'];
          $shipto['AttentionName'] = $sj['lastname'];
          $addressTo['AddressLine'] = $sj['address1'].','.$sj['address2'];
          //$addressTo['AddressLine'] = '8500 Beverly Blvd';
          $addressTo['City'] = $sj['city'];
          $addressTo['StateProvinceCode'] = $sj['zone'];
          $addressTo['PostalCode'] = $sj['zip'];
          $addressTo['CountryCode'] = $sj['country_code'];
          $phone2['Number'] = $sj['phone'];
          $shipto['Address'] = $addressTo;
          $shipto['Phone'] = $phone2;
          $shipment['ShipTo'] = $shipto;
          //exit();
          $shipfrom['Name'] = $set_url['sname'];
          $shipfrom['AttentionName'] = 'ca';
          $addressFrom['AddressLine'] = $set_url['address'];
          $addressFrom['City'] = $set_url['city'];
          $addressFrom['StateProvinceCode'] = $zonecode;
          $addressFrom['PostalCode'] = $set_url['zipcode'];
          $addressFrom['CountryCode'] = $countrycode;
          $phone3['Number'] =  $set_url['phnumber'];
          $shipfrom['Address'] = $addressFrom;
          $shipfrom['Phone'] = $phone3;
          $shipment['ShipFrom'] = $shipfrom;

          $shipmentcharge['Type'] = '01';
          /*$creditcard['Type'] = '06';
          $creditcard['Number'] = '4716995287640625';
          $creditcard['SecurityCode'] = '864';
          $creditcard['ExpirationDate'] = '12/2013';
          $creditCardAddress['AddressLine'] = '2010 warsaw road';
          $creditCardAddress['City'] = 'Roswell';
          $creditCardAddress['StateProvinceCode'] = 'GA';
          $creditCardAddress['PostalCode'] = '30076';
          $creditCardAddress['CountryCode'] = 'US';*/
          $creditcard['Address'] = $creditCardAddress;
          $billshipper['AccountNumber'] = $set_url['shipper_number'];
          $shipmentcharge['BillShipper'] = $billshipper;
          $paymentinformation['ShipmentCharge'] = $shipmentcharge;
          $shipment['PaymentInformation'] = $paymentinformation;

          $service['Code'] = '08';
          $service['Description'] = 'Expedited';
          $shipment['Service'] = $service;

         /* $internationalForm['FormType'] = '01';
          $internationalForm['InvoiceNumber'] = 'asdf123';
          $internationalForm['InvoiceDate'] = '20151225';
          $internationalForm['PurchaseOrderNumber'] = '999jjj777';
          $internationalForm['TermsOfShipment'] = 'CFR';
          $internationalForm['ReasonForExport'] = 'Sale';
          $internationalForm['Comments'] = 'Your Comments';
          $internationalForm['DeclarationStatement'] = 'Your Declaration Statement';
          $soldTo['Option'] = '01';
          $soldTo['AttentionName'] = 'Sold To Attn Name';
          $soldTo['Name'] = 'Sold To Name';
          $soldToPhone['Number'] = '1234567890';
          $soldToPhone['Extension'] = '1234';
          $soldTo['Phone'] = $soldToPhone;
          $soldToAddress['AddressLine'] = '34 Queen St';
          $soldToAddress['City'] = 'Frankfurt';
          $soldToAddress['PostalCode'] = '60547';
          $soldToAddress['CountryCode'] = 'DE';
          $soldTo['Address'] = $soldToAddress;
          $contact['SoldTo'] = $soldTo;
          $internationalForm['Contacts'] = $contact;
          $product['Description'] = 'Product 1';
          $product['CommodityCode'] = '111222AA';
          $product['OriginCountryCode'] = 'US';
          $unitProduct['Number'] = '147';
          $unitProduct['Value'] = '478';
          $uom['Code'] = 'BOX';
          $uom['Description'] = 'BOX';
          $unitProduct['UnitOfMeasurement'] = $uom;
          $product['Unit'] = $unitProduct;
          $productWeight['Weight'] = '10';
          $uomForWeight['Code'] = 'LBS';
          $uomForWeight['Description'] = 'LBS';
          $productWeight['UnitOfMeasurement'] = $uomForWeight;
          $product['ProductWeight'] = $productWeight;
          $internationalForm['Product'] = $product;
          $discount['MonetaryValue'] = '100';
          $internationalForm['Discount'] = $discount;
          $freight['MonetaryValue'] = '50';
          $internationalForm['FreightCharges'] = $freight;
          $insurance['MonetaryValue'] = '200';
          $internationalForm['InsuranceCharges'] = $insurance;
          $otherCharges['MonetaryValue'] = '50';
          $otherCharges['Description'] = 'Misc';
          $internationalForm['OtherCharges'] = $otherCharges;
          $internationalForm['CurrencyCode'] = 'USD';
          $shpServiceOptions['InternationalForms'] = $internationalForm;
          $shipment['ShipmentServiceOptions'] = $shpServiceOptions;*/

         /*echo 'indfor';
         echo $order->id;
         print_r($order->items);
         exit();*/
          $shipment['Package'] = [];
          foreach($order->items as $product){
            //echo 'infor';
          $package['Description'] = '';
          $packaging['Code'] = '02';
          $packaging['Description'] = 'Nails';
          $package['Packaging'] = $packaging;
          $unit['Code'] = 'IN';
          $unit['Description'] = 'Inches';
          $dimensions['UnitOfMeasurement'] = $unit;
          $dimensions['Length'] = $totweight = $product->length*$product->quantity;
          $dimensions['Width'] = $totquan = $product->width*$product->quantity;
          $dimensions['Height'] = '2';
          $package['Dimensions'] = $dimensions;
          $unit2['Code'] = 'LBS';
          $unit2['Description'] = 'Pounds';
          $packageweight['UnitOfMeasurement'] = $unit2;
          $packageweight['Weight'] = $totweigt = $product->weight*$product->quantity;
          $package['PackageWeight'] = $packageweight;
          $shipment['Package'][] = $package;
          //print_r($package);
        }
        /*print_r($shipment);
        exit();*/
          $labelimageformat['Code'] = 'GIF';
          $labelimageformat['Description'] = 'GIF';
          $labelspecification['LabelImageFormat'] = $labelimageformat;
          $labelspecification['HTTPUserAgent'] = 'Mozilla/4.5';
          $shipment['LabelSpecification'] = $labelspecification;
          $request['Shipment'] = $shipment;

         // echo "Request.......\n";
          //print_r($request);
          /*
          print_r($request);
          echo "\n\n";*/
          return $request;
      }

      function processShipConfirm()
      {

        //create soap request

      }

      function processShipAccept()
      {
        //create soap request
      }



}
