<?php defined('BASEPATH') OR exit('No direct script access allowed');
//DO NOT EDIT THIS FILE

//ClassMap for autoloader
$classes = array (
  'GoCart\\Controller\\AdminFlatRate' => '/var/www/html/targetus/addons/flat_rate/controllers/AdminFlatRate.php',
  'GoCart\\Controller\\FlatRate' => '/var/www/html/targetus/addons/flat_rate/controllers/FlatRate.php',
  'GoCart\\Controller\\AdminPickUpRate' => '/var/www/html/targetus/addons/pick_up_rate/controllers/AdminPickUpRate.php',
  'GoCart\\Controller\\PickUpRate' => '/var/www/html/targetus/addons/pick_up_rate/controllers/PickUpRate.php',		
  'GoCart\\Controller\\CreditCard' => '/var/www/html/targetus/addons/creditcard/controllers/CreditCard.php',
  'GoCart\\Controller\\AdminCreditCard' => '/var/www/html/targetus/addons/creditcard/controllers/AdminCreditCard.php',
  'GoCart\\Controller\\Paypal' => '/var/www/html/targetus/addons/paypal/controllers/Paypal.php',
  'GoCart\\Controller\\AdminPaypal' => '/var/www/html/targetus/addons/paypal/controllers/AdminPaypal.php',
  'GoCart\\Controller\\AdminDashboard' => '/var/www/html/targetus/application/modules/dashboard/controllers/AdminDashboard.php',
  'GoCart\\Controller\\AdminUsers' => '/var/www/html/targetus/application/modules/users/controllers/AdminUsers.php',
  'GoCart\\Controller\\Checkout' => '/var/www/html/targetus/application/modules/checkout/controllers/Checkout.php',
  'GoCart\\Controller\\AdminWysiwyg' => '/var/www/html/targetus/application/modules/wysiwyg/controllers/AdminWysiwyg.php',
  'GoCart\\Controller\\AdminSitemap' => '/var/www/html/targetus/application/modules/sitemap/controllers/AdminSitemap.php',
  'GoCart\\Controller\\MyAccount' => '/var/www/html/targetus/application/modules/my_account/controllers/MyAccount.php',
  'GoCart\\Controller\\AdminPayments' => '/var/www/html/targetus/application/modules/payments/controllers/AdminPayments.php',
  'GoCart\\Controller\\AdminGiftCards' => '/var/www/html/targetus/application/modules/gift_cards/controllers/AdminGiftCards.php',
  'GiftCards' => '/var/www/html/targetus/application/modules/gift_cards/models/Giftcards.php',
  'GoCart\\Controller\\AdminShipping' => '/var/www/html/targetus/application/modules/shipping/controllers/AdminShipping.php',
  'GoCart\\Controller\\AdminSettings' => '/var/www/html/targetus/application/modules/settings/controllers/AdminSettings.php',
  'Messages' => '/var/www/html/targetus/application/modules/settings/models/Messages.php',
  'Settings' => '/var/www/html/targetus/application/modules/settings/models/Settings.php',
  'GoCart\\Controller\\AdminOrders' => '/var/www/html/targetus/application/modules/orders/controllers/AdminOrders.php',
  'Orders' => '/var/www/html/targetus/application/modules/orders/models/Orders.php',
  'GoCart\\Controller\\Cart' => '/var/www/html/targetus/application/modules/cart/controllers/Cart.php',
  'GoCart\\Controller\\AdminCustomers' => '/var/www/html/targetus/application/modules/customers/controllers/AdminCustomers.php',
  'Customers' => '/var/www/html/targetus/application/modules/customers/models/Customers.php',
  'GoCart\\Controller\\Addresses' => '/var/www/html/targetus/application/modules/addresses/controllers/Addresses.php',
  'GoCart\\Controller\\Category' => '/var/www/html/targetus/application/modules/categories/controllers/Category.php',
  'GoCart\\Controller\\AdminCategories' => '/var/www/html/targetus/application/modules/categories/controllers/AdminCategories.php',
  'Categories' => '/var/www/html/targetus/application/modules/categories/models/Categories.php',
  'GoCart\\Controller\\AdminProducts' => '/var/www/html/targetus/application/modules/products/controllers/AdminProducts.php',
  'GoCart\\Controller\\Product' => '/var/www/html/targetus/application/modules/products/controllers/Product.php',
  'Products' => '/var/www/html/targetus/application/modules/products/models/Products.php',
  'ProductOptions' => '/var/www/html/targetus/application/modules/products/models/Productoptions.php',
  'GoCart\\Controller\\AdminReports' => '/var/www/html/targetus/application/modules/reports/controllers/AdminReports.php',
  'GoCart\\Controller\\Page' => '/var/www/html/targetus/application/modules/pages/controllers/Page.php',
  'GoCart\\Controller\\AdminPages' => '/var/www/html/targetus/application/modules/pages/controllers/AdminPages.php',
  'Pages' => '/var/www/html/targetus/application/modules/pages/models/Pages.php',
  'GoCart\\Controller\\AdminLocations' => '/var/www/html/targetus/application/modules/locations/controllers/AdminLocations.php',
  'Locations' => '/var/www/html/targetus/application/modules/locations/models/Locations.php',
  'GoCart\\Controller\\DigitalProducts' => '/var/www/html/targetus/application/modules/digital_products/controllers/DigitalProducts.php',
  'GoCart\\Controller\\AdminDigitalProducts' => '/var/www/html/targetus/application/modules/digital_products/controllers/AdminDigitalProducts.php',
  'DigitalProducts' => '/var/www/html/targetus/application/modules/digital_products/models/Digitalproducts.php',
  'Tax' => '/var/www/html/targetus/application/modules/tax/models/Tax.php',
  'GoCart\\Controller\\Search' => '/var/www/html/targetus/application/modules/search/controllers/Search.php',
  'Search' => '/var/www/html/targetus/application/modules/search/models/Search.php',
  'GoCart\\Controller\\Login' => '/var/www/html/targetus/application/modules/login/controllers/Login.php',
  'GoCart\\Controller\\AdminLogin' => '/var/www/html/targetus/application/modules/login/controllers/AdminLogin.php',
  'Login' => '/var/www/html/targetus/application/modules/login/models/Login.php',
  'GoCart\\Controller\\AdminBanners' => '/var/www/html/targetus/application/modules/banners/controllers/AdminBanners.php',
  'Banners' => '/var/www/html/targetus/application/modules/banners/models/Banners.php',
  'GoCart\\Controller\\AdminCoupons' => '/var/www/html/targetus/application/modules/coupons/controllers/AdminCoupons.php',
  'Coupons' => '/var/www/html/targetus/application/modules/coupons/models/Coupons.php',
  'xmlparser' => '/var/www/html/targetus/application/libraries/xmlparser.php',
  'GoCart\\Emails' => '/var/www/html/targetus/application/libraries/Emails.php',
  'Auth' => '/var/www/html/targetus/application/libraries/Auth.php',
  'MY_Image_lib' => '/var/www/html/targetus/application/libraries/MY_Image_lib.php',
  'Session' => '/var/www/html/targetus/application/libraries/Session.php',
  'MY_Form_validation' => '/var/www/html/targetus/application/libraries/MY_Form_validation.php',
  'Breadcrumbs' => '/var/www/html/targetus/application/libraries/Breadcrumbs.php',
  'GoCart\\Controller\\Admin' => '/var/www/html/targetus/application/core/AdminController.php',
  'GoCart\\Controller' => '/var/www/html/targetus/application/core/Controller.php',
  'CI' => '/var/www/html/targetus/application/core/CI.php',
  'GoCart\\Controller\\Front' => '/var/www/html/targetus/application/core/FrontController.php',
  'GoCart' => '/var/www/html/targetus/application/core/GoCart.php',
  'GoCart\\Libraries\\View' => '/var/www/html/targetus/application/core/View.php',
  'GC' => '/var/www/html/targetus/application/core/GC.php',
);

//Available Payment Modules
$GLOBALS['paymentModules'] =array (
  0 => 
  array (
    'name' => 'Credit Card',
    'key' => 'Credit_Card',
    'class' => 'CreditCard',
  ),1 => 
  array (
    'name' => 'Paypal',
    'key' => 'paypal',
    'class' => 'Paypal',
  ),
);

//Available Shipping Modules
$GLOBALS['shippingModules'] = array (
  0 => 
  array (
    'name' => 'Flat Rate',
    'key' => 'flat-rate',
    'class' => 'FlatRate',
  ),
	1 =>
	array (
			'name' => 'UPS Shipping',
			'key' => 'pick-up-rate',
			'class' => 'PickUpRate',
	),		
);

//Theme Shortcodes
$GLOBALS['themeShortcodes'] = array (
  0 => 
  array (
    'shortcode' => 'category',
    'method' => 
    array (
      0 => 'GoCart\\Controller\\Category',
      1 => 'shortcode',
    ),
  ),
  1 => 
  array (
    'shortcode' => 'banner',
    'method' => 
    array (
      0 => 'Banners',
      1 => 'show_collection',
    ),
  ),
);

//Complete Module List
$GLOBALS['modules'] = array (
  0 => '/var/www/html/targetus/application/modules/addresses',
  1 => '/var/www/html/targetus/application/modules/banners',
  2 => '/var/www/html/targetus/application/modules/cart',
  3 => '/var/www/html/targetus/application/modules/categories',
  4 => '/var/www/html/targetus/application/modules/checkout',
  5 => '/var/www/html/targetus/application/modules/coupons',
  6 => '/var/www/html/targetus/application/modules/customers',
  7 => '/var/www/html/targetus/application/modules/dashboard',
  8 => '/var/www/html/targetus/application/modules/digital_products',
  9 => '/var/www/html/targetus/application/modules/gift_cards',
  10 => '/var/www/html/targetus/application/modules/locations',
  11 => '/var/www/html/targetus/application/modules/login',
  12 => '/var/www/html/targetus/application/modules/my_account',
  13 => '/var/www/html/targetus/application/modules/orders',
  14 => '/var/www/html/targetus/application/modules/pages',
  15 => '/var/www/html/targetus/application/modules/payments',
  16 => '/var/www/html/targetus/application/modules/products',
  17 => '/var/www/html/targetus/application/modules/reports',
  18 => '/var/www/html/targetus/application/modules/search',
  19 => '/var/www/html/targetus/application/modules/settings',
  20 => '/var/www/html/targetus/application/modules/shipping',
  21 => '/var/www/html/targetus/application/modules/sitemap',
  22 => '/var/www/html/targetus/application/modules/tax',
  23 => '/var/www/html/targetus/application/modules/users',
  24 => '/var/www/html/targetus/application/modules/wysiwyg',
  25 => '/var/www/html/targetus/addons/creditcard',
  26 => '/var/www/html/targetus/addons/flat_rate',
  26 => '/var/www/html/targetus/addons/pick_up_rate',
  27 => '/var/www/html/targetus/addons/paypal',
);

//Defined Routes
$routes = array (
  0 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/flat-rate/form',
    2 => 'GoCart\\Controller\\AdminFlatRate#form',
  ),
  1 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/flat-rate/install',
    2 => 'GoCart\\Controller\\AdminFlatRate#install',
  ),
  2 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/flat-rate/uninstall',
    2 => 'GoCart\\Controller\\AdminFlatRate#uninstall',
  ),
  3 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/creditcard/form',
    2 => 'GoCart\\Controller\\AdminCreditCard#form',
  ),
  4 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/creditcard/install',
    2 => 'GoCart\\Controller\\AdminCreditCard#install',
  ),
  5 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/creditcard/uninstall',
    2 => 'GoCart\\Controller\\AdminCreditCard#uninstall',
  ),
  6 => 
  array (
    0 => 'GET|POST',
    1 => '/creditcard/process-payment',
    2 => 'GoCart\\Controller\\CreditCard#processPayment',
  ),
  7 => 
  array (
    0 => 'GET',
    1 => '/admin/dashboard',
    2 => 'GoCart\\Controller\\AdminDashboard#index',
  ),
  8 => 
  array (
    0 => 'GET',
    1 => '/admin',
    2 => 'GoCart\\Controller\\AdminDashboard#index',
  ),
  9 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/users',
    2 => 'GoCart\\Controller\\AdminUsers#index',
  ),
  10 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/users/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminUsers#form',
  ),
  11 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/users/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminUsers#delete',
  ),
  12 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout',
    2 => 'GoCart\\Controller\\Checkout#index',
  ),
  13 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/address-list',
    2 => 'GoCart\\Controller\\Checkout#addressList',
  ),
  14 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/submit-order',
    2 => 'GoCart\\Controller\\Checkout#submitOrder',
  ),
  15 => 
  array (
    0 => 'GET|POST',
    1 => '/order-complete/[:order_id]',
    2 => 'GoCart\\Controller\\Checkout#orderComplete',
  ),
  16 => 
  array (
    0 => 'GET|POST',
    1 => '/order-complete-email/[:order_id]',
    2 => 'GoCart\\Controller\\Checkout#orderCompleteEmail',
  ),
  17 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/address',
    2 => 'GoCart\\Controller\\Checkout#address',
  ),
  18 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/payment-methods',
    2 => 'GoCart\\Controller\\Checkout#paymentMethods',
  ),
  19 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/shipping-methods',
    2 => 'GoCart\\Controller\\Checkout#shippingMethods',
  ),
  20 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/set-shipping-method',
    2 => 'GoCart\\Controller\\Checkout#setShippingMethod',
  ),
  21 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/wysiwyg/upload_image',
    2 => 'GoCart\\Controller\\AdminWysiwyg#upload_image',
  ),
  22 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/wysiwyg/get_images',
    2 => 'GoCart\\Controller\\AdminWysiwyg#get_images',
  ),
  23 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap',
    2 => 'GoCart\\Controller\\AdminSitemap#index',
  ),
  24 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/new-sitemap',
    2 => 'GoCart\\Controller\\AdminSitemap#newSitemap',
  ),
  25 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/generate-products',
    2 => 'GoCart\\Controller\\AdminSitemap#generateProducts',
  ),
  26 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/generate-pages',
    2 => 'GoCart\\Controller\\AdminSitemap#generatePages',
  ),
  27 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/generate-categories',
    2 => 'GoCart\\Controller\\AdminSitemap#generateCategories',
  ),
  28 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/sitemap/complete-sitemap',
    2 => 'GoCart\\Controller\\AdminSitemap#completeSitemap',
  ),
  29 => 
  array (
    0 => 'GET|POST',
    1 => '/my-account',
    2 => 'GoCart\\Controller\\MyAccount#index',
  ),
  30 => 
  array (
    0 => 'GET|POST',
    1 => '/my-account/downloads',
    2 => 'GoCart\\Controller\\MyAccount#downloads',
  ),
  31 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/payments',
    2 => 'GoCart\\Controller\\AdminPayments#index',
  ),
  32 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/gift-cards/form',
    2 => 'GoCart\\Controller\\AdminGiftCards#form',
  ),
  33 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/gift-cards/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminGiftCards#delete',
  ),
  34 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/gift-cards/enable',
    2 => 'GoCart\\Controller\\AdminGiftCards#enable',
  ),
  35 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/gift-cards/disable',
    2 => 'GoCart\\Controller\\AdminGiftCards#disable',
  ),
  36 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/gift-cards/settings',
    2 => 'GoCart\\Controller\\AdminGiftCards#settings',
  ),
  37 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/gift-cards',
    2 => 'GoCart\\Controller\\AdminGiftCards#index',
  ),
  38 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/shipping',
    2 => 'GoCart\\Controller\\AdminShipping#index',
  ),
  39 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings',
    2 => 'GoCart\\Controller\\AdminSettings#index',
  ),
  40 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings/canned_messages',
    2 => 'GoCart\\Controller\\AdminSettings#canned_messages',
  ),
  41 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings/canned_message_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminSettings#canned_message_form',
  ),
  42 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/settings/delete_message/[i:id]',
    2 => 'GoCart\\Controller\\AdminSettings#delete_message',
  ),
  43 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminOrders#form',
  ),
  44 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/export',
    2 => 'GoCart\\Controller\\AdminOrders#export',
  ),
  45 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/bulk_delete',
    2 => 'GoCart\\Controller\\AdminOrders#bulk_delete',
  ),
  46 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/order/[:orderNumber]',
    2 => 'GoCart\\Controller\\AdminOrders#order',
  ),
  47 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/sendNotification/[:orderNumber]',
    2 => 'GoCart\\Controller\\AdminOrders#sendNotification',
  ),
  48 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/packing_slip/[:orderNumber]',
    2 => 'GoCart\\Controller\\AdminOrders#packing_slip',
  ),
  49 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/edit_status',
    2 => 'GoCart\\Controller\\AdminOrders#edit_status',
  ),
  50 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminOrders#delete',
  ),
  51 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders',
    2 => 'GoCart\\Controller\\AdminOrders#index',
  ),
  52 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/orders/index/[:orderBy]?/[:orderDir]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminOrders#index',
  ),
  53 => 
  array (
    0 => 'GET|POST',
    1 => '/digital-products/download/[i:fileId]/[i:orderId]',
    2 => 'GoCart\\Controller\\DigitalProducts#download',
  ),
  54 => 
  array (
    0 => 'GET|POST',
    1 => '/cart/summary/[:hide]?',
    2 => 'GoCart\\Controller\\Cart#summary',
  ),
  55 => 
  array (
    0 => 'GET|POST',
    1 => '/cart/add-to-cart',
    2 => 'GoCart\\Controller\\Cart#addToCart',
  ),
  56 => 
  array (
    0 => 'GET|POST',
    1 => '/cart/update-cart',
    2 => 'GoCart\\Controller\\Cart#updateCart',
  ),
  57 => 
  array (
    0 => 'GET|POST',
    1 => '/cart/submit-coupon',
    2 => 'GoCart\\Controller\\Cart#submitCoupon',
  ),
  58 => 
  array (
    0 => 'GET|POST',
    1 => '/cart/submit-gift-card',
    2 => 'GoCart\\Controller\\Cart#submitGiftCard',
  ),
  59 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/export',
    2 => 'GoCart\\Controller\\AdminCustomers#export',
  ),
  60 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/get_subscriber_list',
    2 => 'GoCart\\Controller\\AdminCustomers#getSubscriberList',
  ),
  61 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#form',
  ),
  62 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/addresses/[i:id]',
    2 => 'GoCart\\Controller\\AdminCustomers#addresses',
  ),
  63 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/delete/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#delete',
  ),
  64 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/groups',
    2 => 'GoCart\\Controller\\AdminCustomers#groups',
  ),
  65 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/group_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#groupForm',
  ),
  66 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/delete_group/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#deleteGroup',
  ),
  67 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/address_list/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#addressList',
  ),
  68 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/address_form/[i:customer_id]/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCustomers#addressForm',
  ),
  69 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/delete_address/[i:customer_id]/[i:id]',
    2 => 'GoCart\\Controller\\AdminCustomers#deleteAddress',
  ),
  70 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/customers/[:order_by]?/[:direction]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminCustomers#index',
  ),
  71 => 
  array (
    0 => 'GET|POST',
    1 => '/addresses',
    2 => 'GoCart\\Controller\\Addresses#index',
  ),
  72 => 
  array (
    0 => 'GET|POST',
    1 => '/addresses/json',
    2 => 'GoCart\\Controller\\Addresses#addressJSON',
  ),
  73 => 
  array (
    0 => 'GET|POST',
    1 => '/addresses/form/[i:id]?',
    2 => 'GoCart\\Controller\\Addresses#form',
  ),
  74 => 
  array (
    0 => 'GET|POST',
    1 => '/addresses/delete/[i:id]',
    2 => 'GoCart\\Controller\\Addresses#delete',
  ),
  75 => 
  array (
    0 => 'GET|POST',
    1 => '/addresses/get-zone-options/[i:id]',
    2 => 'GoCart\\Controller\\Addresses#getZoneOptions',
  ),
  76 => 
  array (
    0 => 'GET',
    1 => '/admin/categories',
    2 => 'GoCart\\Controller\\AdminCategories#index',
  ),
  77 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/categories/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCategories#form',
  ),
  78 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/categories/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminCategories#delete',
  ),
  79 => 
  array (
    0 => 'GET|POST',
    1 => '/category/[:slug]/[:sort]?/[:dir]?/[:page]?',
    2 => 'GoCart\\Controller\\Category#index',
  ),
  80 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/product_autocomplete',
    2 => 'GoCart\\Controller\\AdminProducts#product_autocomplete',
  ),
  81 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/bulk_save',
    2 => 'GoCart\\Controller\\AdminProducts#bulk_save',
  ),
  82 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/product_image_form',
    2 => 'GoCart\\Controller\\AdminProducts#product_image_form',
  ),
  83 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/product_image_upload',
    2 => 'GoCart\\Controller\\AdminProducts#product_image_upload',
  ),
  84 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/form/[i:id]?/[i:copy]?',
    2 => 'GoCart\\Controller\\AdminProducts#form',
  ),
  85 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/gift-card-form/[i:id]?/[i:copy]?',
    2 => 'GoCart\\Controller\\AdminProducts#giftCardForm',
  ),
  86 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminProducts#delete',
  ),
  87 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/products/[i:rows]?/[:order_by]?/[:sort_order]?/[:code]?/[i:page]?',
    2 => 'GoCart\\Controller\\AdminProducts#index',
  ),
  88 => 
  array (
    0 => 'GET|POST',
    1 => '/product/[:slug]',
    2 => 'GoCart\\Controller\\Product#index',
  ),
  89 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/reports',
    2 => 'GoCart\\Controller\\AdminReports#index',
  ),
  90 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/reports/best_sellers',
    2 => 'GoCart\\Controller\\AdminReports#best_sellers',
  ),
  91 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/reports/sales',
    2 => 'GoCart\\Controller\\AdminReports#sales',
  ),
  92 => 
  array (
    0 => 'GET',
    1 => '/admin/pages',
    2 => 'GoCart\\Controller\\AdminPages#index',
  ),
  93 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/pages/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminPages#form',
  ),
  94 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/pages/link_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminPages#link_form',
  ),
  95 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/pages/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminPages#delete',
  ),
  96 => 
  array (
    0 => 'GET|POST',
    1 => '/page/[:slug]',
    2 => 'GoCart\\Controller\\Page#index',
  ),
  97 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/zone_areas/[i:id]',
    2 => 'GoCart\\Controller\\AdminLocations#zone_areas',
  ),
  98 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/delete_zone_area/[i:id]',
    2 => 'GoCart\\Controller\\AdminLocations#delete_zone_area',
  ),
  99 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/zone_area_form/[i:zone_id]/[i:id]?',
    2 => 'GoCart\\Controller\\AdminLocations#zone_area_form',
  ),
  100 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/zones/[i:id]',
    2 => 'GoCart\\Controller\\AdminLocations#zones',
  ),
  101 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/zone_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminLocations#zone_form',
  ),
  102 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/delete_zone/[i:id]',
    2 => 'GoCart\\Controller\\AdminLocations#delete_zone',
  ),
  103 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/get_zone_menu',
    2 => 'GoCart\\Controller\\AdminLocations#get_zone_menu',
  ),
  104 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/country_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminLocations#country_form',
  ),
  105 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/delete_country/[i:id]',
    2 => 'GoCart\\Controller\\AdminLocations#delete_country',
  ),
  106 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations/organize_countries',
    2 => 'GoCart\\Controller\\AdminLocations#organize_countries',
  ),
  107 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/locations',
    2 => 'GoCart\\Controller\\AdminLocations#index',
  ),
  108 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/digital_products/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminDigitalProducts#form',
  ),
  109 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/digital_products/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminDigitalProducts#delete',
  ),
  110 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/digital_products',
    2 => 'GoCart\\Controller\\AdminDigitalProducts#index',
  ),
  111 => 
  array (
    0 => 'GET|POST',
    1 => '/search/[:code]?/[:sort]?/[:dir]?/[:page]?',
    2 => 'GoCart\\Controller\\Search#index',
  ),
  112 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/login',
    2 => 'GoCart\\Controller\\AdminLogin#login',
  ),
  113 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/logout',
    2 => 'GoCart\\Controller\\AdminLogin#logout',
  ),
  114 => 
  array (
    0 => 'GET|POST',
    1 => '/login/[:redirect]?',
    2 => 'GoCart\\Controller\\Login#login',
  ),
  115 => 
  array (
    0 => 'GET|POST',
    1 => '/logout',
    2 => 'GoCart\\Controller\\Login#logout',
  ),
  116 => 
  array (
    0 => 'GET|POST',
    1 => '/forgot-password',
    2 => 'GoCart\\Controller\\Login#forgotPassword',
  ),
  117 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/forgot-password',
    2 => 'GoCart\\Controller\\AdminLogin#forgotPassword',
  ),
  118 => 
  array (
    0 => 'GET|POST',
    1 => '/register',
    2 => 'GoCart\\Controller\\Login#register',
  ),
  119 => 
  array (
    0 => 'GET',
    1 => '/admin/banners',
    2 => 'GoCart\\Controller\\AdminBanners#index',
  ),
  120 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/banners/banner_collection_form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminBanners#banner_collection_form',
  ),
  121 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/banners/delete_banner_collection/[i:id]',
    2 => 'GoCart\\Controller\\AdminBanners#delete_banner_collection',
  ),
  122 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/banners/banner_collection/[i:id]',
    2 => 'GoCart\\Controller\\AdminBanners#banner_collection',
  ),
  123 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/banners/banner_form/[i:banner_collection_id]/[i:id]?',
    2 => 'GoCart\\Controller\\AdminBanners#banner_form',
  ),
  124 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/banners/delete_banner/[i:id]',
    2 => 'GoCart\\Controller\\AdminBanners#delete_banner',
  ),
  125 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/banners/organize',
    2 => 'GoCart\\Controller\\AdminBanners#organize',
  ),
  126 => 
  array (
    0 => 'GET',
    1 => '/admin/coupons',
    2 => 'GoCart\\Controller\\AdminCoupons#index',
  ),
  127 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/coupons/form/[i:id]?',
    2 => 'GoCart\\Controller\\AdminCoupons#form',
  ),
  128 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/coupons/delete/[i:id]',
    2 => 'GoCart\\Controller\\AdminCoupons#delete',
  ),129 => 
  array (
    0 => 'GET|POST',
    1 => '/categoriescontroller/getcategories',
    2 => 'GoCart\\Controller\\Category#getcategories',
  ), 130 => 
  array (
    0 => 'GET|POST',
    1 => '/categoriescontroller/getCategoriesProduct',
    2 => 'GoCart\\Controller\\Category#getCategoriesProduct',
  ),  131 => 
  array (
    0 => 'GET|POST',
    1 => '/cart/get-cart-info',
    2 => 'GoCart\\Controller\\Cart#getCartInfo',
  ), 132 => 
  array (
    0 => 'GET|POST',
    1 => '/userpayments/paypalreturn',
    2 => 'GoCart\\Controller\\UserPayments#paypalreturn',
  ), 133 =>
  array (
    0 => 'GET|POST',
    1 => '/product/list/',
    2 => 'GoCart\\Controller\\Product#product_list',
    ), 134 =>
  array (
    0 => 'GET|POST',
    1 => '/checkout/checkout-wizard',
    2 => 'GoCart\\Controller\\Checkout#checkoutWizard',
  )	,  135 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/pick-up-rate/form',
    2 => 'GoCart\\Controller\\AdminPickUpRate#form',
  ),
  136 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/pick-up-rate/install',
    2 => 'GoCart\\Controller\\AdminPickUpRate#install',
  ),
  137 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/pick-up-rate/uninstall',
    2 => 'GoCart\\Controller\\AdminPickUpRate#uninstall',
  ),
		138 =>
		array (
				0 => 'GET|POST',
				1 => '/admin/paypal/form',
				2 => 'GoCart\\Controller\\AdminPaypal#form',
		),
		139 =>
		array (
				0 => 'GET|POST',
				1 => '/admin/paypal/install',
				2 => 'GoCart\\Controller\\AdminPaypal#install',
		),
		140 =>
		array (
				0 => 'GET|POST',
				1 => '/admin/paypal/uninstall',
				2 => 'GoCart\\Controller\\AdminPaypal#uninstall',
		),
  141 => 
  array (
    0 => 'GET|POST',
    1 => '/paypal/process-payment',
    2 => 'GoCart\\Controller\\Paypal#processPayment',
  ),
  142 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/ship-payment-method',
    2 => 'GoCart\\Controller\\Checkout#saveShipPaymentMethod',
  ),
  143 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/order-overview',
    2 => 'GoCart\\Controller\\Checkout#showOrderOverview',
  ),
    144 => 
  array (
    0 => 'GET|POST',
    1 => '/ssearch/searchproduct/[:code]?/[:sort]?/[:dir]?/[:page]?',
    2 => 'GoCart\\Controller\\Search#searchproduct',
  ),
  145 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/orderpayment',
    2 => 'GoCart\\Controller\\Checkout#orderPayment',
    ),146 => 
  array (
    0 => 'GET|POST',
    1 => '/checkout/afterpaymentaddr',
    2 => 'GoCart\\Controller\\Checkout#afterpaymentaddr',
  ),147 => 
  array (
    0 => 'GET|POST',
    1 => '/search/searchprdtbyprice/',
    2 => 'GoCart\\Controller\\Search#searchproductby_price',
  ),148 => 
  array (
    0 => 'GET|POST',
    1 => '/search/searchallval/[:code]?/',
    2 => 'GoCart\\Controller\\Search#searchallvaluseajax',
  ),149 => 
  array (
    0 => 'GET|POST',
    1 => '/pagecontroller/pagealbum',
    2 => 'GoCart\\Controller\\Page#pagealbum',
  ),150 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/avail_location/[:code]?',
    2 => 'GoCart\\Controller\\AdminCategories#avail_location',
  ),151 => 
  array (
    0 => 'GET',
    1 => '/category/location/',
    2 => 'GoCart\\Controller\\Category#availabelocation',
  ),152 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/list_location',
    2 => 'GoCart\\Controller\\AdminCategories#list_location',
  ),153 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/delete_location/[:code]?/[:status]?',
    2 => 'GoCart\\Controller\\AdminCategories#deletelocation',
  ),154 => 
  array (
    0 => 'GET|POST',
    1 => '/page/event_news/',
    2 => 'GoCart\\Controller\\Page#event_news',
  ),
  155 => 
  array (
    0 => 'GET|POST',
    1 => '/addresses/getaddress/[i:id]?',
    2 => 'GoCart\\Controller\\Addresses#getaddress',
  ),156 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/productreports',
    2 => 'GoCart\\Controller\\AdminCategories#product_report',
  ),157 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/searchallproduct',
    2 => 'GoCart\\Controller\\AdminCategories#productrportlist',
 ),158 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/add_slider/[:code]?',
    2 => 'GoCart\\Controller\\AdminCategories#add_slider',
  ),159 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/view_slider/[:code]?',
    2 => 'GoCart\\Controller\\AdminCategories#slider_view',
  ),160 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/delete_slider/[:code]?',
    2 => 'GoCart\\Controller\\AdminCategories#slider_delete',
   ),161 => 
  array (
    0 => 'GET|POST',
    1 => '/admin/Credit_Card/form',
    2 => 'GoCart\\Controller\\AdminPayments#monerisconfig',
  )
);