<?php namespace GoCart\Controller;
/**
 * CreditCard Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    CreditCard
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class CreditCard extends Front {

    public function __construct()
    {
        parent::__construct();
        \CI::lang()->load('creditcard');
    }

    //back end installation functions
    public function checkoutForm()
    {
        //set a default blank setting for flatrate shipping
        $this->partial('creditcardCheckoutForm');    
    }

    public function isEnabled()
    {
        $settings = \CI::Settings()->get_settings('Credit_Card');
        return (isset($settings['enabled']) && (bool)$settings['enabled'])?true:false;
    }

    function processPayment()
    {
        $errors = \GC::checkOrder();
        if(count($errors) > 0)
        {
            echo json_encode(['errors'=>$errors]);
            return false;
        }
        else
        {
            $payment = [
                'order_id' => \GC::getAttribute('id'),
                'amount' => \GC::getGrandTotal(),
                'status' => 'processed',
                'payment_module' => 'CreditCard',
                'description' => lang('charge_on_delivery')
            ];

            \CI::Orders()->savePaymentInfo($payment);

            $orderId = \GC::submitOrder();

            //send the order ID
            echo json_encode(['orderId'=>$orderId]);
            return false;
        }
    }

    public function getName()
    {
        echo lang('credit_card');
    }
}
