<?php

$routes[] = ['GET|POST', '/admin/creditcard/form', 'GoCart\Controller\AdminCreditCard#form'];
$routes[] = ['GET|POST', '/admin/creditcard/install', 'GoCart\Controller\AdminCreditCard#install'];
$routes[] = ['GET|POST', '/admin/creditcard/uninstall', 'GoCart\Controller\AdminCreditCard#uninstall'];
$routes[] = ['GET|POST', '/creditcard/process-payment', 'GoCart\Controller\CreditCard#processPayment'];

$paymentModules[] = ['name'=>'Credit Card', 'key'=>'creditcard', 'class'=>'CreditCard'];