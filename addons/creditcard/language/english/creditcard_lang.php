<?php

$lang['credit_card'] = 'Credit Card';
$lang['enabled'] = 'Enabled';
$lang['disabled'] = 'Disabled';
$lang['processing_error'] = 'There was an error processing your payment';