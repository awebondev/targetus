<div class="page-header">
    <?php echo lang('credit_card');?>
</div>

<button class="blue" id="btn_creditcard" onclick="CreditCardSubmitOrder()"><?php echo lang('submit_order');?></button>

<script>
function CreditCardSubmitOrder()
{
    $('#btn_creditcard').attr('disabled', true).addClass('disabled');

    $.post('<?php echo base_url('/creditcard/process-payment');?>', function(data){
        if(data.errors != undefined)
        {
            var error = '<div class="alert red">';
            $.each(data.errors, function(index, value)
            {
                error += '<p>'+value+'</p>';
            });
            error += '</div>';

            $.gumboTray(error);
            $('#btn_creditcard').attr('disabled', false).addClass('disabled');
        }
        else
        {
            if(data.orderId != undefined)
            {
                window.location = '<?php echo site_url('order-complete/');?>/'+data.orderId;
            }
        }
    }, 'json');

}
</script>
