<div class="page-header">
    <?php echo lang('paypal');?>
</div>

<button class="blue" id="btn_paypal" onclick="PaypalSubmitOrder()"><?php echo lang('submit_order');?></button>

<script>
function PaypalSubmitOrder()
{
    $('#btn_paypal').attr('disabled', true).addClass('disabled');

    $.post('<?php echo base_url('/paypal/process-payment');?>', function(data){
        if(data.errors != undefined)
        {
            var error = '<div class="alert red">';
            $.each(data.errors, function(index, value)
            {
                error += '<p>'+value+'</p>';
            });
            error += '</div>';

            $.gumboTray(error);
            $('#btn_paypal').attr('disabled', false).addClass('disabled');
        }
        else
        {
            if(data.orderId != undefined)
            {
                window.location = '<?php echo site_url('order-complete/');?>/'+data.orderId;
            }
        }
    }, 'json');

}
</script>
