<?php namespace GoCart\Controller;
/**
 * Paypal Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    Paypal
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class Paypal extends Front {

    public function __construct()
    {
        parent::__construct();
        \CI::lang()->load('paypal');
    }

    //back end installation functions
    public function checkoutForm()
    {
        //set a default blank setting for flatrate shipping
        $this->partial('paypalCheckoutForm');    
    }

    public function isEnabled()
    {
        $settings = \CI::Settings()->get_settings('paypal');
        return (isset($settings['enabled']) && (bool)$settings['enabled'])?true:false;
    }

    function processPayment()
    {
        $errors = \GC::checkOrder();
        if(count($errors) > 0)
        {
            echo json_encode(['errors'=>$errors]);
            return false;
        }
        else
        {
            $payment = [
                'order_id' => \GC::getAttribute('id'),
                'amount' => \GC::getGrandTotal(),
                'status' => 'processed',
                'payment_module' => 'Paypal',
                'description' => lang('charge_on_delivery')
            ];

            \CI::Orders()->savePaymentInfo($payment);

            $orderId = \GC::submitOrder();
			redirect('order-complete/'.$orderId);
            //send the order ID
            //echo json_encode(['orderId'=>$orderId]);
            //return false;
        }
    }

    public function getName()
    {
        echo lang('paypal');
    }
}
