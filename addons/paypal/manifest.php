<?php

$routes[] = ['GET|POST', '/admin/paypal/form', 'GoCart\Controller\AdminPaypal#form'];
$routes[] = ['GET|POST', '/admin/paypal/install', 'GoCart\Controller\AdminPaypal#install'];
$routes[] = ['GET|POST', '/admin/paypal/uninstall', 'GoCart\Controller\AdminPaypal#uninstall'];
$routes[] = ['GET|POST', '/paypal/process-payment', 'GoCart\Controller\Paypal#processPayment'];

$paymentModules[] = ['name'=>'Credit Card', 'key'=>'paypal', 'class'=>'Paypal'];