<?php namespace GoCart\Controller;
/**
 * FlatRate Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    FlatRate
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class FlatRate extends Front { 

    public function __construct()
    {
        parent::__construct();
        \CI::load()->model(array('Locations'));
        $this->customer = \CI::Login()->customer();
    }

    public function rates()
    {
        $settings = \CI::Settings()->get_settings('UpsShipping');
        /*echo $ee = \CI::input()->post('address1');
        exit();
*/
        $sj = \CI::session()->userdata('myaddr');
        /*echo $sj.'ff';
        exit();*/
        if($sj == '1b'){
          $set_url = \CI::Settings()->get_settings('PickUpRate');         
          $access = $set_url['access'];
          $userid = $set_url['userid'];
          $passwd = $set_url['passwd'];
          
          $wsdl = base_url()."assets/shipaddress/RateWS.wsdl";
          $operation = "ProcessRate";
          if($set_url['liveurl']==1){
            $endpointurl = 'https://onlinetools.ups.com/webservices/Rate';
          }else{
            $endpointurl = 'https://wwwcie.ups.com/webservices/Rate';
          //$outputFileName = 'D:\xampp\htdocs\targetus\assets\shipaddress\XOLTResult.xml';
          }          
          try
          {


            $mode = array
            (
                 'soap_version' => 'SOAP_1_1',  // use soap 1.1 client
                 'trace' => 1
            );
            /*echo "dfdf";
        exit();*/
            // initialize soap client
            $client = new \SoapClient($wsdl , $mode);
            /*echo "dfdf";
        exit();*/
            //set endpoint url
            $client->__setLocation($endpointurl);

            /*echo "dfdf";
        exit();*/
            //create soap header
            $usernameToken['Username'] = $userid;
            $usernameToken['Password'] = $passwd;
            $serviceAccessLicense['AccessLicenseNumber'] = $access;
            $upss['UsernameToken'] = $usernameToken;
            $upss['ServiceAccessToken'] = $serviceAccessLicense;

            $header = new \SoapHeader('http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0','UPSSecurity',$upss);
            $client->__setSoapHeaders($header);


/*
echo "dfdf";
        exit();*/
            //echo 'sj';
            //get response
            $resp = $client->__soapCall($operation ,array(\CI::Customers()->processRate()));

                       /*if (is_soap_fault($resp)) {
    trigger_error("SOAP Fault: (faultcode: {$result->faultcode}, faultstring: {$result->faultstring})", E_USER_ERROR);
} 
            print_r($resp);
            exit();
            echo 'fdfd';*/
            if(!$resp->Response->ResponseStatus->Description){
                    redirect('checkout/checkout-wizard');
                }
            //print_r($resp->RatedShipment);
            //print_r($resp);
            //echo 'sj1';
            //exit();
            //foreach ($resp as $resps) {
                //print_r($resp->RatedShipment[0]->TotalCharges->MonetaryValue);
            //}
                $shipfinal_value = $resp->RatedShipment[0]->TotalCharges->MonetaryValue;
                //echo '1';
                //exit();
            
            //echo 'sj2';
            //exit();
            //print_r($resp['ratedshipment']);
            //echo 'echo'.$resp->ratedshipment;

             /*$rt = array(
                'retval' => 1,
                'shipvaladdr' => $shipfinal_value
                );
            
             echo json_encode($rt);*/
            //get status
                //echo "Response Status: " . $resp->Response->ResponseStatus->Description ."\n";
                

            //save soap request and response to file
            /*$fw = fopen($outputFileName , 'w');
            fwrite($fw , "Request: \n" . $client->__getLastRequest() . "\n");
            fwrite($fw , "Response: \n" . $client->__getLastResponse() . "\n");
            fclose($fw);*/

          }
          catch(\SoapFault $e)
          {
            $addrer = $e->detail->Errors->ErrorDetail->PrimaryErrorCode->Description;
            $sj = \CI::session()->set_userdata('myaddr','1b'); 
            $addrerror = \CI::session()->set_userdata('addr_error',$addrer); 
            return ['UPS Shipping'=> $sj];
          }
      }



        if(isset($settings['enabled']) && (bool)$settings['enabled'])
        {
            if($sj == '1b'){
                \CI::session()->set_userdata('myaddr',$shipfinal_value); 
            return ['UPS Shipping'=> $shipfinal_value];
            }else{
                return ['UPS Shipping'=> $sj];
            }
        }
        else
        {
            return [];
        }
    }
}