<?php namespace GoCart\Controller;
/**
 * AdminPickUpRate Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    AdminPickUpRate
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class AdminPickUpRate extends Admin { 

    public function __construct()
    {       
        parent::__construct();
        \CI::auth()->check_access('Admin', true);
        \CI::lang()->load('pick_up_rate');
    }

    //back end installation functions
    public function install()
    {
        //set a default blank setting for PickUpRate shipping
        \CI::Settings()->save_settings('shipping_modules', ['PickUpRate'=>'1']);
        \CI::Settings()->save_settings('PickUpRate', ['enabled'=>'1', 'rate'=>0]);

        redirect('admin/shipping');
    }

    public function uninstall()
    {
        \CI::Settings()->delete_setting('shipping_modules', 'PickUpRate');
        \CI::Settings()->delete_settings('PickUpRate');
        redirect('admin/shipping');
    }
    
    //admin end form and check functions
    public function form()
    {
        //this same function processes the form

        \CI::load()->helper('form');
        \CI::load()->library('form_validation');

        \CI::form_validation()->set_rules('sname', 'lang:name', 'required');
        \CI::form_validation()->set_rules('shipper_number', 'lang:shipper_number', 'required');
        \CI::form_validation()->set_rules('city', 'lang:city', 'required');
        \CI::form_validation()->set_rules('country', 'lang:country', 'required');
        \CI::form_validation()->set_rules('zipcode', 'lang:zipcode', 'required');
        \CI::form_validation()->set_rules('zone', 'lang:zone', 'required');
        \CI::form_validation()->set_rules('phnumber', 'lang:phone', 'required');

        \CI::form_validation()->set_rules('address', 'lang:address', 'required');
        \CI::form_validation()->set_rules('access', 'lang:access', 'required');
        \CI::form_validation()->set_rules('userid', 'lang:userid', 'required');
        \CI::form_validation()->set_rules('passwd', 'lang:passwd', 'required');

        if (\CI::form_validation()->run() == FALSE)
        {
            $settings = \CI::Settings()->get_settings('PickUpRate');

            $this->view('pick_up_rate_form', $settings);
        }
        else
        {
            
            \CI::Settings()->save_settings('PickUpRate', \CI::input()->post());
            redirect('admin/shipping');
        }
    }
}