<?php namespace GoCart\Controller;
/**
 * PickUpRate Class
 *
 * @package     GoCart
 * @subpackage  Controllers
 * @category    PickUpRate
 * @author      Clear Sky Designs
 * @link        http://gocartdv.com
 */

class PickUpRate extends Front { 

    public function __construct()
    {
        parent::__construct();
        \CI::load()->model(array('Locations'));
        $this->customer = \CI::Login()->customer();
    }

    public function rates()
    {
        $settings = \CI::Settings()->get_settings('PickUpRate');
        
        if(isset($settings['enabled']) && (bool)$settings['enabled'])
        {
            return ['Pick Up Rate'=> $settings['rate']];
        }
        else
        {
            return [];
        }
    }
}