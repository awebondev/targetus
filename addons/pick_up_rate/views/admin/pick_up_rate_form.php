<div class="page-header">
    <h2><?php echo lang('pick_up_rate');?></h2>
</div>

<div class="row">
<?php echo form_open_multipart('admin/pick-up-rate/form'); ?>
<div class="col-md-6">
<!-- <div class="form-group">
    <label for="enabled"><?php echo lang('enabled');?> </label>
    <?php echo form_dropdown('enabled', array('0' => lang('disabled'), '1' => lang('enabled')), assign_value('enabled',$enabled), 'class="form-control"'); ?>
</div>
<div class="form-group">
    <label for="enabled"><?php echo lang('rate');?> </label>
    <?php echo form_input(['name'=>'rate', 'value'=>assign_value('rate', $rate), 'class' => 'form-control' ]); ?>
</div> -->
<h3>Shipping Details:</h3>

<div class="form-group">
    <label for="enabled"><?php echo lang('access');?> </label>
    <?php echo form_input(['name'=>'access', 'value'=>assign_value('access', $access), 'class' => 'form-control' ]); ?>
</div>
<div class="form-group">
    <label for="enabled"><?php echo lang('userid');?> </label>
    <?php echo form_input(['name'=>'userid', 'value'=>assign_value('userid', $userid), 'class' => 'form-control' ]); ?>
</div> 
<div class="form-group">
    <label for="enabled"><?php echo lang('passwd');?> </label>
    <?php echo form_password(['name'=>'passwd', 'value'=>assign_value('passwd', $passwd), 'class' => 'form-control' ]); ?>
</div>
<input type="hidden" name="liveurl" id="liveurl" value="0">
<div class="form-group">   
    <?php echo form_checkbox(['name'=>'liveurll', 'id'=>'liveurl', 'value'=>assign_value('liveurl', $liveurl), 'checked'=> ($liveurl == 1)? 'checked':'', 'class' => '' ]); ?><label for="enabled"><?php echo lang('testurl');?> </label>
</div>
<br>
<h2>Address</h2>
<br>
<div class="form-group">
    <label for="enabled"><?php echo lang('name');?> </label>
    <?php echo form_input(['name'=>'sname', 'value'=>assign_value('sname', $sname), 'class' => 'form-control' ]); ?>
</div>
<div class="form-group">
    <label for="enabled"><?php echo lang('shipper_number');?> </label>
    <?php echo form_input(['name'=>'shipper_number', 'value'=>assign_value('shipper_number', $shipper_number), 'class' => 'form-control' ]); ?>
</div>
<div class="form-group">
    <label for="enabled"><?php echo lang('address');?> </label>
    <?php echo form_input(['name'=>'address', 'value'=>assign_value('address', $address), 'class' => 'form-control' ]); ?>
</div>
<div class="form-group">
    <label for="enabled"><?php echo lang('city');?> </label>
    <?php echo form_input(['name'=>'city', 'value'=>assign_value('city', $city), 'class' => 'form-control' ]); ?>
</div>
<div class="form-group">
    <label for="enabled"><?php echo lang('zipcode');?> </label>
    <?php echo form_input(['name'=>'zipcode', 'value'=>assign_value('zipcode', $zipcode), 'class' => 'form-control' ]); ?>
</div>
<?php $zones_menu = \CI::Locations()->get_zones_menu($country); ?>
<div class="form-group">
    <label><?php echo lang('state');?></label>
    <?php echo form_dropdown('zone', $zones_menu, assign_value('zone', $zone), $ar = array('id'=>'zone','class'=>'form-control'));?>
</div>
<?php $countries_menu = \CI::Locations()->get_countries_menu(); ?>
<div class="form-group">
    <label><?php echo lang('country');?></label>
    <?php echo form_dropdown('country',  $countries_menu, assign_value('country', $country), $ar = array('id'=>'country','class'=>'form-control'));?>
</div>
<div class="form-group">
    <label><?php echo lang('phone');?></label>
    <?php echo form_input(['name'=>'phnumber', 'value'=>assign_value('phnumber', $phnumber), 'class' => 'form-control' ]); ?>
</div>
    <div class="form-actions">
        <button type="submit" class="btn btn-primary"><?php echo lang('save');?></button>
    </div>
</div>   
</form>
</div> 
<script type="text/javascript">
$('#country').change(function(){
            $('#zone').load('<?php echo site_url('addresses/get-zone-options');?>/'+$('#country').val());
        });
$('form').submit(function() {
    $('.btn .btn-primary').attr('disabled', true).addClass('disabled');
});
$(document).ready(function(){
        $('input[type="checkbox"]').click(function(){
            if($(this).prop("checked") == true){
                $("#liveurl").val(1);
            }
            else if($(this).prop("checked") == false){
                $("#liveurl").val(0);
            }
        });
    });
</script>