<?php

$routes[] = ['GET|POST', '/admin/pick-up-rate/form', 'GoCart\Controller\AdminPickUpRate#form'];
$routes[] = ['GET|POST', '/admin/pick-up-rate/install', 'GoCart\Controller\AdminPickUpRate#install'];
$routes[] = ['GET|POST', '/admin/pick-up-rate/uninstall', 'GoCart\Controller\AdminPickUpRate#uninstall'];

$shippingModules[] = ['name'=>'Pick Up Rate', 'key'=>'pick-up-rate', 'class'=>'PickUpRate'];