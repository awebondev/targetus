<?php

$lang['pick_up_rate'] = 'Ups Shipping';
$lang['enabled'] = 'Enabled';
$lang['disabled'] = 'Disabled';
$lang['rate'] = 'Rate';
$lang['access'] = 'Access name';
$lang['userid'] = 'User id';
$lang['passwd'] = 'Password';
$lang['testurl'] = 'Use live url';
$lang['address'] = 'address';
$lang['city'] = 'city';
$lang['country'] = 'country';	
$lang['zipcode'] = 'zipcode';
$lang['state'] = 'state';
$lang['name'] = 'Name';
$lang['shipper_number'] = 'Shipper Number';
$lang['phone'] = 'phone';
