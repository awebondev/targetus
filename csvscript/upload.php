<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Upload page</title>
<style type="text/css">
body {
	background: #E3F4FC;
	font: normal 14px/30px Helvetica, Arial, sans-serif;
	color: #2b2b2b;
}
a {
	color:#898989;
	font-size:14px;
	font-weight:bold;
	text-decoration:none;
}
a:hover {
	color:#CC0033;
}

h1 {
	font: bold 14px Helvetica, Arial, sans-serif;
	color: #CC0033;
}
h2 {
	font: bold 14px Helvetica, Arial, sans-serif;
	color: #898989;
}
#container {
	background: #CCC;
	margin: 100px auto;
	width: 945px;
}
#form 			{padding: 20px 150px;}
#form input     {margin-bottom: 20px;}
</style>
</head>
<body>
<div id="container">
<div id="form">

<?php

//include "connection.php"; //Connect to Database
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "gocart";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$deleterecords = "TRUNCATE TABLE tablename"; //empty the table of its current records
//mysql_query($deleterecords);

//Upload File
if (isset($_POST['submit'])) {
	if (is_uploaded_file($_FILES['filename']['tmp_name'])) {
		echo "<h1>" . "File ". $_FILES['filename']['name'] ." uploaded successfully." . "</h1>";
		echo "<h2>Displaying contents:</h2>";
		//readfile($_FILES['filename']['tmp_name']);
	}

	//Import uploaded file to Database
	$handle = fopen($_FILES['filename']['tmp_name'], "r");

	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$insertrow = $data[1];
		$firstrow = $data[0];
		if(empty($insertrow) || $firstrow == 'ID') continue;
	    $categoryname = substr($insertrow, strrpos($insertrow, "/")+1);
 		$categoryparentstr = substr($insertrow, 0,strrpos($insertrow, "/"));
 		$parent_id = 0;
 		if(!empty($categoryparentstr)){
	     	 $selevalue1 ='SELECT * FROM gocart_categories WHERE parenthei="'.$categoryparentstr.'"';	         	    
	 		 $result2=$conn->query($selevalue1);	
	 		 if($result2->num_rows != 0 ){
		      $row = $result2->fetch_assoc();
		      $parent_id =$row['id'];
		     }else{
		     	$selevalue3 ='SELECT * FROM gocart_categories ';	         	    
	         		 $result3=$conn->query($selevalue3);	
	         		 $newpare = $result3->num_rows;
		     	     $parent_id = $newpare;
		     }	
		}else{
			 $categoryname = substr($insertrow, strrpos($insertrow, "/"));

		}
		$input = preg_replace("/[^a-zA-Z0-9]+/", "", $categoryname);
		$selevalue3 ='SELECT * FROM gocart_categories WHERE name="'.$categoryname.'"';	         	    
	 	$result3=$conn->query($selevalue3);
	 	if($result3->num_rows > 0){
	 		$slug = strtolower($input.$result3->num_rows);
	 	}else{
	 		$slug = strtolower($input);
	 	}
		 	$import='INSERT into gocart_categories(parent_id,server_catid,name,slug,enabled_1,enabled_2,parenthei) values('.$parent_id.',"'.$data[0].'","'.$categoryname.'","'.$slug.'",1,1,'.'"'.$insertrow.'")';
		    $conn->query($import); 				
 		//echo $categoryname."-> parent ->";
 	//echo $categoryparentstr."</br>";

	      /*   $str2 = explode('/',$data[0]);
	         //print_r($str);
	        
	         $cout = count($str2);
	         $str1 = array_filter($str2);  
	         if(!empty($str1))
	         {
	         	   $count1 = $cout-1;
	         	    $str = preg_replace("/[^a-zA-Z 0-9]+/", "", $str1[$count1] );	         		
	         	    $slug = strtolower($str);
	                $last = end($str1);
	         	   
	         	     if($cout>1){
	         	     			//print_r($str1);
	         		 			 unset($str1[$count1]);
	         		 			 print_r($str1);
	         		 			
	         		 			 $parenthei1 = $str1;
	         		 			 echo "----------";
	         		 			echo  $parenthei = implode("/",$str1);
	         		 			 
	         		 			
	         		 		}else{
	         		 		 	$parenthei = $str1[$count1];
	         		 		 
	         		 		}
	         	     $selevalue1 ='SELECT * FROM gocart_categories WHERE parenthei="'.$parenthei.'"';	         	    
	         		 $result2=$conn->query($selevalue1);	
	         		 if($result2->num_rows != 0 ){
				      $row = $result2->fetch_assoc();
				     }
	         			if($result2->num_rows == 0 ){
	         		 		$parentid = 0;
	         		 		$import='INSERT into gocart_categories(parent_id,name,slug,enabled_1,enabled_2,parenthei) values('.$parentid.',"'.$last.'","'.$slug.'",1,1,'.'"'.$parenthei.'")';
						    $conn->query($import); 
	         		  	}else{
	         		  		
	         		  		if($row['parenthei'] == $parenthei)
	         		  		{
	         		  			$parentid = $row['id'];

	         		  		}
	         		  		$import='INSERT into gocart_categories(parent_id,name,slug,enabled_1,enabled_2,parenthei) values('.$parentid.',"'.$last.'","'.$slug.'",1,1,'.'"'.$parenthei.'")';
						    $conn->query($import); 

	         		  	}
	             	
	          }
                  */  
	    }

	        
}else {

	print "Upload new csv by browsing to file and clicking on Upload<br />\n";

	print "<form enctype='multipart/form-data' action='upload.php' method='post'>";

	print "File name to import:<br />\n";

	print "<input size='50' type='file' name='filename'><br />\n";

	print "<input type='submit' name='submit' value='Upload'></form>";

}

?>

</div>
</div>
</body>
</html>