<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width" />
    	<meta name="description" content="">
    	<meta name="author" content="Karthikeyan Balasubramanian, Rajavel Balasubramanian, Awebon">
    	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">    	
    	<link href="<?php echo base_url(); ?>assets/css/user/paymentfont.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/user/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/user/bootstrap-theme.min.css" rel="stylesheet"> 
		<link href="<?php echo base_url(); ?>assets/css/user/holdon.min.css" rel="stylesheet"> 
		<link href="<?php echo base_url(); ?>assets/css/user/jQueryFacebookWall.css" rel="stylesheet" />   						
		<link href="<?php echo base_url(); ?>assets/css/user/iphone-4-5.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/css/user/ipad.css" rel="stylesheet">			
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user/demo.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user/style3.css" />						
		<link href="<?php echo base_url(); ?>assets/css/user/style.css" rel="stylesheet">
		<?php
		$_css = new CSSCrunch();
		$_css->addFile('gumbo/alerts');

		if(true) //Dev Mode
		{
		    //in development mode keep all the css files separate
		    $_css->crunch(true);
		}
		else
		{
		    //combine all css files in live mode
		    $_css->crunch();
		}


		//with this I can put header data in the header instead of in the body.
		if(isset($additional_header_info))
		{
		    echo $additional_header_info;
		}
		?>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>

		

		<title>TargetUs</title>
	</head> 
<body>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-sm-5 col-xs-12">
			<div class="logo">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/img/logo.png" class="img-responsive"></a>
			</div>
		</div>
		<div class="col-md-1 hidden-sm"></div>
		<div class="col-md-5 col-sm-4 col-xs-12">
			<div class="login-reg pull-right">
				<?php
					$ses_data = CI::session()->userdata('customer');
					if($ses_data->firstname) { ?>
						<a href="<?php echo base_url(); ?>logout" class="login">
							<span><img src="<?php echo base_url(); ?>assets/img/login.png" class="img-responsive"></span>
							<span>Logout</span>
						</a>
					<?php } else { ?>
						<a href="<?php echo base_url(); ?>login" class="login">
							<span><img src="<?php echo base_url(); ?>assets/img/login.png" class="img-responsive"></span>
							<span>Login</span>
						</a>
						<a href="<?php echo base_url(); ?>register" class="register">
							<span><img src="<?php echo base_url(); ?>assets/img/register.png" class="img-responsive"></span>
							<span>Register</span>
						</a>
				<?Php } ?>
							
			</div>		
		</div>
		<div class="col-md-2 col-sm-3 col-xs-12 mobile_device">
			<div class="myaccount">
				<?php
				$ses_data = CI::session()->userdata('customer');
				if($ses_data->firstname) { ?>
						<i class="fa fa-user"></i><a href="<?php echo base_url() ?>my-account">My Account</a>
					<?php } else { ?>
						<a href="#"></a>
					<?php } ?>
			</div>
			<div class="add-to-cart" id='header-cart-info'>			
				<a href="<?php echo base_url(); ?>checkout">
					<span><img src="<?php echo base_url(); ?>assets/img/cart.png" class="img-responsive"></span>
					<div class="add-rate">
						<span class="rate"><?php echo GC::getGrandTotal();?> $</span><br>
						<span class="item"><?php echo GC::totalItems();?> Items</span>
					</div>
				</a>
			</div>
		</div>
		<div class="col-md-9 col-sm-12 col-xs-12 no-padding" id="menu">
			<nav class="navbar" role="banner">				            					            
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					    <span class="sr-only">Toggle navigation</span>
					    <span class="icon-bar"></span>
					    <span class="icon-bar"></span>
					    <span class="icon-bar"></span>
					</button>		                    
				</div>						
				<div class="collapse navbar-collapse text-center no-padding">	
					<ul class="nav navbar-nav ">
					    <li><a href="<?php echo base_url(); ?>">Home</a></li>
					    <li><a href="<?php echo base_url(); ?>category/all">Shopping</a></li>
					    <li><a href="<?php echo base_url(); ?>page/event_news/">Event / News</a></li>
					    <li><a href="<?php echo base_url(); ?>pagecontroller/pagealbum">Photo Ablum</a></li>		                        
					    <!-- <li><a href="#">Attorneys</a></li>  -->
					    <li><a href="<?php echo base_url(); ?>category/location/">Available Location</a></li>    
					    <li><a href="<?php echo base_url(); ?>page/contact">Contact</a></li>	    
					</ul>
					<div class="hidden-lg hidden-md hidden-sm col-xs-12">
						<form class="navbar-form" role="search">                
			                <div class="box">
							  <div class="container-1">
							      <span class="icon" onclick="return searinputchanges('searchinallr');"><i class="fa fa-search"></i></span>
							      <input type="search" class="searchinall" id="searchinallr" placeholder="Search..." />
							  </div>
							</div>		                
			            </form>
		        	</div>		
				</div>							
        	</nav>
		</div>
		<div class="col-md-3 col-sm-12 hidden-xs">
			<form class="navbar-form" id="search_form" onsubmit="return false;">                
                <div class="box">
				  <div class="container-1">
				      <span id="search_icon" onclick="return searinputchanges('searchinall');" class="icon"><i class="fa fa-search"></i></span>
				      <input type="search" class="searchinall" value="<?php echo $search_input; ?>" name="search" id="searchinall" placeholder="Search..." />
				  </div>
				</div>
                
            </form>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
        	<img src="<?php echo base_url(); ?>assets/img/line-new-1.png" class="img-responsive">
        </div>		
	</div>
</div>
<script type="text/javascript">
	/*$("#search_icon").click(function(){
		$("#search_form").submit();
	});*/
</script>
