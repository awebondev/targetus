
<div class="spacer"></div>
<div class="container-fluid" id="contact">
	<div class="container">
		<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
			<center>				
				<ul class="nav navbar-nav ">
				    <li><a href="<?php echo base_url() ?>">Home</a></li>
				    <li><a href="<?php echo base_url(); ?>category/all">Shopping</a></li>
				    <li><a href="<?php echo base_url(); ?>page/event_news/">Event / News</a></li>
				    <li><a href="<?php echo base_url(); ?>pagecontroller/pagealbum">Photo Ablum</a></li>		                        				    
				    <li><a href="<?php echo base_url(); ?>category/location/">Available Location</a></li>    
				    <li><a href="<?php echo base_url(); ?>page/contact">Contact</a></li>	    
				</ul>		
			</center>
			<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12 condition">
				<ul class="nav navbar-nav ">
				    <li><a href="<?php echo base_url(); ?>page/termsofuse">Terms of Use</a></li>
				    <li><a href="<?php echo base_url(); ?>page/privacypolicy">Privacy Policy</a></li>
				    <li><a href="<?php echo base_url(); ?>page/shippingdeliverypolicy">Shipping & Delivery Policy</a></li>				        
				</ul>				
			</div>					
		</div>
		<div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12 ">	
		<?php $data = \CI::Settings()->get_settings('gocart');
		  $country = \CI::Locations()->get_country($data['country_id']); ?>
		  <p>TargetUs @ Copyright 2015 | <?php echo $data['phone_set'].' | '.$data['address1'].' '.$data['address2'].' '.$data['city'].' '.$data['zip'].' '.$country->name; ?></p>			
		</div>
	</div>
</div>




	<!--script src="<?php echo base_url(); ?>assets/js/user/jquery.js"></script-->
	
	<script src="<?php echo base_url(); ?>assets/js/user/jquery.spin.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/user/gumbo.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/user/elementQuery.min.js"></script>	
	<script src="<?php echo base_url(); ?>assets/js/user/highchecktree.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/user/bootstrap.min.js"></script>	
	<script src="<?php echo base_url(); ?>assets/js/user/script.js"></script>	
	<script src="<?php echo base_url(); ?>assets/js/user/fbwall/languages.js"></script> 
	<script src="<?php echo base_url(); ?>assets/js/user/fbwall/ejs.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/user/fbwall/jQueryFacebookWall.js"></script>	
	<script src="<?php echo base_url(); ?>assets/js/user/holdon.min.js"></script>
	        
	

	<script type="text/javascript">
		function testHoldon(themeName){
	          HoldOn.open({
	              theme:themeName,
	              message:"<h4>Loading</h4>"
	          });	          	      	   
	      }
	</script>

	<script>
	$(document).ready(function(){
		// Calling our plugin with a fb account username and an access token, from 
		$('.facebook-wall').jQueryFacebookWall({
			'appId': '1684140971802595',
			'facebookUser':'targetuscanada',
			'domain': 'http://www.targetus.ca/',
			'installDirectory': '/', //where the plugin lives relative to your web root
			'display':'wall',
			'posts':{
				'feedType': 'feed',
				'limit': 12	
			},
			'comments':{
				'showAtStart': 3,
				'showMoreNum': 10,
				'limit': 50
			},
			'likes':{
				'minLikes': 0 
			},
			'albums':{
				'limit': 6
			},
			'events':{
				'show':true
			},
			'debug': false
		});
	})//end document ready
	</script>

	</body>
</html>