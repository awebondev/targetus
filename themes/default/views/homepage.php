<div class="container">
	<div class="row">
		<div class="col-md-9 col-sm-9 col-xs-12">
		    <div id="custom_carousel" class="carousel slide" data-ride="carousel" data-interval="2500">
		        <!-- Wrapper for slides -->
		        <div class="carousel-inner">
		            <?php $sid = 0;
		            $slidercount = count($sliderslist);
		             foreach($sliderslist as $slider){ ?>
		        	<div class="item <?php echo ($sid==0)? 'active':'' ?>">
		                <div class="container-fluid">
		                    <div class="row">	                        
		                        <div class="col-md-8 col-sm-8 col-xs-7">
		                            <h2><?php echo $slider->title ?></h2>
		                            <p class="hidden-xs"><?php echo $slider->description ?></p>
		                            <a href="<?php echo $slider->image_link ?>" class="login-btn green-bg table-mar">
										<span><img src="<?php echo base_url(); ?>assets/img/cart.png"></span>
										<h1>Shop Now</h1>
									</a>
		                            <div class="controls">
							            <ul class="nav">
							            <?php for($i=0;$i<$slidercount;$i++){ ?>
							                <li data-target="#custom_carousel" data-slide-to="<?php echo $i ?>" class="<?php echo ($sid==$i)? 'activated':'deactive' ?>">
								                <a href="#"></a>
							                </li>
							                
							             <?php } ?>                
							            </ul>
						        	</div>
		                        </div>
		                        <div class="col-md-4 col-sm-4 col-xs-5"><img src="<?php echo base_url(); ?>product_photo/<?php echo $slider->image ?>" class="img-responsive mobile"></div>
		                    </div>
		                    
		                </div>            
		            </div> 
		            <?php $sid = $sid+1; } ?>          
		            		            	           
		        <!-- End Item -->
		        </div>
		        <!-- End Carousel Inner -->	        
		    </div>
		    <!-- End Carousel -->
		</div>

		<div class="col-md-3 col-sm-3 col-xs-12">
			<div class="left-slider">			
				<div class="slide col-xs-4 col-sm-12 col-xs-12 no-padding">
					<a href="#"><img src="<?php echo base_url(); ?>assets/img/slide-2.png" class="img-responsive"></a>
				</div>
				<div class="slide col-xs-4 col-sm-12 col-xs-12 no-padding">						
					<a href="#"><img src="<?php echo base_url(); ?>assets/img/slide-1.png" class="img-responsive"></a>
				</div>					
				<div class="slide col-xs-4 col-sm-12 col-xs-12 no-padding">
					<a href="#"><img src="<?php echo base_url(); ?>assets/img/slide-3.png" class="img-responsive"></a>
				</div>												
			</div>
		</div>

	</div>
</div>


<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12 no-padding divided-mar">
			<div class="divider">
				<h5>Main Categories</h5>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12">
				<center>
					<img src="<?php echo base_url(); ?>assets/img/divided-logo.png" class="img-responsive mobile-2">
				</center>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
		</div>
		<!-- categories -->
		<div class="col-md-12 col-sm-12 col-xs-12" id="categories">
			<div class="row">
				<div class="firstcat">
					<?php 
					$cats = CI::Categories()->getBySlug('accessories'); 
					$selected = (CI::uri()->segment(2) == $cats->slug)?'class="selected"':'';
					?>
						<div class="col-md-4 col-sm-4 col-xs-12">
						<h2><?php echo $cats->name; ?></h2>
						<p><?php echo $cats->description; ?></p>
						<div class="categories">
							<div class="product_main_img">
								<div class="dynamic-img">
								<?php if($cats->image != ''){ ?>
									<img src="<?php echo base_url('product_photo/'.$cats->image); ?>" class="img-responsive">
									<?php } else { ?>
									<img src="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png" class="img-responsive">
									<?php } ?>
								</div>
							</div>
								<a href="<?php echo 'category/'.$cats->slug,$selected; ?>" class="orange">Learn More</a>
							</div>
						</div>
				</div>
				<div class="secondcat">
					<?php 
					$cats = CI::Categories()->getBySlug('cases'); 
					$selected = (CI::uri()->segment(2) == $cats->slug)?'class="selected"':'';
						?>
						<div class="col-md-4 col-sm-4 col-xs-12">
						<h2><?php echo $cats->name; ?></h2>
						<p><?php echo $cats->description; ?></p>
						<div class="categories">
							<div class="product_main_img">
								<div class="dynamic-img">
									<?php if($cats->image != ''){ ?>
									<img src="<?php echo base_url('product_photo/'.$cats->image); ?>" class="img-responsive">
									<?php } else { ?>
									<img src="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png" class="img-responsive">
									<?php } ?>
								</div>
							</div>
								<a href="<?php echo 'category/'.$cats->slug,$selected; ?>" class="orange">Learn More</a>
							</div>
						</div>
				</div>
				<div class="thirdcat">
					<?php 
					$cats = CI::Categories()->getBySlug('dartboard'); 
					$selected = (CI::uri()->segment(2) == $cats->slug)?'class="selected"':'';
						?>
						<div class="col-md-4 col-sm-4 col-xs-12">
						<h2><?php echo $cats->name; ?></h2>
						<p><?php echo $cats->description; ?></p>
						<div class="categories">
							<div class="product_main_img">
								<div class="dynamic-img">
									<?php if($cats->images != ''){ ?>
									<img src="<?php echo base_url('product_photo/'.$cats->image); ?>" class="img-responsive">
									<?php } else { ?>
									<img src="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png" class="img-responsive">
									<?php } ?>
								</div>
							</div>
								<a href="<?php echo 'category/'.$cats->slug,$selected; ?>" class="orange">Learn More</a>
							</div>
						</div>
				</div>
			</div>
		</div>
		<!-- categories -->			
		<div class="col-md-12 col-sm-12 col-xs-12 no-padding divided-mar">
			<div class="divider">
				<h5>Latest Product</h5>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12">
				<center>
					<img src="<?php echo base_url(); ?>assets/img/divided-logo.png" class="img-responsive mobile-2">
				</center>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
		</div>

		<!-- latest -->
		<div class="col-md-12 col-sm-12 col-xs-12" id="latest-product">
			<div class="row">
			<?php 
				$products = CI::Products()->getProducts(0,3);
				foreach($products as $product) {
					
					$photo  = theme_img('no_picture.png');
		            if(!empty($product->images[0]))
		            {
		                $primary = $product->images[0];
		                foreach($product->images as $photo)
		                {
		                    if(isset($photo['primary']))
		                    {
		                        $primary = $photo;
		                    }
		                }

		                $photo  = base_url('product_photo/'.$primary['filename']);
		            } ?>	
					<div class="col-md-4 col-sm-4 col-xs-12">						
						<div class="latest-product">
							<div class="green-strip"><?php echo $product->name; ?></div>
							<div class="product_main_img">
								<div class="dynamic-img">
								<?php if($photo != ''){ ?>
									<img src="<?php echo $photo;?>" class="img-responsive">
									<?php } else { ?>
									<img src="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png" class="img-responsive">
									<?php } ?>

									
								</div>
							</div>
							<?php if(!$product->is_giftcard): //do not display this if the product is a giftcard?>
                        <div class="price-tag">
                            <a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');">
                            <?php echo ( $product->saleprice>0?format_currency($product->saleprice):format_currency($product->price) );?></a>
                        </div>
                    <?php endif;?>
							<div class="pro-list-name">
		                        <a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');">
		                        <?php echo $product->name;?></a>
		                    </div>
		                    <div class="pro-list-desc">
		                        <?php echo $product->description; ?>
		                    </div>
		                    <div class="favorite">
		                        <ul>
		                            <li class="pro-cart"><a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');"></a></li>
		                            <!-- <li class="pro-heart"><a href="#"></a></li>
		                            <li class="pro-file"><a href="#"></a></li> -->
		                        </ul>                               
		                    </div>							
						</div>
					</div>
				<?php } ?>	
			</div>
		</div>
		<!-- Latest -->

		<div class="col-md-12 col-sm-12 col-xs-12 no-padding divided-mar">
			<div class="divider">
				<h5>Feature Products</h5>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12">
				<center>
					<img src="<?php echo base_url(); ?>assets/img/divided-logo.png" class="img-responsive mobile-2">
				</center>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
		</div>

		<!-- promo -->
		<div class="col-md-12 col-sm-12 col-xs-12" id="latest-product">
			<div class="row">
			<?php 
				$products = CI::Products()->getProductsbyprspecial(0,3);				
				if(!($products)){
					echo 'No Feature Products';
				}
				foreach($products as $product) {
					
					$photo  = theme_img('no_picture.png');
		            if(!empty($product->images[0]))
		            {
		                $primary = $product->images[0];
		                foreach($product->images as $photo)
		                {
		                    if(isset($photo['primary']))
		                    {
		                        $primary = $photo;
		                    }
		                }

		                $photo  = base_url('product_photo/'.$primary['filename']);
		            } ?>	
					<div class="col-md-4 col-sm-4 col-xs-12">						
						<div class="latest-product">
							<div class="green-strip"><?php echo $product->name; ?></div>
							<div class="dynamic-img">
							<?php if($photo != ''){ ?>
								<img src="<?php echo $photo;?>" class="img-responsive">
								<?php } else { ?>
								<img src="<?php echo base_url(); ?>/themes/default/assets/img/no_picture.png" class="img-responsive">
								<?php } ?>

								
							</div>
							<?php if(!$product->is_giftcard): //do not display this if the product is a giftcard?>
                        <div class="price-tag">
                            <a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');">
                            <?php echo ( $product->saleprice>0?format_currency($product->saleprice):format_currency($product->price) );?></a>
                        </div>
                    <?php endif;?>
							<div class="pro-list-name">
		                        <a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');">
		                        <?php echo $product->name;?></a>
		                    </div>
		                    <div class="pro-list-desc">
		                        <?php echo $product->description; ?>
		                    </div>
		                    <div class="favorite">
		                        <ul>
		                            <li class="pro-cart"><a href="<?php echo site_url('/product/'.$product->slug); ?>" onClick="return testHoldon('sk-dot');"></a></li>
		                            <!-- <li class="pro-heart"><a href="#"></a></li>
		                            <li class="pro-file"><a href="#"></a></li> -->
		                        </ul>                               
		                    </div>							
						</div>
					</div>
				<?php } ?>	
			</div>
		</div>
		<!-- promo -->

		<div class="col-md-12 col-sm-12 col-xs-12 no-padding divided-mar">
			<div class="divider">
				<h5>News Letter</h5>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12">
				<center>
					<img src="<?php echo base_url(); ?>assets/img/divided-logo.png" class="img-responsive mobile-2">
				</center>
			</div>
			<div class="col-md-5 col-sm-5 col-xs-12">
				<img src="<?php echo base_url(); ?>assets/img/line.png" class="img-responsive line-img">
			</div>
		</div>
		<div class="col-md-12 col-sm-12 col-xs-12" id="news-letter">
			<div class="col-md-8 col-md-offset-2">
				<p>Sign up for our exclusive email list and be the first to hear of <br>
				   special promotions, new arrivals, and designer news
				</p>

				<form method="post" action="http://targetus.ca/emailmarketer/form.php?form=1" id="frmSS1" onsubmit="return CheckForm1(this);">
					<div id="custom-search-input">
	                    <div class="input-group col-md-10 col-md-offset-1">
	                        <input type="text" name="email" class="  search-query form-control" placeholder="Enter your email address" />
	                        <input type="hidden" name="format" value="h" />
	                        <span class="input-group-btn">
	                            <button class="btn orange green-bg" type="submit">
	                            SUBSCRIBE                                    
	                            </button>
	                        </span>
	                        
	                    </div>
	                </div>
                </form>
			</div>
		</div>
	</div>
</div>
<!-- <form method="post" action="http://targetus.ca/emailmarketer/form.php?form=1" id="frmSS1" onsubmit="return CheckForm1(this);">
	<table border="0" cellpadding="2" class="myForm">
		<tr>
	<td><span class="required">*</span>&nbsp;
Your Email Address:</td>
	<td><input type="text" name="email" value="" /></td>
</tr><input type="hidden" name="format" value="h" />
		<tr>
			<td></td>
			<td>
				<input type="submit" value="Subscribe" />
				<br/><span style="display: block; font-size: 10px; color: gray; padding-top: 5px;"><a href="#" target="__blank" style="font-size:10px;color:gray;">Email marketing</a> by Hairyspire</span>
			</td>
		</tr>
	</table>
</form> -->

<script type="text/javascript">
// <![CDATA[

			function CheckMultiple1(frm, name) {
				for (var i=0; i < frm.length; i++)
				{
					fldObj = frm.elements[i];
					fldId = fldObj.id;
					if (fldId) {
						var fieldnamecheck=fldObj.id.indexOf(name);
						if (fieldnamecheck != -1) {
							if (fldObj.checked) {
								return true;
							}
						}
					}
				}
				return false;
			}
		function CheckForm1(f) {
			var email_re = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/i;
			if (!email_re.test(f.email.value)) {
				alert("Please enter your email address.");
				f.email.focus();
				return false;
			}
		
				return true;
			}
		
// ]]>
</script>
